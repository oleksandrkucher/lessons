<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Lesson 8. GeekHub</h1>
		<h2>Without Custom Taglib</h2>
		<form action = "/Lesson8.GeekHub/Manage">
			<input type = 'hidden' name = 'cd' value = 'noTagLib'>
			<table border = 1 height = 500 width = 1000>
				<tr>
					<th>Name:</th><th>Value:</th><th>Action</th>
				</tr>
				<c:set var="valuesSession" value = "${sessionScope}"></c:set>
					<c:forEach var="vs" items="${valuesSession}">
						<c:if test="${vs.key != null && vs.value != null}">
							<tr>
								<td>${vs.key}</td>
								<td>${vs.value}</td>
								<td><a href="Manage?cd=noTagLib&action=remove&name=${vs.key}">delete</a></td>
							</tr>
						</c:if>
  					</c:forEach>
				<tr>
					<td><input type = 'text' name = 'name' value = ''></td>
					<td><input type = 'text' name = 'value' value = ''></td>
					<td><input type = 'submit' value = 'add'></td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>