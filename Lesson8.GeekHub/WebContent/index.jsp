<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/MyTags.tld" prefix="m" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Lesson 8. GeekHub. With Custom TagLib</title>
</head>
<body>
	<center>
		<h1>Lesson 8. GeekHub</h1>
		<h2>With Custom Taglib</h2>
		<form action = "/Lesson8.GeekHub/Manage">
			<m:sessionToTable/>
		</form>
	</center>
</body>
</html>