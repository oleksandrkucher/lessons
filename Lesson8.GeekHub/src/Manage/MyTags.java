package Manage;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

public class MyTags extends TagSupport {

	private static final long serialVersionUID = 1L;

	private HttpSession session = null;

	public int doStartTag() {
		session = pageContext.getSession();
		try {
            if (session != null) {
            	Enumeration<String> valuesSession = session.getAttributeNames();
            	pageContext.getOut().write(
            			"<table border = 1 height = 500 width = 1000>" +
            				"<tr>" +
            				"<th>Name:</th><th>Value:</th><th>Action</th>" +
            			"</tr>");
            	while (valuesSession.hasMoreElements()) {
					String key = valuesSession.nextElement();
					String val = (String) session.getAttribute(key);
					pageContext.getOut().write(
							"<tr>" +
								"<td>" + key + "</td>" +
								"<td>" + val + "</td>" +
								"<td><a href='/Lesson8.GeekHub/Manage?action=remove&name=" + key + 
								"'>delete</a></td>" +
							"</tr>");
            	}
            	pageContext.getOut().write(
            			"<tr>" +
            				"<td><input type = 'text' name = 'name' value = ''></td>" +
            				"<td><input type = 'text' name = 'value' value = ''></td>" +
            				"<td><input type = 'submit' value = 'add'></td>" +
            			"</tr>" +
            		"</table>");
            } else {
            	pageContext.getOut().write("");
            }
        } catch (IOException ioe) {
            try {
				throw new JspTagException(ioe.getMessage());
			} catch (JspTagException e) {
				e.printStackTrace();
			}
        }
        return SKIP_BODY;
    }
	
	/**
	 * @return the session
	 */
	public HttpSession getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(HttpSession session) {
		this.session = session;
	}

	public void release() {
        super.release();
        session = null;
    }
}