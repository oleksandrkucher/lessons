package ServerAndClient;

import java.io.*;
import java.net.*;

/**
 * Main class for simple server
 * 
 * @author kol
 *
 */

public class SimpleServer {
	
	private static final int PORT_NUMBER = 6789;

	public static void main(String args[]) throws IOException {
		
		String clientSentence;
		SimpleServer serv = new SimpleServer();
		@SuppressWarnings("resource")
		ServerSocket connectionSocket = new ServerSocket(PORT_NUMBER);
		while(true){
			Socket server = null;
			server = connectionSocket.accept();
			while ((clientSentence = serv.fromClient(server)) != null) {
				if(clientSentence.length() > 0){
					System.out.println("FROM CLIENT: " + clientSentence);
				}
				serv.toClient(clientSentence, server);
			}
		}
	}
	
	/**
	 * Method gets message from client
	 * @param connectionSocket - active socket
	 * @return - message which server gets from client
	 */
	
	private String fromClient(Socket connectionSocket){
		BufferedReader inFromClient = null;
		String clientMessage = "";
		try {
			inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			clientMessage = inFromClient.readLine();
		} catch (IOException e){
			System.out.println("ERROR! " + e);
		}
		return clientMessage;
	}
	
	/**
	 * Method sends new message to client
	 * @param clientMessage - message which server gets from client
	 * @param connectionSocket - active socket
	 */
	
	private void toClient(String clientMessage, Socket connectionSocket){
		
		try {
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			if(!clientMessage.equals("exit") && clientMessage.length() > 0){
				outToClient.writeByte(clientMessage.charAt(0));
			} else if(clientMessage.length() == 0){
				outToClient.writeByte(0);
			}
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
	}
}