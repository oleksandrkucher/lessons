package ServerAndClient;

import java.io.*;
import java.net.*;

/**
 * Main class for simple client
 * 
 * @author kol
 * 
 */

public class SimpleClient {

	private static final int PORT_NUMBER = 6789;

	public static void main(String args[]) throws IOException {

		Socket client;
		SimpleClient sc = new SimpleClient();
		client = sc.createForClient();
		while (sc.toServer(client)) {
			char tmp = sc.fromServer(client);
			if(tmp != 0){
				System.out.println("ANSWER FROM SERVER: " + tmp);
			}
		}
		client.close();
	}

	/**
	 * Method gets all information for creating new Socket and creates it
	 * 
	 * @return - created socket
	 */

	private Socket createForClient() {

		String hostname = getStr("hostname");
		Socket clientSocket = null;
		try {
			clientSocket = new Socket(hostname, PORT_NUMBER);
		} catch (IOException e) {
			System.out.println("ERROR! " + e + " Try again.");
			createForClient();
		}
		return clientSocket;
	}
	
	/**
	 * Method sends message to server
	 * @param client - active socket
	 * @return - boolean value for exit or not
	 */
	
	private boolean toServer(Socket client){
		
		boolean continueProgram = true;
		DataOutputStream outToServer;
		try {
			outToServer = new DataOutputStream(client.getOutputStream());
			String str = getStr("message to server or 'exit' to shutdown program");
			if(str.equals("exit")){
				continueProgram = false;
			} else {
				outToServer.writeBytes(str + '\n');
			}
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return continueProgram;
		
	}

	/**
	 * Method gets message from server
	 * @param client - active socket
	 * @return - message
	 */
	
	private char fromServer(Socket client){
		
		BufferedReader inFromServer = null;
		char modifiedMessage = 0;
		try {
			inFromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
			modifiedMessage = (char)inFromServer.read();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return modifiedMessage;
	}
	
	/**
	 * Method reads string from keyboard
	 *  
	 * @return string which was read from keyboard 
	 */
	
	private String getStr(String string){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		System.out.print("Input " + string + ": ");
		try {
			str = buf.readLine();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return str;
	}

}