package FindAllLinks;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Class contains methods for search and check links in html page
 * 
 * host - address of host
 * protocol - access protocol
 * links - set for all links
 * sortedLinksOK - set for good links
 * sortedLinksTO - set for redirected links
 * sortedLinksBAD - set for bad links
 * 
 * @author kol
 */

public class FindLinks {

	private String host;
	private String protocol;
	private Set<String> links;
	private Set<String> sortedLinksOK;
	private Set<String> sortedLinksTO;
	private Set<String> sortedLinksBAD;
	
	/**
	 * Constructor create HashSets for storage links 
	 */
	
	public FindLinks(){
		
		sortedLinksOK = new HashSet<String>();
		sortedLinksTO = new HashSet<String>();
		sortedLinksBAD = new HashSet<String>();
		links = new HashSet<String>();
	}
	
	/**
	 * Method gets all information and gets connection to host
	 * 
	 * @return connection
	 */
	
	public URL getConnection(){
		
		URL url = null;
		protocol = getStr("of protocol");
		host = getStr("of host");
		String file = getStr("file with all simbols '/'");
		System.out.print("Do you want to input number of port");
		int port = 0;
		try {
			if(yesOrNo()){
				System.out.print("Input number of port: ");
				port = getNumber();
				url = new URL(protocol, host, port, file);
			} else {
				url = new URL(protocol + "://" + host + file);
			}
		} catch (MalformedURLException e) {
			System.out.println("ERROR! " + e + "Try again.");
			return getConnection();
		}
		
		return url;
	}
	
	/**
	 * Method search links and add it to the HashSet
	 * 
	 * @param str - html code of web-page
	 */
	
	public void searchLinkAndAddToSet(String str) {
		
		String tmp;
		str = str.replace(" ", "");
		int start = str.indexOf("<a");
		if(start >= 0){
			start = str.indexOf("href=\"");
			if(start >= 0){
				str = str.substring(start+6, str.length());
				start = str.indexOf("\"");
				tmp = str;
				if(start >= 0){
					str = str.substring(0, start);
					start = str.indexOf("http");
					if(start < 0){
						str = protocol + "://" + host + str; 
					}
					links.add(str);
				}
				searchLinkAndAddToSet(tmp);
			}
		}
	}
	
	/**
	 * Method check all links and sorted it (good, bad, redirected)
	 */
	
	public void countAllLinksStatus() {
		
		for (String link : links) {
			HttpURLConnection httpURL;
			int code = 0;
			try {
				httpURL = (HttpURLConnection) new URL(link).openConnection();
				httpURL.setRequestMethod("HEAD");
				code = httpURL.getResponseCode();
			} catch (IOException e) {
				System.out.println("ERROR! " + e);
			}
			
			if(code >= 200 && code < 300){
				sortedLinksOK.add(link);
			} else if(code >= 300 && code < 400){
				sortedLinksTO.add(link);
			} else if(code >= 400){
				sortedLinksBAD.add(link);
			}
		}
	}

	/**
	 * Method print all found links by groups
	 */
	
	public void printAllLinks(){
		
		System.out.println("Good links:");
		for (Object link : sortedLinksOK) {
			System.out.println(link);
		}
		System.out.println("Redirected links:");
		for (Object link : sortedLinksTO) {
			System.out.println(link);
		}
		System.out.println("Bad Links:");
		for (Object link : sortedLinksBAD) {
			System.out.println(link);
		}
	}
	
	/**
	 * Method gets html page and write it in string
	 */
	
	public void readHTML(URL url){
		
		String str = "";
		String tmp;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			while((tmp = in.readLine()) != null){
				str = str + tmp;
			}
			searchLinkAndAddToSet(str);
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
	}
	
	/**
	 * Method gets integer number from keyboard
	 * 
	 * @return integer number
	 */
	
	private int getNumber(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		try {
			return Integer.parseInt(buf.readLine());
		} catch (NumberFormatException | IOException e) {
			System.out.println("ERROR! " + e);
			return getNumber();
		}
	}
	
	/**
	 * Method reads string from keyboard
	 *  
	 * @param string - out string
	 * @return string which was read from keyboard 
	 */
	
	private String getStr(String string){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		System.out.print("Input name "+ string +" : ");
		try {
			str = buf.readLine();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return str;
	}
	
	/**
	 * Method asks you to confirm your choice
	 * @return boolean value true or false
	 * @throws IOException
	 */
	
	private boolean yesOrNo() {

		String yesOrNo = scanYesOrNo();
		switch (yesOrNo) {
		case "YES":
		case "Y":
			return true;
		case "NO":
		case "N":
			return false;
		default:
			return false;
		}
	}
	
	/**
	 * Method reads a term yes/no or y/n
	 * 
	 * @return answer yes/no 
	 */
	
	private String scanYesOrNo() {

		System.out.print(" Y/N: ");
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		String yesOrNo = null;
		try {
			yesOrNo = read.readLine();
		} catch (IOException e) {
			System.out.println("ERROR! " + e + "Try again.");
		}
		if(yesOrNo != null){
			yesOrNo = yesOrNo.toUpperCase();
		}
		return yesOrNo;

	}

}
