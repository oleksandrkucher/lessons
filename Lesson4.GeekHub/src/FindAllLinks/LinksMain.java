package FindAllLinks;

import java.net.*;

/**
 * Main class for search and check links
 * 
 * @author kol
 *
 */

public class LinksMain {

	public static void main(String[] args) {
		
		FindLinks link = new FindLinks();
		URL url = link.getConnection();
		link.readHTML(url);
		link.countAllLinksStatus();
		link.printAllLinks();
	}

}