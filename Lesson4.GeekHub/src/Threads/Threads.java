package Threads;

import java.io.*;

public class Threads {

	
	/**
	 * Method gets all files from hard disk with using threads
	 * @param dir - current directory
	 * @param str - pattern
	 */
	
	public void getAllFilesByThreads(final File dir, final String str){
		
		Thread newThread = new Thread(new Runnable() {
			public void run() {
				File [] files = dir.listFiles();
				if(null != files && files.length > 0) {
					for(File f : files) {
						if(f.isDirectory()) {
							getAllFilesByThreads(f, str);
						} else {
							if(f.getAbsolutePath().endsWith(str.substring(1))){
								System.out.println(f.getAbsolutePath());
							}
						}
					}
				}
			}
		});
		newThread.start();
	}

	/**
	 * Method gets all files from hard disk with using recursion (one thread)
	 * @param dir - current directory
	 * @param str - pattern
	 */
    
    public void getAllFilesByRecurs(File dir, String str){

    	File [] files = dir.listFiles();
		if(null != files && files.length > 0) {
			for(File f : files) {
				if(f.isDirectory()) {
					getAllFilesByRecurs(f, str);
				} else {
					if(f.getAbsolutePath().endsWith(str.substring(1))){
						System.out.println(f.getAbsolutePath());
					}
				}
			}
		}
    }
	

	/**
	 * Method reads string from keyboard
	 *  
	 * @return string which was read from keyboard 
	 */
	
	public String getStr(String string){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		System.out.print("Input " + string + ": ");
		try {
			str = buf.readLine();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return str;
	}
	
	/**
	 * Method gets pattern and check if it is correct
	 * @return - pattern
	 */
	
	public String readPattern(){
		String str = getStr("pattern");
		if(str.charAt(0) == '*' && str.charAt(1) == '.'){
			return str;
		} else {
			System.out.println("Your pattern is wrong. Try again.");
			return readPattern();
		}
	}
}