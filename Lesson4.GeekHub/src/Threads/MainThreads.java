package Threads;

import java.io.*;

public class MainThreads {

	public static void main(String[] args) {

		Threads thread = new Threads();
		String str = thread.readPattern();
		String path = thread.getStr("path to your folder");
		
		System.out.println("Recursion: ");
		thread.getAllFilesByRecurs(new File(path), str);
		
		System.out.println("Threads: ");
		thread.getAllFilesByThreads(new File(path), str);
	}
}