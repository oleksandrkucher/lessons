package Exceptions;

import java.io.*;
import java.util.Random;

public class Exceptions {

	int array[];
	
	public static void main(String[] args){
		
		Exceptions my = new Exceptions();
		int number;
		my.makeArray();
		
		try {
			System.out.print("Input index: ");
			number = my.getNum();
			System.out.println("RunTimeException:");
			my.myRunTimeException(number);
		} catch (MyRunTimeException e) {
			System.out.println("Catched " + e);
		}
		try {
			System.out.print("Input element: ");
			number = my.getNum();
			System.out.println("Exception:");
			my.myException(number);
		} catch (MyException e) {
			System.out.println("Catched " + e);
		}

	}

	public void myException(int a) throws MyException{
		
		if(a == 10){
			throw new MyException(a);
		}
		System.out.println("Good finish.");
		
	}
	
	public void myRunTimeException(int indexOfArray) {
		
		if(indexOfArray >= 10 || indexOfArray < 0){
			throw new MyRunTimeException(indexOfArray);
		}
		System.out.println("Element = " + array[indexOfArray]);
		System.out.println("Good finish.");
		
	}

	public void makeArray(){
		
		array = new int[10];
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			array[i] = random.nextInt(15);
		}
	}

	public int getNum(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		try {
			return Integer.parseInt(buf.readLine());
		} catch (NumberFormatException | IOException e) {
			return getNum();
		}
	}
	
}
