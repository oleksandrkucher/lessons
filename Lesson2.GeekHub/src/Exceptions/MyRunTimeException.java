package Exceptions;

public class MyRunTimeException extends RuntimeException{

	private int index;
	
	public MyRunTimeException(int index){
		
		this.index = index;
	}
	
	public String toString(){
		
		return "MyRunTimeException: ArrayIndexOutOfBoundsException."
				+ " Index = " + index;
	}

}
