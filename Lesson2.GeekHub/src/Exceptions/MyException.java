package Exceptions;

public class MyException extends Exception{

	private int num;
	
	public MyException(int num){
		
		this.num = num;
	}
	
	public String toString(){
		
		return "Exception: UnacceptableValue."
				+ " Number = " + num;
	}

}