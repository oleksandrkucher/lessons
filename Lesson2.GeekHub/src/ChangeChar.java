import java.io.*;

public class ChangeChar {

	public static void main(String[] args) {
		
		String str = getStr();
		str = changeChar(str);
		System.out.println("Your new string: " + str);

	}
	
	public static String getStr(){
		
		String str = null;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Input your string: ");
		try {
			str = buf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return str;
	}
	
	public static String changeChar(String str){
		
		String newStr = null;
		char newCh = str.charAt(0);
		if(Character.isLowerCase(newCh)){
			newCh = Character.toUpperCase(newCh);
		} else if(Character.isUpperCase(newCh)){
			newCh = Character.toLowerCase(newCh);
		}
		newStr = newCh + str.substring(1);
		
		return newStr;
	}
	
}
