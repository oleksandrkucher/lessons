package Stack;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class ListLink implements Queue<ListLink>{
	
	ListLink top;
	ListLink next;
	String value;
	static int size = 0;
	
	public ListLink(){
		
		size = 0;
		top = null;
		next = null;
	}
	
	public ListLink(String str){
		
		this.next = null;
		this.top = this;
		this.value = str;
	}
	
	public boolean add(ListLink list){
		
		list.next = top;
		this.top = list;
		this.value = list.value;
		size++;
		return true;
	}
	
	public boolean isEmpty(){
		
		if(this.top == null || size <= 0){
			return true;
		} else {
			return false;
		}
	}
	
	public int size(){
		
		return size;
	}
	
	public ListLink peek(){
		
		if(!isEmpty()){
			return get();
		} else {
			return null;
		}
	}
	
	public ListLink get(){
		
		return top;
	}
	
	public ListLink poll(){
		
		if(isEmpty() == false){
			ListLink tmp = get();
			this.top = remove();
			return tmp;
		} else {
			return null;
		}
	}
	
	public ListLink remove(){
		
		if(isEmpty() == false){
			ListLink tmp = this.top.next;
			size--;
			return tmp;
		} else {
			return null;
		}
	}
	
	public void clear(){
		
		this.top = null;
		this.value = null;
		this.next = null;
	}

	
	
	public Iterator<ListLink> iterator(){
		return null;
	}
	public Object[] toArray(){
		return null;
	}
	public boolean remove(Object obj){
		return true;
	}
	public ListLink element(){
		return null;
	}
	public boolean offer(ListLink str){
		return true;
	}
	public boolean contains(Object obj){
		return true;
	}
	public boolean removeAll(Collection<?> col){
		return true;
	}
	public boolean containsAll(Collection<?> col){
		return true;
	}
	public boolean retainAll(Collection<?> col){
		return true;
	}
	public boolean addAll(Collection<? extends ListLink> col){
		return true;
	}
	public <T> T[] toArray(T T[]){
		return null;
	}
}
