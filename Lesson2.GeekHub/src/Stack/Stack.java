package Stack;

public class Stack {

	public static void main(String[] args) {

		ListLink listLink = new ListLink();
		listLink.add(new ListLink("5"));
		listLink.add(new ListLink("4"));
		listLink.add(new ListLink("3"));
		listLink.add(new ListLink("2"));
		listLink.add(new ListLink("1"));
		
		if (listLink.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			System.out.println("Stack is not empty!");
		}
		
		System.out.println("peek: value = " + listLink.peek().value
				+ ", size = " + listLink.size());
		System.out.println("pool: value = " + listLink.poll().value
				+ ", size = " + listLink.size());
		System.out.println("pool: value = " + listLink.poll().value
				+ ", size = " + listLink.size());
		System.out.println("pool: value = " + listLink.poll().value
				+ ", size = " + listLink.size());
		System.out.println("pool: value = " + listLink.poll().value
				+ ", size = " + listLink.size());
		System.out.println("pool: value = " + listLink.poll().value
				+ ", size = " + listLink.size());

		if (listLink.isEmpty()) {
			System.out.println("Stack is empty!");
		}
		
		listLink.add(new ListLink("3"));
		listLink.add(new ListLink("2"));
		listLink.add(new ListLink("1"));
		
		if (listLink.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			System.out.println("Stack is not empty!");
		}

		listLink.clear();
		System.out.println("After clear():");
		
		if (listLink.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			System.out.println("Stack is not empty!");
		}
	}
}
