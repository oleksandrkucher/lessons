package Garage;

public class Car implements Comparable<Car>{

	private String color;
	private String model;
	private int length;
	
	public Car(String model, String color, int length){
		
		this.model = model;
		this.color = color;
		this.length = length;
	}

	public String toString(){
		
		return this.length + " " + this.model + " " + this.color;
	}
	
	public int compareTo(Car car) {
		
		int result = color.compareTo(car.color);
		if (result != 0) {
			return result;
		}
		
		result = model.compareTo(car.model);
		if (result != 0) {
			return result;
		}
		
		result = length - car.length;
		if (result != 0) {
			return -result / Math.abs(result);
		}
		
		return 0;
	}
}
