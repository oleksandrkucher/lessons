package Garage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Garage.Car;

public class Garage {
	
	private List<Car> garage = new ArrayList<>();
	
	public static void main(String[] args) {
		
		Garage carGarage = new Garage();
		carGarage.addAllCars(); 
		Collections.sort(carGarage.garage);
		
		for(Car tmp : carGarage.garage){
			System.out.println(tmp);
		}
	}
	
	public void addAllCars(){
		
		garage.add(0, new Car("Hyundai", "GREY", 3));
		garage.add(1, new Car("Honda", "BLUE", 5));
		garage.add(2, new Car("Dodge", "GREEN", 6));
		garage.add(3, new Car("Honda", "RED", 4));
		garage.add(4, new Car("Nissan", "YELLOW", 3));
		garage.add(5, new Car("Mercedes-Benz", "BLACK", 4));
		garage.add(6, new Car("Hyundai", "RED", 5));
		garage.add(7, new Car("Hyundai", "YELLOW", 3));
		garage.add(8, new Car("Mazda", "GREEN", 4));
		garage.add(9, new Car("Toyota", "BLACK", 6));
		garage.add(10, new Car("Honda", "BLUE", 5));
		garage.add(11, new Car("Dodge", "BLACK", 3));
		garage.add(12, new Car("Honda", "RED", 3));
		garage.add(13, new Car("Subaru", "YELLOW", 3));
		garage.add(14, new Car("Subaru", "GREEN", 4));
		garage.add(15, new Car("Mitsubishi", "YELLOW", 3));
		garage.add(16, new Car("Nissan", "GREEN", 5));
		garage.add(17, new Car("Audi", "BLUE", 4));
		garage.add(18, new Car("Mitsubishi", "BLACK", 5));
		garage.add(19, new Car("Audi", "RED", 3));
		garage.add(20, new Car("BMW", "GREY", 3));
		garage.add(21, new Car("Chevrolet", "YELLOW", 4));
		garage.add(22, new Car("Audi", "GREY", 5));
		garage.add(23, new Car("Chevrolet", "GREEN", 3));
		garage.add(24, new Car("BMW", "GREY", 4));
		garage.add(25, new Car("Chevrolet", "RED", 5));
		garage.add(26, new Car("Porsche", "GREY", 3));
		garage.add(27, new Car("Porsche", "BLACK", 4));
		garage.add(28, new Car("Maybach", "BLUE", 4));
		garage.add(29, new Car("Maserati", "RED", 6));
		garage.add(30, new Car("Maybach", "RED", 3));
		garage.add(31, new Car("Maserati", "BLACK", 4));
		garage.add(32, new Car("Bugatti", "GREY", 5));
		garage.add(33, new Car("Ferrari", "YELLOW", 3));
		garage.add(34, new Car("Maybach", "YELLOW", 5));
		garage.add(35, new Car("Bugatti", "GREEN", 3));
	}
}
