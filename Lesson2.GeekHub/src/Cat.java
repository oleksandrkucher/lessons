import java.io.*;
import java.util.Arrays;

public class Cat {

	int rgbColor[] = new int[3];
	int age;
	
	public static void main(String[] args) {
		
		Cat cat1 = new Cat(Cat.readAge(), Cat.readColors());
		Cat cat2 = new Cat(Cat.readAge(), Cat.readColors());
		if(cat1.equals(cat2)){
			System.out.println("This cats are equals.");
		} else {
			System.out.println("This cats are not equals.");
		}
	}
	
	public Cat(int age, int rgbColor[]){
		
		this.age = age;
		this.rgbColor = rgbColor;
		
	}
	
	public static int readAge(){
		
		int age = 0;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Input cat's age: ");
		try {
			age = Integer.parseInt(buf.readLine());
		} catch (NumberFormatException | IOException e) {
			//e.printStackTrace();
			return readAge();
		}
		return age;
	}
	
	public static int[] readColors(){
		
		int color[] = new int[3];
		System.out.println("Input cat's colors:");
		for(int i = 0; i < 3; i++){
			color[i] = readOneColor();
		}
		
		return color;
	}
	
	private static int readOneColor(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		int color = 0;
		System.out.print("Input color: ");
		try {
			color = Integer.parseInt(buf.readLine());
		} catch (NumberFormatException e) {
			return readOneColor();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return color;
		
	}

	public boolean equals(Object cat){
		
		Cat newCat = (Cat) cat;
		if(this == cat){
			return true;
		} else if(cat instanceof Cat) {
			if(this.age == newCat.age && Arrays.equals(newCat.rgbColor, this.rgbColor)){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}
	
	public String toString(){
		
		String str = "Age: ";
		str = str + this.age + ". Color: ";
		for(int i = 0; i < 3; i++){
			str = str + this.rgbColor[i] + " ";
		}
		
		return str;
	}
	
	public int hashCode(){
		
		return this.toString().hashCode();
	}

}
