package Votes;

public class Vote {

	String name;
	
	public Vote(String name){
		
		this.name = name;	
	}
	
	public String toString(){
		
		return "Name: " + name;
	}

}
