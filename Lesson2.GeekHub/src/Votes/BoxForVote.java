package Votes;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import Votes.Vote;

public class BoxForVote implements Collection<Vote>{

	private static final String nameTwo = "two";
	private static final String nameNoting = "nothing";
	private static List<Vote> list = new LinkedList<>();	
	
	public static void main(String[] args){
		
		BoxForVote box = new BoxForVote();
		for(int i = 0; i < 5; i++){
			Vote vote = new Vote(box.getName());
			box.add(vote);
		}
		System.out.println("Your votes:");
		for(Vote votes : list){
			System.out.println(votes);
		}
	}

	public String getName(){
		
		System.out.print("Input name: ");
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String name = null;
		try{
			name = buf.readLine();
		} catch (IOException e){
			System.out.println("ERROR! " + e);
		}
		
		return name;
	}

	@Override
	public boolean add(Vote vote) {
		
		if(vote.name.equalsIgnoreCase(new Vote(nameTwo).name)){
			list.add(vote);
			list.add(vote);
		} else if(vote.name.equalsIgnoreCase(new Vote(nameNoting).name) != true){
			list.add(vote);
		}
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

}
