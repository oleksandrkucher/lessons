package BeanRepresenterAndAnnotation;

import java.lang.annotation.*;

/**
 * Annotation sets the status of the field (not represent) etc
 * 
 * @author kol
 *
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
	String status();
}