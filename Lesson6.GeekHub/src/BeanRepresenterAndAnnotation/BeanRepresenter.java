package BeanRepresenterAndAnnotation;

import java.lang.reflect.Field;

public class BeanRepresenter {

	public static void main(String[] args) {
		
		Cat cat = new Cat("Cat", 5, 4, "grey");
		Car car = new Car("red", "Galardo", 4);
		Human human = new Human("Tom", 19, car, cat);
		BeanRepresenter bean = new BeanRepresenter();
		bean.printInfoClass(human, "");
	}
	
	/**
	 * Method gets information about fields of class and prints it in console
	 * @param obj - object which info must get method
	 * @param str  - "\t"
	 */
	
	public void printInfoClass(Object obj, String str){
		
		@SuppressWarnings("rawtypes")
		Class clazz = obj.getClass();
		String nameClass = clazz.getSimpleName();
		Field[] fields = clazz.getDeclaredFields();
		
		System.out.println(str + nameClass);
		str += "\t"; 
		Field.setAccessible(fields, true);
		try {
			for(Field f: fields){
				MyAnnotation myAnn = f.getAnnotation(MyAnnotation.class);
				if (!(myAnn != null && myAnn.status().equals("not represent"))) {
					if (!f.getType().isInstance(toString())
							&& !f.getType().isPrimitive()) {
						printInfoClass(f.get(obj), str);
					} else if (f.getType().isInstance(toString())) {
						System.out.println(str + "String\t" + f.getName()
								+ "\t" + f.get(obj));
					} else {
						System.out.println(str + f.getType() + "\t"
								+ f.getName() + "\t" + f.get(obj));
					}
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			System.out.println("ERROR! " + e);
		}
	}

}
