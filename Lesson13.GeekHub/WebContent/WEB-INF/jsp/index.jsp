<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 13. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>Main page</h1>
		<a href="/Lesson13.GeekHub/listUsers.html">Users (${users})</a> <br> <br> <br>
		<a href="/Lesson13.GeekHub/listTickets.html">Tickets (${tickets})</a>
	</center>
</body>
</html>