<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
	<script>
		$(document).ready(function(){
			$("#form").validate({
				rules:{
					title: {
						required: true,
						minlength: 5,
				        maxlength: 20,
					},
					description: {
						required: true,
						minlength: 10,
					},
				}, 
			    messages: {
			    	title: {
				        required: "Input title of your description!",
				        minlength: "Minimum 5 characters",
				        maxlength: "Maximum 20 characters",
				    },
					description: {
						required: "Describe your problem.",
				        minlength: "Your problem very short. Min length 10 charapters",
					},
				},
				errorPlacement: function(error, element) {
					if (element.attr("name") == "title"){
						error.insertAfter($("input[name=title]"));
					}
					if (element.attr("name") == "description"){
						error.insertAfter($("input[name=description]"));
					}
				}
			});
			$("input.submit").click(function() {
				if($("#form").valid()){
					return true;
				} else {
					alert("Do you not see?! You do not enter correct values in all fields!\nReturn and do this!!!");
				}
			});
		});
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 13. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}

.error{
	color: red;
}
.valid{
	color: green;
}
#title{ 
	min-width: 100px;
}
#value{
	max-width: 300px;
}
</style>
</head>
<body>
	<center>
		<h1>NewTicket</h1>
		<form id="form" action="/Lesson13.GeekHub/saveTicket.html">
			<table border = 1 width = 400 height = 200>
				<tr>
					<td id="title">Title</td> 
					<td id="value"><input type="text" value="${ticket.title}" name="title" size = 40></td>
				</tr>
				<tr>
					<td id="title">Description</td>
					<td id="value"><input type="text" value="${ticket.description}" name="description" size = 40></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><select name="status">
						<c:forEach items="${status}" var="ticketStatus">
							<c:choose>
 								<c:when test="${ticketStatus == ticket.status}">
									<option selected value="${ticketStatus}">${ticketStatus}</option>
 								</c:when>
								<c:otherwise>
									<option value="${ticketStatus}">${ticketStatus}</option>
 								</c:otherwise>
 							</c:choose>
						</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Priority</td>
					<td><select name="priority">
						<c:forEach items="${priority}" var="ticketPriority">
							<c:choose>
 								<c:when test="${ticketPriority == ticket.priority}">
									<option selected value="${ticketPriority}">${ticketPriority}</option>
 								</c:when>
								<c:otherwise>
									<option value="${ticketPriority}">${ticketPriority}</option>
 								</c:otherwise>
 							</c:choose>
						</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Owner</td>
					<td><select name="ownerId">
						<c:forEach items="${users}" var="user">
							<c:choose>
 								<c:when test="${user.id == ticket.owner.id}">
									<option selected value="${user.id}">${user.name} (${user.id})</option>
 								</c:when>
								<c:otherwise>
									<option value="${user.id}">${user.name} (${user.id})</option>
 								</c:otherwise>
 							</c:choose>
						</c:forEach>
					</select></td>
				</tr>
			</table>
			<input type="hidden" name="id" value="${ticket.id}"><br>
			<input class="submit" type="submit" value="Save">
		</form>
		<br>
		<br> <a href="/Lesson13.GeekHub/index.html">Main Page</a>
	</center>
</body>
</html>