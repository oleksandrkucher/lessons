<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 13. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>ListTickets</h1>
		<table border="1" width="700">
			<tr>
				<th>Title</th>
				<th>Description</th>
				<th>Owner</th>
				<th>Status</th>
				<th>Priority</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach items="${tickets}" var="ticket">
				<tr>
					<td>${ticket.title}</td>
					<td>${ticket.description}</td>
					<td>${ticket.owner.name}</td>
					<td>${ticket.status}</td>
					<td>${ticket.priority}</td>
					<td><a href="/Lesson13.GeekHub/loadTicket.html?id=${ticket.id}">Edit</a></td>
					<td style="color: red;"><a href="/Lesson13.GeekHub/deleteTicket.html?id=${ticket.id}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<br> <a href="/Lesson13.GeekHub/loadTicket.html">Create New Ticket</a>
		<br>
		<br> <a href="/Lesson13.GeekHub/index.html">Main Page</a>
	</center>
</body>
</html>