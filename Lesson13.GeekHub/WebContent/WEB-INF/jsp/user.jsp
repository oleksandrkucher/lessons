<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
	<script>
		$(document).ready(function(){
			$("#form").validate({
				rules:{
					name: {
						required: true,
						minlength: 4,
				        maxlength: 12,
					},
					login: {
						required: true,
						email: true,
					},
					password: {
						required: true,
						minlength: 6,
				        maxlength: 20,
					},
				}, 
			    messages: {
			    	name: {
				        required: "Input your name!",
				        minlength: "Minimum 4 characters",
				        maxlength: "Maximum 12 characters",
				    },
					login: {
						required: "Input your login!",
						email: "Your login = your email. Input Your email!",
					},
					password: {
						required: "Input your password!",
						minlength: "Minimum 6 characters",
				        maxlength: "Maximum 20 characters",
					},
				},
				errorPlacement: function(error, element) {
					if (element.attr("name") == "name"){
						error.insertAfter($("input[name=name]"));
					}
					if (element.attr("name") == "login"){
						error.insertAfter($("input[name=login]"));
					}
					if (element.attr("name") == "password"){
						error.insertAfter($("input[name=password]"));
					}
				}
			});
			$("input.submit").click(function() {
				if($("#form").valid()){
					return true;
				} else {
					alert("Do you not see?! You do not enter correct values in all fields!\nReturn and do this!!!");
				}
			});
		});
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 13. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}

.error{
	color: red;
}
.valid{
	color: green;
}
#title{
	min-width: 100px;
}
#value{
	max-width: 300px;
}
</style>
</head>
<body>
	<center>
		<h1>NewUser</h1>
		<form id="form" action="/Lesson13.GeekHub/saveUser.html">
			<table border = 1 width = 400 height = 200>
				<tr>
					<td id="title">Name</td>
					<td id="value"><input type = "text" value = "${user.name}" name = "name" size = 40></td>
				</tr>
				<tr>
					<td id="title">Login (Email)</td>
					<td id="value"><input type = "text" value = "${user.login}" name = "login" size = 40></td>
				</tr>
				<tr>
					<td id="title">Password</td>
					<td id="value"><input type = "text" value = "${user.password}" name = "password" size = 40></td>
				</tr>
			</table>	
			<input type = "hidden" 	value = "${user.id}" name = "id" ><br>
			<input class="submit" type = "submit" value = "Save">
		</form>
		<br>
		<br> <a href="/Lesson13.GeekHub/index.html">Main Page</a>
	</center>
</body>
</html>