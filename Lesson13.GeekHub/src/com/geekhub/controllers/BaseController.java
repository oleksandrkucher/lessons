package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.services.TicketDAO;
import com.geekhub.services.UserDAO;

/**
 * Controller for work with main page - "Index.html"
 */
@Controller
public class BaseController {
	
	@Autowired TicketDAO ticketDAO;
	@Autowired UserDAO userDAO;
	
	/**
	 * Show index.html with information about tables "User" and "Ticket" (record count)
	 * Process request "/index.html"
	 * @param map
	 * @return - name of jsp page
	 */
	@RequestMapping(value="index.html")
	public String index (ModelMap map) {
		map.put("users", userDAO.getUserList().size());
		map.put("tickets", ticketDAO.getTicketList().size());
		return "index";
	}
}