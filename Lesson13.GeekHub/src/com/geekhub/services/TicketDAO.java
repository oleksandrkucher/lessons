package com.geekhub.services;

import java.util.List;

import com.geekhub.entity.Ticket;

public interface TicketDAO {

	/**
	 * Gets list of tickets from database (table "Ticket")
	 * @return List<>
	 */
	public List<Ticket> getTicketList ();
	
	/**
	 * Gets one record from database by id of it (table "Ticket")
	 * @param id
	 * @return Ticket
	 */
	public Ticket getTicketById (Integer id);
	
	/**
	 * Delete one record from database (table "Ticket") by id
	 * @param id
	 */
	public void deleteTicket (Integer id);
	
	/**
	 * Save or update one record in database (table "Ticket")
	 * @param ticket - record for save or update
	 * @param ownerId - id of ticket`s owner
	 */
	public void saveTicket (Ticket ticket, Integer ownerId);
}
