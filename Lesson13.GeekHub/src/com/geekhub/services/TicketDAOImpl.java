package com.geekhub.services;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.entity.Ticket;
import com.geekhub.entity.User;

/**
 * Implementation of TicketDAO
 */
@Repository
@Transactional
public class TicketDAOImpl implements TicketDAO {

	@Autowired SessionFactory sessionFactory;
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Ticket> getTicketList() {
		return sessionFactory.getCurrentSession().createQuery("from Ticket").list ();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Ticket getTicketById(Integer id) {
		return (Ticket)sessionFactory.getCurrentSession().get(Ticket.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteTicket(Integer id) {
		Ticket ticket = getTicketById(id);
		if (null != ticket){
			User owner = ticket.getOwner();
			if(owner != null && owner.getTickets() != null){
				owner.getTickets().remove(ticket);
				sessionFactory.getCurrentSession().update(owner);
			}
			sessionFactory.getCurrentSession().delete(ticket);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveTicket(Ticket ticket, Integer ownerId) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<User> users = session.createCriteria(User.class).list();
		User user = null;
		for (User g: users){
			if (g.getId() == ownerId){
				user = g; 
			}
		}
		List<Ticket> tickets = user.getTickets();
		for(Ticket t : tickets){
			if(t != null && t.getId() == ticket.getId()){
				tickets.remove(t);
				session.update(user);
				break;
			}
		}
		session.getTransaction().commit();
		session.close();
		session = sessionFactory.openSession();
		session.beginTransaction();
		user.getTickets().add(ticket);
		session.saveOrUpdate(user);
		session.getTransaction().commit();
		session.close();
	}
}