package com.geekhub.services;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.entity.Ticket;
import com.geekhub.entity.User;

/**
 * Implementation of UserDAO
 */
@Repository
@Transactional
public class UserDAOImpl implements UserDAO {
	
	@Autowired SessionFactory sessionFactory;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserList() {
		return sessionFactory.getCurrentSession().createQuery("from User").list ();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User getUserById(Integer id) {
		return (User)sessionFactory.getCurrentSession().get(User.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteUser(Integer id) {
		User user = getUserById(id);
		if (null != user){
			sessionFactory.getCurrentSession().delete(user);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveUser(User user) {
		user.setTickets(new LinkedList<Ticket>());
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}
}