<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 10. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>Lesson 10. GeekHub</h1>
		<a href="/Lesson10.GeekHub/ServletManager?action=showUser">Show Users</a> <br> <br> 
		<a href="/Lesson10.GeekHub/ServletManager?action=showGroup">Show Groups</a> <br> <br>
	</center>
</body>
</html>