<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 10. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>Lesson 10. GeekHub</h1>
		<c:set var="valuesSession" value="${sessionScope}"></c:set>
		<c:forEach var="vs" items="${valuesSession}">
			<c:if test="${vs.key != null && vs.value != null}">
				<c:choose>
    				<c:when test="${vs.key == 'value'}">
						<c:set var="value" value="${vs.value}"></c:set>
    				</c:when>
    				<c:when test="${vs.key == 'groups'}">
						<c:set var="groups" value="${vs.value}"></c:set>
    				</c:when>
				</c:choose>
			</c:if>
		</c:forEach>

		<h2>Add/Update User</h2>
		<form action="/Lesson10.GeekHub/ServletManager">
			<input type='hidden' name='action' value='addUser'>
			<input type='hidden' name='id' value='${val.id}'>
			<table border=1 height=300 width=500>
				<tr>
					<th>Name</th>
					<td><input type='text' name='name' value='${value.name}' size="30"></td>
				</tr>
				<tr>
					<th>Group</th>
					<td>
						<select name = "groupId">
							<option disabled>Choose a group</option>
							<c:forEach var="val" items="${groups}">
								<c:if test="${value.group.id == val.id}">
									<option selected value = "${val.id}">${val.id}. ${val.name}</option>
								</c:if>
								<c:if test="${value.group.id != val.id}">
									<option value = "${val.id}">${val.id}. ${val.name}</option>
								</c:if>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>Login</th>
					<td><input type='text' name='login' value='${value.login}' size="30"></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><input type='text' name='email' value='${value.email}' size="30"></td>
				</tr>
				<tr>
					<th>Password</th>
					<td><input type='text' name='password' value='${value.password}' size="30"></td>
				</tr>
				<tr>
					<td><input type='submit' value='ADD User'></td>
				</tr>
			</table>
		</form>
		<br> <br>
		<a href="/Lesson10.GeekHub/ServletManager">Main</a>
	</center>
</body>
</html>