<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 10. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>Lesson 10. GeekHub</h1>
		<c:set var="valuesSession" value="${sessionScope}"></c:set>
		<c:forEach var="vs" items="${valuesSession}">
			<c:if test="${vs.key != null && vs.value != null}">
				<c:choose>
    				<c:when test="${vs.key == 'list'}">
						<c:set var="list" value="${vs.value}"></c:set>
    				</c:when>
				</c:choose>
			</c:if>
		</c:forEach>
		<h2>Show Users</h2>
		<table border="1" width="500">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Group</th>
				<th>Login</th>
				<th>Email</th>
				<th>Password</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach items="${list}" var="val">
				<tr>
					<td>${val.id}</td>
					<td>${val.name}</td>
					<td>
						<c:if test="${val.group != null}">
							${val.group.name}
						</c:if>
					</td>
					<td>${val.login}</td>
					<td>${val.email}</td>
					<td>${val.password}</td>
					<td><a href="/Lesson10.GeekHub/ServletManager?action=updateUser&id=${val.id}">Edit</a></td>
					<td style="color: red;"><a href="/Lesson10.GeekHub/ServletManager?action=deleteUser&id=${val.id}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
		
		<br> <br> <a href="/Lesson10.GeekHub/addUser.jsp">Add user</a>
		<br> <br> <a href="/Lesson10.GeekHub/ServletManager">Main</a>
	</center>
</body>
</html>