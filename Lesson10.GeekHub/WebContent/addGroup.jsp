<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 10. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>Lesson 10. GeekHub</h1>
		<c:set var="valuesSession" value="${sessionScope}"></c:set>
		<c:forEach var="vs" items="${valuesSession}">
			<c:if test="${vs.key != null && vs.value != null}">
				<c:choose>
    				<c:when test="${vs.key == 'value'}">
						<c:set var="val" value="${vs.value}"></c:set>
    				</c:when>
				</c:choose>
			</c:if>
		</c:forEach>

		<h2>Add/Update Group</h2>
		<form action="/Lesson10.GeekHub/ServletManager">
			<input type='hidden' name='action' value='addGroup'>
			<input type='hidden' name='id' value='${val.id}'>
			<table border=1 height=100 width=500>
				<tr>
					<th>Name</th>
					<td><input type='text' name='name' value='${val.name}'></td>
				</tr>
				<tr>
					<td><input type='submit' value='ADD Group'></td>
				</tr>
			</table>
		</form>
		<br> <br>
		<a href="/Lesson10.GeekHub/ServletManager">Main</a>
	</center>
</body>
</html>