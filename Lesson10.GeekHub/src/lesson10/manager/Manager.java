package lesson10.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import lesson10.HibernateUtils;
import lesson10.entity.Group;
import lesson10.entity.User;

@SuppressWarnings("deprecation")
public class Manager {

	private static Manager instance;

	private Manager() {}

	/**
	 * Gets singleton of this class
	 * @return
	 */
	public static Manager getInstance() {
		if (instance == null)
			instance = new Manager();
		return instance;
	}

	/**
	 * Opens session, save or update one User into database and close session 
	 * @param user
	 */
	private void addOrUpdateOneUser(User user, int groupId) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Group> groups = session.createCriteria(Group.class).list();
		Group group = null;
		for (Group g: groups){
			if (g.getId() == groupId){
				group = g; 
			}
		}
		if (group!= null && group.getUsers() != null){
			group.getUsers().add(user);
			session.saveOrUpdate(group);
		} else {
			session.saveOrUpdate(user);
		}
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Opens session, save or update one Group into database and close session
	 * @param group
	 */
	private void addOrUpdateOneGroup(Group group) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		if (group.getUsers() == null){
			group.setUsers(new ArrayList<User>());
		}
		session.saveOrUpdate(group);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Opens session, delete one User from database by ID and close session
	 * @param user
	 */
	private void deleteOneUser(User user) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		if (user.getGroup() != null && user.getGroup().getUsers() != null){
			user.getGroup().getUsers().remove(user);
		}
		session.delete (user);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Opens session, delete one Group from database by ID and close session
	 * @param group
	 */
	private void deleteOneGroup(Group group) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		List<User> users = group.getUsers();
		if (users != null && users.size()>0){
			for (User u: users){
				u.setGroup(null);
			}
		}
		session.delete(group);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Gets all users from table User and create List with User-object
	 * @return
	 */
	private List<User> getUsers() {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<User> users = session.createCriteria(User.class).list();
		session.getTransaction().commit();
		session.close();
		return users;
	}
	
	/**
	 * Gets all groups from table Group and create List with Group-object
	 * @return
	 */
	private List<Group> getGroups() {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Group> groups = session.createCriteria(Group.class).list();
		session.getTransaction().commit();
		session.close();
		return groups;
	}
	
	/**
	 * Gets one user from table User with 'id'==id 
	 * @param id - id of element in table
	 * @return
	 */
	private User getUserById(Integer id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		User user = (User) session.createCriteria(User.class).add(Expression.eq("id", id)).list().get(0);
		session.getTransaction().commit();
		session.close();
		return user;
	}
	
	/**
	 * Gets one group from table Group with 'id'==id 
	 * @param id - id of element in table
	 * @return
	 */
	private Group getGroupById(Integer id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		Group group = (Group) session.createCriteria(Group.class).add(Expression.eq("id", id)).list().get(0);
		session.getTransaction().commit();
		session.close();
		return group;
	}
	
	/**
	 * Add or update one element in table User
	 * @param request
	 */
	public void addOrUpdateUser(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		int userId = 0;
		if (!"".equals(idStr) && idStr != null) {
			userId = Integer.parseInt(idStr);
		}
		String name = request.getParameter("name");
		String login = request.getParameter("login");
		String email = request.getParameter("email");
		String pass = request.getParameter("password");
		String groupId = request.getParameter("groupId");
		int id = 0;
		if (!"".equals(groupId) && groupId != null) {
			try {
				id = Integer.parseInt(groupId);
			} catch (NumberFormatException e) {}
		}
		User user = new User();
		if (userId > 0){
			user.setId(userId);
			request.removeAttribute("id");
		}
		user.setName(name);
		user.setLogin(login);
		user.setEmail(email);
		user.setPassword(pass);
		addOrUpdateOneUser(user, id);
	}
	
	/**
	 * Add or update one element in table Group
	 * @param request
	 */
	public void addOrUpdateGroup(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		int id = 0;
		if (!"".equals(idStr) && idStr != null) {
			id = Integer.parseInt(idStr);
		}
		String name = request.getParameter("name");
		Group group = new Group();
		if (id > 0){
			group.setId(id);
			request.removeAttribute("id");
		}
		group.setName(name);
		addOrUpdateOneGroup(group);
	}
	
	/**
	 * Delete one element from table User
	 * @param request
	 */
	public void deleteUser (HttpServletRequest request){
		String idStr = request.getParameter("id");
		int id = 0;
		if (idStr != null && !"".equals(idStr)) {
			try {
				id = Integer.parseInt(idStr);
			} catch (NumberFormatException e){}
			User user = getUserById(id);
			deleteOneUser(user);
		}
	}
	
	/**
	 * Delete one element from table User
	 * @param request
	 */
	public void deleteGroup (HttpServletRequest request){
		String idStr = request.getParameter("id");
		int id = 0;
		if (idStr != null && !"".equals(idStr)) {
			try {
				id = Integer.parseInt(idStr);
			} catch (NumberFormatException e){}
			Group group = getGroupById(id);
			deleteOneGroup(group);
		}
	}
	
	/**
	 * Gets value of fields of User or Group before updating it
	 * @param table - name of table
	 * @param session - current session
	 * @param request
	 * @param response
	 */
	public void getForUpdate(String table, HttpSession session, HttpServletRequest request, HttpServletResponse response){
		String idStr = request.getParameter("id");
		int id = 0;
		if (!"".equals(idStr) && idStr != null) {
			id = Integer.parseInt(idStr);
		}
		if ("user".equals(table)){
			User user = getUserById(id);
			session.setAttribute("value", user);
			try {
				request.getRequestDispatcher("addUser.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		} else if ("group".equals(table)){
			Group group = getGroupById(id);
			session.setAttribute("value", group);
			try {
				request.getRequestDispatcher("addGroup.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Show selected elements from table User or Group
	 * @param table name of table for creating view
	 * @param session
	 */
	public void show (String table, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		if ("user".equals(table)){
			List<User> users = getUsers();
			session.setAttribute("list", users);
			session.removeAttribute("value");
			session.setAttribute("groups", getGroups());
			try {
				request.getRequestDispatcher("showUser.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		} else if ("group".equals(table)){
			List<Group> groups = getGroups();
			session.setAttribute("list", groups);
			session.removeAttribute("value");
			try {
				request.getRequestDispatcher("showGroup.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		}
	}
}