package lesson10;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtils {
	private static final SessionFactory sessionFactory = configureSessionFactory();
	private static ServiceRegistry serviceRegistry;
	
	/**
	 * Gets configuration of Session Factory
	 * @return SessionFactory
	 * @throws HibernateException
	 */
	private static SessionFactory configureSessionFactory()
			throws HibernateException {
		Configuration configuration = new Configuration().configure();
		serviceRegistry = new ServiceRegistryBuilder().applySettings(
				configuration.getProperties()).buildServiceRegistry();
		return configuration.buildSessionFactory(serviceRegistry);
	}

	/**
	 * Gets Session Factory
	 * @return SessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}