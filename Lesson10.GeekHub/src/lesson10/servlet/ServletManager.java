package lesson10.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lesson10.manager.Manager;

/**
 * Servlet implementation class ServletManager
 */
public class ServletManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletManager() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		if (action != null){
			switch (action) {
				case "addUser":
					Manager.getInstance().addOrUpdateUser(request);
					Manager.getInstance().show("user", session, request, response);
					break;
				case "addGroup":
					Manager.getInstance().addOrUpdateGroup(request);
					Manager.getInstance().show("group", session, request, response);
					break;
				case "updateUser":
					Manager.getInstance().getForUpdate("user", session, request, response);
					break;
				case "updateGroup":
					Manager.getInstance().getForUpdate("group", session, request, response);
					break;
				case "showUser":
					Manager.getInstance().show("user", session, request, response);
					break;
				case "showGroup":
					Manager.getInstance().show("group", session, request, response);
					break;
				case "deleteUser":
					Manager.getInstance().deleteUser(request);
					Manager.getInstance().show("user", session, request, response);
					break;
				case "deleteGroup":
					Manager.getInstance().deleteGroup(request);
					Manager.getInstance().show("group", session, request, response);
					break;
				default:
					request.getRequestDispatcher("index.jsp").forward(request, response);
					break;
			}
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}
}