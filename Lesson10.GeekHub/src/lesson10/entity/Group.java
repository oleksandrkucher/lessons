package lesson10.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

/**
 * Class describes table Group in database
 *
 * @author kol
 */
@Entity
@Table(name="Group1")
public class Group {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(cascade={CascadeType.ALL},
			fetch = FetchType.EAGER)
	@JoinColumn(name="groupId")
	@IndexColumn(name="idx")
	private List<User> users;
	
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	/**
	 * Gets column 'id'
	 * @return integer value
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets column 'id'
	 * @param id integer value
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets column 'name'
	 * @return string value
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets column 'name'
	 * @param name string value
	 */
	public void setName(String name) {
		this.name = name;
	}
}