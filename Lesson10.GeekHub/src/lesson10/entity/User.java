package lesson10.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class describes table User in database
 * 
 * @author kol
 */
@Entity
@Table(name="User1")
public class User {


	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="groupId", 
				insertable=false, updatable=false, nullable=true)
	private Group group;
	
	@Column(name = "login")
	private String login;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	/**
	 * Gets column 'id'
	 * @return integer value
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Sets column 'id'
	 * @param id > 0
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Gets column 'name'
	 * @return string value
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets column 'id'
	 * @param name string value
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets column 'group'
	 */
	public Group getGroup() {
		return group;
	}
	
	/**
	 * Sets column 'group'
	 * @param group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * Gets column 'login'
	 * @return string value
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets column 'login'
	 * @param login string value
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Gets column 'email'
	 * @return string value
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets column 'email'
	 * @param email string value
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets column 'password'
	 * @return string value
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets column 'password'
	 * @param password string value
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}