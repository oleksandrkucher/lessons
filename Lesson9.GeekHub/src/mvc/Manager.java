package mvc;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Manager {

	/**
	 * Method makes ModelAndView for page index.jsp 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/example.page")
	protected ModelAndView page (HttpServletRequest request, HttpServletResponse response) throws Exception{
		getParamerersFromLinkAndSetToScope(request);
		ModelAndView mav = new ModelAndView("index");
		HttpSession session = request.getSession();
		Enumeration<String> names = session.getAttributeNames();
		HashMap<String, String> sessionValues = new HashMap<>();
		while (names.hasMoreElements()) {
			String key = (String) names.nextElement();
			String val = (String) session.getAttribute(key);
			sessionValues.put(key, val);
		}
		mav.addObject("values", sessionValues);
		return mav;
	}
	
	/**
	 * Method receives all parameters by method GET initializes variables and set values
	 * into session and servler context
	 * @param request
	 * @param response
	 */
	private void getParamerersFromLinkAndSetToScope(HttpServletRequest request){
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		HttpSession session = request.getSession();
		if (name != null) {
			if ("remove".equals(action)) {
				session.removeAttribute(name);
			} else if(value != null) {
				session.setAttribute(name, value);
			}
		}
	}
}