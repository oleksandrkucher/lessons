package Car.interfaces;

import Car.components.SteeringWheel;

public interface Steared {

	/**
	 * Turns car left or right 
	 * @param angle - angle of turning (can be and negative (turn left) positive (turn right))
	 * @param sw - steering wheel of this car
	 * @return - result angle of forward wheels
	 */
	public int turn(int angle, SteeringWheel sw);
}