package Car.interfaces;

public interface Retarding {
	
	/**
	 * Gets power of brake
	 * @param brakeType - brake pedal or hand brake of this car
	 * @return - power of selected brake
	 */
	int brakePower (Brake brakeType) ;
}