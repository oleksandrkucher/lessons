package Car.interfaces;

public interface Brake {
	
	/**
	 * Gets power of brake (hand brake and brake pedal)
	 * @return - power
	 */
	int getPower () ;
}