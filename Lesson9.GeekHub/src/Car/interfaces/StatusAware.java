package Car.interfaces;

public interface StatusAware {

	/**
	 * Prints status into console or another 
	 */
	public void printStatus();
}