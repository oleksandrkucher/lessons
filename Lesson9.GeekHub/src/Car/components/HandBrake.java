package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.Brake;
import Car.interfaces.StatusAware;

/**
 * Class describe hand brake in a real car
 * 
 * @author kol
 */
@Component
public class HandBrake implements StatusAware, Brake{

	final private int power = 10;
	private boolean status = false;
	
	/**
	 * {@inheritDoc}
	 * 
	 * Gets value which characterizes power of brake
	 * @return integer value
	 */
	public int getPower() {
		return power;
	}

	/**
	 * Press hand brake button (or another)
	 */
	public void pressHandBrake () {
		status = true;
	}
	
	/**
	 * Release hand brake button (or another)
	 */
	public void stopPressHandBrake () {
		status = false;
	}
	
	/**
	 * Gets status of hand brake
	 * @return boolean value
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Hand brake is using now.");
		} else {
			System.out.println ("Hand brake is unusing now.");
		}
	}
}