package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.Brake;
import Car.interfaces.Retarding;
import Car.interfaces.StatusAware;

/**
 * Class describe rear wheels in a real car
 * 
 * @author kol
 */
@Component
public class RearWheels implements StatusAware, Retarding{

	private int power = 0;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int brakePower(Brake brakeType) {
		power =  brakeType.getPower();
		return power;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		System.out.println("Power of brake = " + power + ".");
	}
}