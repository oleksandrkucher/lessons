package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.Brake;
import Car.interfaces.Retarding;
import Car.interfaces.StatusAware;
import Car.interfaces.Steared;

/**
 * Class describe forward wheels in a real car
 * 
 * @author kol
 */
@Component
public class ForwardWheels implements Retarding, StatusAware, Steared{
	
	private int angleOfWheels = 0;
	private int power = 0;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int turn(int angleSteeringWheel, SteeringWheel sw) {
		if (angleSteeringWheel > 0){
			sw.turnRight(angleSteeringWheel);
		} else if (angleSteeringWheel < 0){
			sw.turnLeft(Math.abs(angleSteeringWheel));
		}
		this.angleOfWheels = sw.getAngle() / 2;
		return this.angleOfWheels;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		System.out.println ("Angle of wheels = " + angleOfWheels + "; Power of brake = " + power + ".");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int brakePower(Brake brakeType) {
		power =  brakeType.getPower();
		return power;
	}
}
