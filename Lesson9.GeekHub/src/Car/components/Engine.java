package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class describe engine in a real car
 * 
 * @author kol
 */
@Component
public class Engine implements StatusAware{

	private boolean status = false;
	final private int maxSpeed = 300;
	
	/**
	 * Gets max speed of car with this engine
	 * @return integer value (km/h)
	 */
	public int getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * Engages engine in the car
	 */
	public void onEngine () {
		status = true;
	}
	
	/**
	 * Stops engine in the car
	 */
	public void offEngine () {
		status = false;
	}
	
	/**
	 * Gets status of engine
	 * @return boolean value
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Engine is active.");
		} else {
			System.out.println ("Engine is not active.");
		}
	}
}