package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class describe accelerate pedal in a real car
 * 
 * @author kol
 */
@Component
public class Accelerator implements StatusAware{

	private boolean status = false;
	
	/**
	 * Press accelerate pedal 
	 */
	public void pressPedal () {
		status = true;
	}
	
	/**
	 * Release accelerate pedal
	 */
	public void stopPressPedal () {
		status = false;
	}
	
	/**
	 * Gets status of accelerate pedal
	 * @return boolean value
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Accelerator pedal is pressed.");
		} else {
			System.out.println ("Accelerator pedal is unpressed.");
		}
	}
}