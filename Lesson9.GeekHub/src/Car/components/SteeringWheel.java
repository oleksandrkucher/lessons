package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class describe steering wheel in a real car
 * 
 * @author kol
 */
@Component
public class SteeringWheel implements StatusAware{
	
	final private int maxAngle = 120;
	private int angle = 0;
	
	/**
	 * Turns steering wheel left 
	 * @param angle - grade of turning
	 */
	public void turnLeft(int angle){
		this.angle -= angle;
		if (Math.abs(this.angle) > maxAngle){
			this.angle = -maxAngle;
		}
	}
	
	/**
	 * Turns steering wheel right 
	 * @param angle - grade of turning
	 */
	public void turnRight(int angle){
		this.angle += angle;
		if (this.angle > maxAngle){
			this.angle = maxAngle;
		}
	}

	/**
	 * Gets current angle of turning of steering wheel
	 * @return - angle (grades)
	 */
	public int getAngle() {
		return angle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		if (angle < 0){
			System.out.println("Car turning left.");
		} else if (angle > 0){
			System.out.println("Car turning right.");
		}
		System.out.println ("Angle of steering wheel = " + angle + ".");
	}
}