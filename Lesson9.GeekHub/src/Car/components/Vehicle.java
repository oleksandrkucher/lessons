package Car.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class describe vehicle in a real car
 * 
 * @author kol
 */
@Component
public class Vehicle implements StatusAware{

	@Autowired private ControlPanel control;
	
	/**
	 * Gets Control Panel for control of the car
	 * @return Control Panel
	 */
	public ControlPanel getControl() {
		return control;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		System.out.println ();
	}
}