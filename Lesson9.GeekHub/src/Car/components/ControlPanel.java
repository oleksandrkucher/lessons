package Car.components;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class control all elements of car
 * 
 * @author kol
 */
@Component
public class ControlPanel implements StatusAware{
	
	@Autowired private Accelerator accelerator;
	@Autowired private BrakePedal brakePedal;
	@Autowired private Engine engine;
	@Autowired private ForwardWheels fw;
	@Autowired private GasTank gasTank;
	@Autowired private HandBrake handBrake;
	@Autowired private Horn horn;
	@Autowired private RearWheels rw;
	@Autowired private SteeringWheel sw;
	
	volatile private double currentSpeed;
	volatile private double currentVolume = 0;
	volatile private int currentAngle;
	volatile private double distance = 0;

	
	volatile private boolean stoping = false;
	private long startTime;
	
	/**
	 * Initializes all elements of the car
	 */
	@PostConstruct
	public void init (){
		startTime = new Date().getTime(); 
		System.out.println ("======================================================");
		accelerator.printStatus();
		brakePedal.printStatus();
		engine.printStatus();
		fw.printStatus();
		gasTank.printStatus();
		handBrake.printStatus();
		horn.printStatus();
		rw.printStatus();
		sw.printStatus();
		System.out.println ("======================================================\n");
	}
	
	/**
	 * Engages engine of the car
	 */
	public void start () {
		if (!engine.isStatus()) {
			engine.onEngine();
			currentSpeed = 0;
			currentAngle = 0;
			engine.printStatus();
			System.out.println();
			this.printStatus();
			this.drive();
		} else {
			System.out.println("Engine is active, you can not to start it!");
		}
	}
	
	/**
	 * Stops the car and turn off the engine
	 */
	public void stop () {
		if (engine.isStatus()) {
			stoping = true;
			while(currentSpeed > 0){
				useBrakePedal(2000);
				try {
					Thread.sleep(2100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				showSpeed();
			}
			engine.offEngine();
			currentSpeed = 0;
			engine.printStatus();
			this.printStatus();
			System.out.println ("Time of work of the car = " + (new Date().getTime() - startTime) / 1000 + " seconds");
			stoping = false;
		} else {
			System.out.println ("Engine is not active, you can not to stop it!");
		}
	}
	
	/**
	 * Stops engine and show message when you have no gas
	 */
	private void noFuel () {
		engine.offEngine();
		engine.printStatus();
		System.out.println ("You have not fuel. HA-HA-HA...");
	}
	
	/**
	 * Use horn of the car during 3 seconds
	 */
	public void useHorn (){
		Thread thread = new Thread() {
			public void run(){
				horn.pressHornButton();
				horn.printStatus();
				System.out.println ();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (engine.isStatus()){
					horn.stopPressHornButton();
					horn.printStatus();
					System.out.println ();
				}
			}
		};
		thread.start();
	}

	/**
	 * Pour gas into the gas tank
	 * @param volume - volume of new gas
	 */
	public void addFuel (double volume) {
		if (!engine.isStatus()){
			gasTank.pourGas(volume);
			currentVolume = gasTank.getCurrentVolume();
			gasTank.printStatus();
		} else {
			System.out.println ("You must stop the car before adding fuel!!!");
		}
		System.out.println ();
	}

	/**
	 * Use gas from gas tank for work of engine
	 * @param volume - volume of gas which using in current moment of time
	 */
	private void useFuel (final double volume) {
		if (engine.isStatus()) {
			gasTank.useGas(volume);
			currentVolume = gasTank.getCurrentVolume();
			if (currentVolume <= 0) {
				currentVolume = 0;
				noFuel();
			}
			gasTank.printStatus();
		}
	}

	/**
	 * Speed up the car	 owing to using accelerate pedal during some time
	 * @param timeSpeedUp - time of using accelerate pedal
	 */
	public void speedUp (final long timeSpeedUp) {
		Thread thread = new Thread () {
			public void run () {
				long currentTime = 0;
				if (currentSpeed < engine.getMaxSpeed() && engine.isStatus()) {
					accelerator.pressPedal();
					accelerator.printStatus();
					System.out.println ();
					while (engine.isStatus()
							&& currentSpeed <= engine.getMaxSpeed()
							&& currentTime < timeSpeedUp) {
						useFuel(0.01);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						currentTime += 1000;
						currentSpeed += 20;
						if (currentSpeed > engine.getMaxSpeed()) {
							currentSpeed = engine.getMaxSpeed();
						}
						showSpeed();
					}
					if (engine.isStatus()) {
						accelerator.stopPressPedal();
						accelerator.printStatus();
						printStatus();
					}
				} else {
					System.out.println("Speed is maximum now.");
					printStatus();
				}
			}
		};
		thread.start () ;
	}
	
	/**
	 * Prints current speed of car into console
	 */
	public void showSpeed () {
		System.out.println("Current speed = " + currentSpeed + " km/h\n");
	}
	
	/**
	 * Slow down the car owing to using brake pedal during some time
	 * @param timeSpeedDown - time of using brake pedal
	 */
	public void useBrakePedal (final long timeSpeedDown) {
		Thread thread = new Thread () {
			public void run () {
				long currentTime = 0;
				if (currentSpeed > 0 && engine.isStatus()) {
					if (!stoping){
						brakePedal.pressPedal();
						brakePedal.printStatus();
						System.out.println();
					}
					while (engine.isStatus() && currentSpeed >= 0
							&& currentTime < timeSpeedDown) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						currentTime += 1000;
						if (!stoping){
							fw.printStatus();
						}
						currentSpeed -= fw.brakePower(brakePedal);
						if (currentSpeed < 0) {
							currentSpeed = 0;
						}
						if (!stoping){
							showSpeed();
						}
					}
					if (engine.isStatus() && !stoping) {
						brakePedal.stopPressPedal();
						brakePedal.printStatus();
						printStatus();
					}
				} else {
					System.out.println("Car is stoped.");
					printStatus();
				}
			}
		};
		thread.start () ;
	}
	
	/**
	 * Slow down the car owing to using hand brake during some time
	 * @param timeSpeedDown - time of using hand brake
	 */
	public void useHandBrake (final long timeSpeedDown) {
		Thread thread = new Thread() {
			public void run() {
				long currentTime = 0;
				if (currentSpeed > 0 && engine.isStatus()) {
					handBrake.pressHandBrake();
					handBrake.printStatus();
					System.out.println ();
					while (engine.isStatus() && currentSpeed >= 0
							&& currentTime < timeSpeedDown) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						currentTime += 1000;
						rw.printStatus();
						currentSpeed -= rw.brakePower(handBrake);
						if (currentSpeed < 0) {
							currentSpeed = 0;
						}
						showSpeed();
					}
					if (engine.isStatus()) {
						handBrake.stopPressHandBrake();
						handBrake.printStatus();
						printStatus();
					}
				} else {
					System.out.println("Car is stoped.");
					printStatus();
				}
			}
		};
		thread.start () ;
	}
	
	/**
	 * Turns car left or right owing to using steering wheel
	 * @param angle - angle of turning of steering wheel
	 */
	public void turning (final int angle) {
		Thread thread = new Thread() {
			public void run() {
				if (currentSpeed > 0 && engine.isStatus()) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (engine.isStatus()) {
						fw.turn(angle, sw);
						currentAngle = sw.getAngle();
						sw.printStatus();
						currentSpeed -= 5;
						if (currentSpeed < 0) {
							currentSpeed = 0;
						}
						printStatus();
					}
				} else {
					System.out.println("Car is stoped.");
					printStatus();
				}
			}
		};
		thread.start () ;
	}

	/**
	 * Controls car and using of gas when engine is active
	 */
	private void drive () {
		final double usingGas = 5;
		Thread thread = new Thread () {
			public void run() {
				while (engine.isStatus()){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (currentSpeed > 0) {
						useFuel(currentSpeed * (usingGas/100/1000));
						distance += currentSpeed;
					} else {
						useFuel(0.01);
					}
				}
			}
		};
		thread.start();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		System.out.println ("Current speed = " + currentSpeed + " km/h; current volume = " + currentVolume 
				+ " l; current angle = " + currentAngle + "; distance = " + distance + " m\n");
	}
}