package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class describe horn in a real car
 * 
 * @author kol
 */
@Component
public class Horn implements StatusAware{
	
	private boolean status = false;
	
	/**
	 * Press horn button (or another)
	 */
	public void pressHornButton () {
		status = true;
	}
	
	/**
	 * Release horn button (or another)
	 */
	public void stopPressHornButton () {
		status = false;
	}
	
	/**
	 * Gets status of horn
	 * @return boolean value
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Horn is using now.");
		} else {
			System.out.println ("Horn is unusing now.");
		}
	}
}