package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.StatusAware;

/**
 * Class describe gas tank in a real car
 * 
 * @author kol
 */
@Component
public class GasTank implements StatusAware{
	
	final private double maxVolume = 50;
	private double currentVolume = 0;
	
	/**
	 * Pours some volume of gas into gas tank
	 * @param volume - volume of gas which you want to pour into gas tank
	 */
	public void pourGas(double volume){
		if(volume < maxVolume){
			this.currentVolume += volume;
		}
		if(this.currentVolume > maxVolume){
			this.currentVolume = maxVolume;
		}
	}
	
	/**
	 * Uses some volume of gas in gas tank
	 * @param volume - volume which using in this moment of time
	 * @return - new volume of gas
	 */
	public double useGas(double volume){
		if(volume > 0){
			this.currentVolume -= volume;
		}
		if(this.currentVolume <= 0){
			this.currentVolume = 0;
		}
		return currentVolume;
	}
	
	/**
	 * Gets current volume of gas in gas tank
	 * @return volume (litres)
	 */
	public double getCurrentVolume () {
		return currentVolume;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		System.out.println ("Current volume is " + currentVolume + ".");
	}
}
