package Car.components;

import org.springframework.stereotype.Component;

import Car.interfaces.Brake;
import Car.interfaces.StatusAware;

/**
 * Class describe brake pedal in a real car
 * 
 * @author kol
 */
@Component
public class BrakePedal implements StatusAware, Brake{

	final private int power = 20;
	private boolean status = false;
	
	/**
	 * {@inheritDoc}
	 * 
	 * Gets value which characterizes power of brake
	 * @return integer value
	 */
	public int getPower() {
		return power;
	}
	
	/**
	 * Press brake pedal
	 */
	public void pressPedal () {
		status = true;
	}
	
	/**
	 * Release brake pedal
	 */
	public void stopPressPedal () {
		status = false;
	}
	
	/**
	 * Gets status of brake pedal
	 * @return boolean value
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Brake pedal is pressed.");
		} else {
			System.out.println ("Brake pedal is unpressed.");
		}
	}
}