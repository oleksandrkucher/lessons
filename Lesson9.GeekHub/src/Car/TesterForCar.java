package Car;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import Car.components.ControlPanel;
import Car.components.Vehicle;

/**
 * Class for testing the car
 * 
 * @author kol
 */
public class TesterForCar {

	public static void main(String[] args) throws InterruptedException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Vehicle vehicle = context.getBean(Vehicle.class);
        
        ControlPanel cp = vehicle.getControl();
        
        //--------------------------------------------
        //Car works
        cp.addFuel(10);
        cp.start();
        cp.speedUp(10000);
        Thread.sleep(10100);
        cp.useHorn();
        Thread.sleep(3100);
        cp.useBrakePedal(3000);
        Thread.sleep(3100);
        cp.turning(45);
        Thread.sleep(2100);
        cp.speedUp(5000);
        Thread.sleep(5100);
        cp.turning(-45);
        Thread.sleep(2100);
        cp.useHandBrake(8000);
        Thread.sleep(8100);
        cp.showSpeed();
        cp.stop();
	}
}