<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Lesson 9. MVC</title>
	</head>
	<center>
		<h1>Lesson 9. GeekHub</h1>
		<h2>With MVC</h2>
		<form action = "/Lesson9.GeekHub/example.page">
			<table border = 1 height = 500 width = 1000>
				<tr>
					<th>Name:</th><th>Value:</th><th>Action</th>
				</tr>
					<c:forEach var="vs" items="${values}">
						<tr>
							<td>${vs.key}</td>
							<td>${vs.value}</td>
							<td><a href="example.page?action=remove&name=${vs.key}">delete</a></td>
						</tr>
  					</c:forEach>
				<tr>
					<td><input type = 'text' name = 'name' value = ''></td>
					<td><input type = 'text' name = 'value' value = ''></td>
					<td><input type = 'submit' value = 'add'></td>
				</tr>
			</table>
		</form>
	</center>
</html>