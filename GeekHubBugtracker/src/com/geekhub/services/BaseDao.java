package com.geekhub.services;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import com.geekhub.beans.Entity;
import com.geekhub.beans.Ticket;
import com.geekhub.beans.TicketPriority;
import com.geekhub.beans.TicketStatus;
import com.geekhub.beans.User;

/**
 * Class includes methods for work with database
 * 
 * @author kol
 */
public class BaseDao extends JdbcTemplate {

	/**
	 * Gets one record from table in database and sets as values of new object (extends Entity)
	 * Universal method for all objects (which extends Entity); with using reflection
	 * Use this method when one or more fields of your object which extends Entity, extends Entity too
	 * @param clazz
	 * @param id - id of selected record
	 * @return new object extends Entity
	 */
	public <T extends Entity> T getReflect(final Class<T> clazz, int id){
		final T object = (T) queryForObject("SELECT * FROM " + clazz.getSimpleName() + " WHERE id = ?", 
				new Object[] {id}, new BeanPropertyRowMapper<>(clazz));
		Field[] fields = clazz.getDeclaredFields();
		Field.setAccessible(fields, true);
		boolean haveEntityField = false;
		try {
			for (final Field f: fields){
				if (f.getType().getSuperclass() == Entity.class){
					String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = ?";
			        queryForObject(sql, new Object[] {id}, new RowMapper<T>() {
			            @Override
			            public T mapRow(ResultSet rs, int i) throws SQLException {
			            	int entityId = rs.getInt(f.getName() + "Id");
			                @SuppressWarnings("unchecked")
							List<T> entityObject = (List<T>) query("SELECT * FROM " + f.getType().getSimpleName(), 
					        		new BeanPropertyRowMapper<>(f.getType()));
					        for (T eObj: entityObject){
					        	if (eObj.getId() == entityId){
									try {
										f.set(object, eObj);
									} catch (IllegalAccessException e) {
										e.printStackTrace();
									}
									break;
								}
							}
			            	return null;
			            }
			        });
			        if (!haveEntityField){
						haveEntityField = true;
					}
				}
			}
		} catch (SecurityException | IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (haveEntityField){
			return object;
		} else {
			return (T) queryForObject("SELECT * FROM " + clazz.getSimpleName() + " WHERE id = ?", 
					new Object[] {id}, new BeanPropertyRowMapper<>(clazz));
		}
    }

	/**
	 * Gets all records from table in database and sets as values of new objects (extends Entity)
	 * Universal method for all objects (which extends Entity); with using reflection
	 * Use this method when one or more fields of your object which extends Entity, extends Entity too
	 * @param clazz
	 * @return
	 */
	public <T extends Entity> List<T> listReflect(final Class<T> clazz){
		final List<T> objectList = (List<T>) query("SELECT * FROM " + clazz.getSimpleName(), new BeanPropertyRowMapper<>(clazz));
		Field[] fields = clazz.getDeclaredFields();
		Field.setAccessible(fields, true);
		boolean haveEntityField = false;
		try {
			for (final Field f: fields){
				if (f.getType().getSuperclass() == Entity.class){
					for(final T obj: objectList){
						String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = ?";
						int id = obj.getId();
						queryForObject(sql, new Object[] {id}, new RowMapper<T>() {
							@Override
							public T mapRow(ResultSet rs, int i) throws SQLException {
								int entityId = rs.getInt(f.getName() + "Id");
								@SuppressWarnings("unchecked")
								List<T> entityObject = (List<T>) query("SELECT * FROM " + f.getType().getSimpleName(), 
										new BeanPropertyRowMapper<>(f.getType()));
								for (T eObj: entityObject){
									if (eObj.getId() == entityId){
										try {
											f.set(obj, eObj);
										} catch (IllegalAccessException e) {
											e.printStackTrace();
										}
										break;
									}
								}
								return null;
							}
						});
						if (!haveEntityField){
							haveEntityField = true;
						}
					}
				}
			}
		} catch (SecurityException | IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (haveEntityField){
			return objectList;
		} else {
			return query("SELECT * FROM " + clazz.getSimpleName(), new BeanPropertyRowMapper<>(clazz));
		}
    }
	
	/**
	 * Delete record from table in database by id
	 * @param clazz
	 * @param id - id of which you want to delete 
	 * @return
	 */
	public <T extends Entity> boolean delete (Class<T> clazz, Integer id){
		return update ("DELETE FROM " + clazz.getSimpleName() + " WHERE id = ?", id) > 0;
	}
	
	/**
	 * Add or update one record in table in database, using for this operation values from 
	 * object (extends Entity) 
	 * Universal method for all objects (which extends Entity); with using reflection
	 * Use this method when one or more fields of your object which extends Entity, extends Entity too
	 * @param obj - object for save
	 */
	public <T extends Entity> void saveReflect (final T obj) {
		@SuppressWarnings("rawtypes")
		final Class clazz = obj.getClass();
		final Field[] fields = clazz.getDeclaredFields();
		Field.setAccessible(fields, true);
		if (obj.isNew()) {
			PreparedStatementCreator psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					String sql = "INSERT INTO " + clazz.getSimpleName() + " (";
					int size = fields.length;
					int i = 1;
					for (Field f : fields) {
						if (f.getType().getSuperclass() == Entity.class) {
							sql += f.getName() + "Id";
						} else {
							sql += f.getName();
						}
						if (i++ < size) {
							sql += ", ";
						}
					}
					sql += ") VALUES (";
					for (int j = 1; j < size; j++) {
						sql += "?, ";
					}
					sql += "?)";
					i = 1;
					PreparedStatement ps = con.prepareStatement(sql);
					for (Field f : fields) {
						try {
							if (f.getType().getSuperclass() == Entity.class) {
								Entity entity = (Entity) f.get(obj);
								ps.setInt(i++, entity.getId());
							} else if (f.getType().getSuperclass() == Enum.class) {
								ps.setString(i++, f.get(obj).toString());
							} else if (f.getType() == String.class) {
								ps.setString(i++, f.get(obj).toString());
							} else {
								ps.setObject(i++, f.get(obj));
							}
						} catch (IllegalArgumentException
								| IllegalAccessException e) {
							e.printStackTrace();
						}
					}
					return ps;
				}
			};
			update(psc);
		} else {
			PreparedStatementCreator psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					String sql = "UPDATE " + clazz.getSimpleName() + " SET ";
					int size = fields.length;
					int i = 1;
					for (Field f : fields) {
						if (f.getType().getSuperclass() == Entity.class) {
							sql += f.getName() + "Id = ?";
						} else {
							sql += f.getName() + " = ?";
						}
						if (i++ < size) {
							sql += ", ";
						}
					}
					sql += " WHERE id = ?";
					i = 1;
					PreparedStatement ps = con.prepareStatement(sql);
					for (Field f : fields) {
						try {
							if (f.getType().getSuperclass() == Entity.class) {
								Entity entity = (Entity) f.get(obj);
								ps.setInt(i++, entity.getId());
							} else if (f.getType().getSuperclass() == Enum.class) {
								ps.setString(i++, f.get(obj).toString());
							} else if (f.getType() == String.class) {
								ps.setString(i++, f.get(obj).toString());
							} else {
								ps.setObject(i++, f.get(obj));
							}
						} catch (IllegalArgumentException
								| IllegalAccessException e) {
							e.printStackTrace();
						}
					}
					ps.setInt(i++, obj.getId());
					return ps;
				}
			};
			update(psc);
		}
	}
	
	
	
	//Next methods not use in this project, because they are not universal
	
	/**
	 * Gets one record from table in database and sets as values of new object (extends Entity)
	 * This method simple but you can use it only for simple pairs table-object  
	 * @param clazz
	 * @param id - id of selected record
	 * @return new object extends Entity
	 */
	public <T extends Entity> T get (Class<T> clazz, Integer id){
		return queryForObject("SELECT * FROM " + clazz.getSimpleName() + " WHERE id = ?", new Object[] {id}, new BeanPropertyRowMapper<>(clazz));
	}
	
	/**
	 * Gets all records from table in database and sets as values of new objects (extends Entity)
	 * This method simple but you can use it only for simple pairs table-object  
	 * @param clazz
	 * @return list of new objects extends Entity
	 */
	public <T extends Entity> List<T> list (Class<T> clazz){
		return query ("SELECT * FROM " + clazz.getSimpleName(), new BeanPropertyRowMapper<>(clazz));
	}
	
	/**
	 * Add or update one record only in table "User"
	 * using for this operation values from object - User
	 * @param user
	 */
	public void save(final User user){
		if (user.isNew()) {
			PreparedStatementCreator psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement("INSERT INTO User (login, password, name)  VALUES (?,?,?)");
					ps.setString(1, user.getLogin());
					ps.setString(2, user.getPassword());
					ps.setString(3, user.getName());
					return ps;
				}
			};
			update (psc) ;
		} else {
			update ("UPDATE User SET name = ?, login = ?, password = ? WHERE id = ?", user.getName(), user.getLogin(), user.getPassword(), user.getId());
		}
	}
	
	/**
	 * Add or update one record only in table "Ticket"
	 * using for this operation values from object - Ticket
	 * @param ticket
	 */
	public void save(final Ticket ticket) {
		if (ticket.isNew()) {
			PreparedStatementCreator psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con
							.prepareStatement("INSERT INTO Ticket (title, description, ownerId, status, priority)"
									+ " VALUES (?,?,?,?,?)");
					ps.setString(1, ticket.getTitle());
					ps.setString(2, ticket.getDescription());
					if (ticket.getOwner() != null) {
						ps.setInt(3, ticket.getOwner().getId());
					} else {
						ps.setInt(3, 0);
					}
					ps.setString(4, ticket.getStatus().name());
					ps.setString(5, ticket.getPriority().name());
					return ps;
				}
			};
			update(psc);
		} else {
			int id = 0;
			if (ticket.getOwner() != null){
				id = ticket.getOwner().getId();
			}
			update("UPDATE Ticket SET title = ?, description = ?, ownerId = ?, status = ?, priority = ? WHERE id = ?",
					ticket.getTitle(), ticket.getDescription(), id,
					ticket.getStatus().toString(), ticket.getPriority().toString(), ticket.getId());
		}
	}
	
	/**
	 * Gets all records from table "Ticket" and sets as values of new objects - Ticket
	 * @return
	 */
	public List<Ticket> ticketsList(){
        String sql = "SELECT * FROM Ticket";
        final List<Ticket> tickets = new ArrayList<>() ;
        query(sql, new RowMapper<Ticket>() {
            @Override
            public Ticket mapRow(ResultSet rs, int i) throws SQLException {
            	User owner = null;
            	int ownerId = rs.getInt("ownerId");
            	List<User> listUser = list (User.class);
                for (User lu: listUser){
                	if (lu.getId() == ownerId){
                		owner = lu;
                	}
                }
            	Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("id"));
                ticket.setTitle(rs.getString("title"));
                ticket.setDescription(rs.getString("description"));
                ticket.setOwner(owner);
                ticket.setPriority(TicketPriority.valueOf(rs.getString("priority")));
                ticket.setStatus(TicketStatus.valueOf(rs.getString("status")));
                tickets.add(ticket);
                return null;
            }
        });
        return tickets;
    }
	
	/**
	 * Gets one record from table "Ticket" and sets as values of new object - Ticket
	 * @param id - id of selected record
	 * @return - new Ticket
	 */
	public Ticket ticketGet(int id){
        String sql = "SELECT * FROM Ticket WHERE id = ?";
        final Ticket ticket = queryForObject(sql, new Object[] {id}, new RowMapper<Ticket>() {
            @Override
            public Ticket mapRow(ResultSet rs, int i) throws SQLException {
            	User owner = null;
            	int ownerId = rs.getInt("ownerId");
            	List<User> listUser = list (User.class);
                for (User lu: listUser){
                	if (lu.getId() == ownerId){
                		owner = lu;
                	}
                }
            	Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("id"));
                ticket.setTitle(rs.getString("title"));
                ticket.setDescription(rs.getString("description"));
                ticket.setOwner(owner);
                ticket.setPriority(TicketPriority.valueOf(rs.getString("priority")));
                ticket.setStatus(TicketStatus.valueOf(rs.getString("status")));
                return ticket;
            }
        });
        return ticket;
    }
}