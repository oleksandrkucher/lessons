package com.geekhub.beans;

public enum TicketStatus {
    ACTIVE, RESOLVED, TESTED, PUSHED;
}