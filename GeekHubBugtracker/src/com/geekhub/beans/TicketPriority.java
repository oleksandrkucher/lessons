package com.geekhub.beans;

public enum TicketPriority {
    LOW(1),
    NORMAL(5),
    HIGH(10);

    public Integer priority;

    private TicketPriority(Integer priority) {
        this.priority = priority;
    }
}