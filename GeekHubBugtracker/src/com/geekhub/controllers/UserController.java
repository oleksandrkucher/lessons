package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;

/**
 * Controller for work with table "User"
 */
@Controller
public class UserController {

	@Autowired BaseDao dao;
	
	/**
	 * Gets all values from table "User" and put it to jsp page
	 * Process request "/listUsers.html"
	 * @param map
	 * @return - name of jsp page, for view this list, without suffix
	 */
	@RequestMapping(value="listUsers.html")
	public String list (ModelMap map){
		map.put("users", dao.listReflect(User.class));
		return "users";
	}
	
	/**
	 * Load one record from table "User" for updating, and put it to jsp page
	 * Process request "/loadUser.html"
	 * @param id - id of selected record
	 * @param map
	 * @return - name of jsp page, for view and correct this record, without suffix
	 */
	@RequestMapping(value="loadUser.html")
	public String load (@RequestParam(value="id", required=false) Integer id, ModelMap map){
		User user = id == null ? new User () : dao.getReflect(User.class, id);
		map.put("user", user);
		return "user";
	}
	
	/**
	 * Delete record from table "Ticket" by id
	 * Process request "/deleteUser.html"
	 * @param id - id of record which you want to delete
	 * @return - redirect to page for view all values from table
	 */
	@RequestMapping(value="deleteUser.html")
	public String delete (@RequestParam(value="id", required=true) Integer id){
		dao.delete(User.class, id);
		return "redirect:listUsers.html";
	}
	
	/**
	 * Add or update one record in table "User"
	 * Process request "/saveUser.html"
	 * @param user - object for addition or updating values in table
	 * @return - redirect to page for view all values from table
	 */
	@RequestMapping(value="saveUser.html")
	public String save (User user){
		dao.saveReflect(user);
		return "redirect:listUsers.html";
	}
}