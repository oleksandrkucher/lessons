package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.TicketPriority;
import com.geekhub.beans.TicketStatus;
import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;

/**
 * Controller for work with table "Ticket"
 */
@Controller
public class TicketController {

	@Autowired BaseDao dao;
	
	/**
	 * Gets all values from table "Ticket" and put it to jsp page
	 * Process request "/listTickets.html"
	 * @param map
	 * @return - name of jsp page, for view this list, without suffix
	 */
	@RequestMapping(value="listTickets.html")
	public String list (ModelMap map){
		map.put("tickets", dao.listReflect(Ticket.class));
		return "tickets";
	}
	
	/**
	 * Load one record from table "Ticket" for updating, and put it to jsp page
	 * Process request "/loadTicket.html"
	 * @param id - id of selected record
	 * @param map
	 * @return - name of jsp page, for view and correct this record, without suffix
	 */
	@RequestMapping(value="loadTicket.html")
	public String load (@RequestParam(value="id", required=false) Integer id, ModelMap map){
		Ticket ticket = id == null ? new Ticket () : dao.getReflect(Ticket.class, id);
		TicketStatus[] ticketStatus = TicketStatus.values(); 
		TicketPriority[] ticketPriority = TicketPriority.values();
		map.put("ticket", ticket);
		map.put("users", dao.list(User.class)); 
		map.put("status", ticketStatus); 
		map.put("priority", ticketPriority); 
		return "ticket";
	}
	
	/**
	 * Delete record from table "Ticket" by id
	 * Process request "/deleteTicket.html"
	 * @param id - id of record which you want to delete
	 * @return - redirect to page for view all values from table
	 */
	@RequestMapping(value="deleteTicket.html")
	public String delete (@RequestParam(value="id", required=true) Integer id){
		dao.delete(Ticket.class, id);
		return "redirect:listTickets.html";
	}
	
	/**
	 * Add or update one record in table "Ticket"
	 * Process request "/saveTicket.html"
	 * @param ticket - object for addition or updating values in table
	 * @return - redirect to page for view all values from table
	 */
	@RequestMapping(value="saveTicket.html")
	public String save (Ticket ticket){
		dao.saveReflect(ticket);
		return "redirect:listTickets.html";
	}
}