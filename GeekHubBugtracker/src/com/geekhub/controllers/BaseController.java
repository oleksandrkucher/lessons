package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;

/**
 * Controller for work with main page - "Index.html"
 */
@Controller
public class BaseController {
	
	@Autowired BaseDao dao;
	
	/**
	 * Show index.html with information about tables "User" and "Ticket" (record count)
	 * Process request "/index.html"
	 * @param map
	 * @return - name of jsp page
	 */
	@RequestMapping(value="index.html")
	public String index (ModelMap map) {
		map.put("users", dao.list(User.class).size());
		map.put("tickets", dao.list(Ticket.class).size ());
		return "index";
	}
}