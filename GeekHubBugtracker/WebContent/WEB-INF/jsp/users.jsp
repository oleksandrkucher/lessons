<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 11. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>ListUsers</h1>
		<table border="1" width="700">
			<tr>
				<th>Name</th>
				<th>Login</th>
				<th>Password</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>${user.name}</td>
					<td>${user.login}</td>
					<td>${user.password}</td>
					<td><a href="/GeekHub_Bugtracker/loadUser.html?id=${user.id}">Edit</a></td>
					<td style="color: red;"><a href="/GeekHub_Bugtracker/deleteUser.html?id=${user.id}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<br> <a href="/GeekHub_Bugtracker/loadUser.html">Create New User</a>
		<br>
		<br> <a href="/GeekHub_Bugtracker/index.html">Main Page</a>
	</center>
</body>
</html>