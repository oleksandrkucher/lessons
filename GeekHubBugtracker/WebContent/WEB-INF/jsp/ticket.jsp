<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 11. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}
</style>
</head>
<body>
	<center>
		<h1>NewTicket</h1>
		<form action="/GeekHub_Bugtracker/saveTicket.html">
			<table border = 1 width = 400 height = 200>
				<tr>
					<td>Title</td> 
					<td><input type="text" value="${ticket.title}" name="title"></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><input type="text" value="${ticket.description}" name="description"></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><select name="status">
						<c:forEach items="${status}" var="ticketStatus">
							<c:choose>
 								<c:when test="${ticketStatus == ticket.status}">
									<option selected value="${ticketStatus}">${ticketStatus}</option>
 								</c:when>
								<c:otherwise>
									<option value="${ticketStatus}">${ticketStatus}</option>
 								</c:otherwise>
 							</c:choose>
						</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Priority</td>
					<td><select name="priority">
						<c:forEach items="${priority}" var="ticketPriority">
							<c:choose>
 								<c:when test="${ticketPriority == ticket.priority}">
									<option selected value="${ticketPriority}">${ticketPriority}</option>
 								</c:when>
								<c:otherwise>
									<option value="${ticketPriority}">${ticketPriority}</option>
 								</c:otherwise>
 							</c:choose>
						</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Owner Id</td>
					<td><select name="owner.id">
						<c:forEach items="${users}" var="user">
							<c:choose>
 								<c:when test="${user.id == ticket.owner.id}">
									<option selected value="${user.id}">${user.name} (${user.id})</option>
 								</c:when>
								<c:otherwise>
									<option value="${user.id}">${user.name} (${user.id})</option>
 								</c:otherwise>
 							</c:choose>
						</c:forEach>
					</select></td>
				</tr>
			</table>
			<input type="hidden" name="id" value="${ticket.id}"><br>
			<input type="submit" value="Save">
		</form>
		<br>
		<br> <a href="/GeekHub_Bugtracker/index.html">Main Page</a>
	</center>
</body>
</html>