<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lesson 12. GeekHub</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red
}

.error{
	color: red;
}
</style>
</head>
<body>
	<center>
		<h1>NewUser</h1>
		<form action="/GeekHubBugtrackerHibernate/saveUser.html">
			<table border = 1 width = 400 height = 200>
				<tr>
					<td>Name</td>
					<td><input type = "text" value = "${user.name}" name = "name" size = 30></td>
				</tr>
				<tr>
					<td>Login</td>
					<td><input type = "text" value = "${user.login}" name = "login" size = 30></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type = "text" value = "${user.password}" name = "password" size = 30></td>
				</tr>
			</table>	
			<input type = "hidden" 	value = "${user.id}" name = "id" ><br>
			<input type = "submit" 	value = "Save">
		</form>
		<br>
		<br> <a href="/GeekHubBugtrackerHibernate/index.html">Main Page</a>
	</center>
</body>
</html>