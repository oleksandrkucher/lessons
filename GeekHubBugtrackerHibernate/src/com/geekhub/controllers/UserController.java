package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.entity.User;
import com.geekhub.services.UserDAO;

/**
 * Controller for work with table "User"
 */
@Controller
public class UserController {

	@Autowired UserDAO userDAO;
	
	/**
	 * Show listUsers.html with records from table "User"
	 * Process request "/listUsers.html"
	 * @param map
	 * @return
	 */
	@RequestMapping(value="listUsers.html")
	public String list (ModelMap map){
		map.put("users", userDAO.getUserList());
		return "users";
	}
	
	/**
	 * Show loadUser.html with one record from table "User" (if you update it) 
	 * and void form (if you create new record)
	 * Process request "/loadUser.html"
	 * @param id - id of record which you update
	 * @param map
	 * @return
	 */
	@RequestMapping(value="loadUser.html")
	public String load (@RequestParam(value="id", required=false) Integer id, ModelMap map){
		User user = id == null ? new User () : userDAO.getUserById(id);
		map.put("user", user);
		return "user";
	}
	
	/**
	 * Delete one record from table "User" and redirect you to listUsers.html
	 * Process request "/deleteUser.html"
	 * @param id - id of record which you delete
	 * @return
	 */
	@RequestMapping(value="deleteUser.html")
	public String delete (@RequestParam(value="id", required=true) Integer id){
		userDAO.deleteUser(id);
		return "redirect:listUsers.html";
	}
	
	/**
	 * Save or update one record in table "User" and redirect you to listUsers.html
	 * Process request "/saveUser.html"
	 * @param user - record for saving in database
	 * @return
	 */
	@RequestMapping(value="saveUser.html")
	public String save (User user){
		userDAO.saveUser(user);
		return "redirect:listUsers.html";
	}
}