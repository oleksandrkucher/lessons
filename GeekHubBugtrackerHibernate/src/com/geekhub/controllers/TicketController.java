package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.entity.Ticket;
import com.geekhub.entity.TicketPriority;
import com.geekhub.entity.TicketStatus;
import com.geekhub.services.TicketDAO;
import com.geekhub.services.UserDAO;

/**
 * Controller for work with table "Ticket"
 */
@Controller
public class TicketController {

	@Autowired TicketDAO ticketDAO;
	@Autowired UserDAO userDAO;
	
	/**
	 * Show listTickets.html with records from table "Ticket"
	 * Process request "/listTickets.html"
	 * @param map
	 * @return
	 */
	@RequestMapping(value="listTickets.html")
	public String list (ModelMap map){
		map.put("tickets", ticketDAO.getTicketList());
		return "tickets";
	}
	
	/**
	 * Show loadTicket.html with one record from table "Ticket" (if you update it) 
	 * and void form (if you create new record)
	 * Process request "/loadTicket.html"
	 * @param id - id of record which you update
	 * @param map
	 * @return
	 */
	@RequestMapping(value="loadTicket.html")
	public String load (@RequestParam(value="id", required=false) Integer id, ModelMap map){
		Ticket ticket = id == null ? new Ticket() : ticketDAO.getTicketById(id);
		map.put("priority", TicketPriority.values());
		map.put("status", TicketStatus.values());
		map.put("users", userDAO.getUserList());
		map.put("ticket", ticket);
		return "ticket";
	}
	
	/**
	 * Delete one record from table "Ticket" and redirect you to listTickets.html
	 * Process request "/deleteTicket.html"
	 * @param id - id of record which you delete
	 * @return
	 */
	@RequestMapping(value="deleteTicket.html")
	public String delete (@RequestParam(value="id", required=true) Integer id){
		ticketDAO.deleteTicket(id);
		return "redirect:listTickets.html";
	}
	
	/**
	 * Save or update one record in table "Ticket" and redirect you to listTickets.html
	 * Process request "/saveTicket.html"
	 * @param ticket - record for saving in database
	 * @param ownerId - id of owner of this ticket
	 * @return
	 */
	@RequestMapping(value="saveTicket.html")
	public String save (Ticket ticket, @RequestParam(value="ownerId", required=true) Integer ownerId){
		ticketDAO.saveTicket(ticket, ownerId);
		return "redirect:listTickets.html";
	}
}