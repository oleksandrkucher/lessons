package com.geekhub.entity;

public enum TicketStatus {
    ACTIVE, RESOLVED, TESTED, PUSHED;
}