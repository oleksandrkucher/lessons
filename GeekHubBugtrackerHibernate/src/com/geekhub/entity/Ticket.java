package com.geekhub.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class describe table "Ticket" in our database
 */
@Entity
@Table(name="Ticket")
public class Ticket {
	
	@GeneratedValue
	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "title")
    private String title;
	
	@Column(name = "description")
    private String description;

	/**
	 * Owner of this ticket
	 */
	@ManyToOne
    @JoinColumn(name="ownerId", 
                insertable=false, updatable=true, 
                nullable=false)
	private User owner;
	
	@Column (name = "status")
    private TicketStatus status;
    
	@Column(name = "priority")
	private TicketPriority priority;
	
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public TicketStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }
}