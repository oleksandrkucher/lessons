package com.geekhub.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

/**
 * Class describe table "User" in our database
 */
@Entity
@Table(name="User")
public class User {

	@GeneratedValue
	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="login")
    private String login;
	
	@Column(name="password")
    private String password;
	
	@Column(name="name")
    private String name;
	
	/**
	 * List of tickets of this user
	 */
	@OneToMany(cascade={CascadeType.ALL})
    @JoinColumn(name="ownerId")
    @IndexColumn(name="idx")
	private List<Ticket> tickets;
	
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}
}