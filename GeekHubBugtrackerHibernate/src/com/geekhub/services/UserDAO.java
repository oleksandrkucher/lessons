package com.geekhub.services;

import java.util.List;

import com.geekhub.entity.User;

public interface UserDAO {

	/**
	 * Gets list of users from database (table "User")
	 * @return List<>
	 */
	public List<User> getUserList ();
	
	/**
	 * Gets one record from database by id of it (table "User")
	 * @param id
	 * @return User
	 */
	public User getUserById (Integer id);
	
	/**
	 * Delete one record from database (table "User") by id
	 * @param id
	 */
	public void deleteUser (Integer id);
	
	/**
	 * Save or update one record in database (table "User")
	 * @param user - record for save or update
	 */
	public void saveUser (User user);
}
