package Coding;

import java.io.*;

/**
 * Class contains methods for encoding files
 * 
 * root - root folder
 * separator - separator of files in different OS
 * Reader - variable for read from file
 * Writer - variable for write to file
 * 
 * @author kol
 *
 */

public class CodeMethods {

	static String root;
	static String separator;
	private Reader read = null;
	private Writer write = null;
	private int b[] = null;
	private int i = 0;

	/**
	 * Constructor initializes variables root and separator
	 */
	
	public CodeMethods(){
		
		root = System.getProperty("user.home");;
		separator = System.getProperty("file.separator");
	}
	
	/**
	 * String array contains name of encodings
	 */
	
	private String[] encodingsList = {"ASCII",
	        "ISO8859_1","ISO8859_2","ISO8859_3","ISO8859_4","ISO8859_5",
	        "ISO8859_6","ISO8859_7","ISO8859_8","ISO8859_9","Big5","Cp037",
	        "Cp1006" ,"Cp1025","Cp1026","Cp1046","Cp1097","Cp1098",
	        "Cp1112" ,"Cp1122","Cp1123","Cp1124",
	        "Cp1250" ,"Cp1251","Cp1252","Cp1253","Cp1254","Cp1255",
	        "Cp1256" ,"Cp1257","Cp1258","Cp1381","Cp1383",
	        "Cp273" ,"Cp277" ,"Cp278" ,"Cp280" ,"Cp284" ,"Cp285","Cp297",
	        "Cp33722","Cp420" ,"Cp424" ,"Cp437" ,"Cp500" ,"Cp737","Cp775",
	        "Cp838" ,"Cp850" ,"Cp852" ,"Cp855" ,"Cp857" ,
	        "Cp860" ,"Cp861" ,"Cp862" ,"Cp863" ,"Cp864" ,"Cp865",
	        "Cp866" ,"Cp868" ,"Cp869" ,"Cp870" ,"Cp871" ,"Cp874","Cp875",
	        "Cp918" ,"Cp921" ,"Cp922" ,"Cp930" ,"Cp933" ,"Cp935","Cp937","Cp939",
	        "Cp942" ,"Cp948" ,"Cp949" ,"Cp950" ,"Cp964" ,"Cp970",
	        "EUC_CN","EUC_JP","EUC_KR","EUC_TW",
	        "GBK","ISO2022CN_CNS","ISO2022JP","ISO2022KR",
	        "JIS0201","JIS0208","JIS0212","KOI8_R","MS874",
	        "MacArabic","MacCentralEurope","MacCroatian","MacCyrillic",
	        "MacDingbat","MacGreek","MacHebrew","MacIceland","MacRoman",
	        "MacRomania","MacSymbol","MacThai","MacTurkish","MacUkraine",
	        "SJIS","UTF8","UTF-16","UnicodeBig","UnicodeLittle"};
	
	/**
	 * Method create variable of type File and verifies if it exists
	 * 
	 * @return File
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	
	public File getInputFile() throws UnsupportedEncodingException, FileNotFoundException {

		String str = getStr("output");
		File file = new File(root + separator + str);
		if (file.exists()) {
			return file;
		} else {
			System.out.print("This file is not exists! Try again. ");
			return getInputFile();
		}
	}

	/**
	 * Method reads string from keyboard
	 *  
	 * @param string - out string
	 * @return string which was read from keyboard 
	 */
	
	private String getStr(String string){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		System.out.print("Input name or path of "+ string +" file: ");
		try {
			str = buf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * Method get file and create Output Stream
	 * 
	 * @return Output Stream
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	
	public Writer getOutput() throws FileNotFoundException, UnsupportedEncodingException {

		String str = getStr("output");
		File file = new File(root + separator + str);
		return new OutputStreamWriter(new FileOutputStream(file), chooseCode("input"));
	}

	/**
	 * Method gets integer number from keyboard
	 * 
	 * @return integer number
	 */
	
	private int getNumber(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		try {
			return Integer.parseInt(buf.readLine());
		} catch (NumberFormatException | IOException e) {
			System.out.println("ERROR! " + e);
			return getNumber();
		}
	}
	
	/**
	 * Method choose name of encoding and return it
	 * @return name of encoding
	 */
	
	public String chooseCode(String str) {
		
		System.out.print("Inpun number of " + str + " coding: ");
		int number = getNumber();
		if(number > 0 && number <= encodingsList.length){		
			return encodingsList[number - 1];
		} else {
			return chooseCode(str);
		}
	}

	/**
	 * Method read info from file to int array
	 * @throws IOException
	 */
	
	public void reader() throws IOException{
		
		try{
			File fileIn = getInputFile();
			read = new InputStreamReader(new FileInputStream(fileIn), chooseCode("input"));
			b = new int[(int)fileIn.length()+1];
			while ((b[i++] = read.read()) >= 0);
			
		} catch (IOException e){
			System.out.println("ERROR! " + e);
		} finally {
			if(read != null){
				read.close();
			}
		}
	}
	
	/**
	 * Method write from int array to file
	 * @throws IOException
	 */
	
	public void writer() throws IOException{
		
		try{
			write = getOutput();
			for(int j = 0; j < i; j++){
				write.write(b[j]);
			}
			System.out.println("Encoding comlete.");
			
		} catch (IOException e){
			System.out.println("ERROR! " + e);
		} finally {
			if(write != null){
				write.close();
			}
		}
	}

	/**
	 * Method prints info about encodings
	 */
	
	public void outputList(){
		
		System.out.println("The list of encodings: ");
		for(int i = 0; i < encodingsList.length; i++){
			System.out.println(i+1 + " - " + encodingsList[i]);
		}
	}
}
