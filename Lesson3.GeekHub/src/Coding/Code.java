package Coding;

import java.io.IOException;

/**
 * Main class for recoding files
 *   
 * @author kol
 *
 */

public class Code {
	
	public static void main(String[] args) throws IOException{
		
		CodeMethods code = new CodeMethods();
		code.outputList();
		System.out.println("The root folder: " + CodeMethods.root);
		code.reader();
		code.writer();
		
	}
}