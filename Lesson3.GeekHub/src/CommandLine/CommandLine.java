package CommandLine;

import java.sql.Connection;

/**
 * Main class for work with database
 *  
 * @author kol
 *
 */

public class CommandLine {

	static boolean exit = false;
	
	public static void main(String[] args) {
		
		CommandsSQL command = new CommandsSQL();
		Connection con = command.getConnect();
		do {
			command.commandInfo();
			System.out.print("Input your command: ");
			int num = command.getNumber();
			doIt(num, con, command);
		} while (exit != true);		
		command.closeConnection(con);
	}

	/**
	 * Method choose command and do it
	 * 
	 * @param num = number of command
	 * @param connection - connection with database
	 * @param command - instance of the class CommandsSQL
	 */
	
	public static void doIt(int num, Connection connection, CommandsSQL command){
		
		switch (num){
			case 1:
				command.createTable(connection);
				break;
			case 2:
				command.select(connection);
				break;
			case 3:
				command.commandUpdate(connection);
				break;
			case 4:
				command.createBase(connection);
				break;
			case 5:
				exit = true;
				System.out.println("Goodbye.");
				break;
			default:
				System.out.println("Wrong command. Try again.");
				break;
		}
	}
}
