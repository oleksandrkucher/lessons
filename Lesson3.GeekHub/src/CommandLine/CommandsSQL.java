package CommandLine;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class contains methods for work with database
 * 
 * @author kol
 *
 */

public class CommandsSQL {

	private String create1 = "CREATE TABLE `?` (";
	private String create2 = "`id` int(11) NOT NULL auto_increment,";
	private String create3 = " `?` ";
	private String create4 = "? default NULL,";
	private String create5 = " PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	private String createDataBase = "CREATE DATABASE ? CHARACTER SET utf8 COLLATE utf8_general_ci";
	
	/**
	 * Method gets string from keyboard
	 * @return string
	 */
	
	public String getStr(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		try {
			str = buf.readLine();
		} catch (IOException e) {
			System.out.print("ERROR! " + e);
		}
		return str;
	}
	
	/**
	 * Method gets name of host from keyboard
	 * @return host name
	 */
	
	private String getHostName(){
		
		System.out.print("Input name of your host: ");
		return getStr();
	}
	
	/**
	 * Method gets your login from keyboard
	 * @return login
	 */
	
	private String getLogin(){
		
		System.out.print("Input your login: ");
		return getStr();
	}
	
	/**
	 * Method gets your password from keyboard
	 * @return password
	 */
	
	private String getPassword(){
		
		System.out.print("Input your password: ");
		return getStr();
	}
	
	/**
	 * Method gets name of database from keyboard
	 * @return database name
	 */
	
	private String getDataBaseName(){
		
		System.out.print("Input name of your database: ");
		return getStr();
	}

	/**
	 * Method sets the connection with the base
	 * @return Connection
	 */
	
	public Connection getConnect(){
		
		Connection connect = null;
		try {
			connect = DriverManager.getConnection("jdbc:mysql://" + getHostName() + "/" 
					+ getDataBaseName(), getLogin(), getPassword());
		} catch (SQLException e) {
			System.out.println("ERROR! " + e);
		}
		return connect;
		
	}
	
	/**
	 * Method close connection
	 * @param connect - Connection
	 */
	
	public void closeConnection(Connection connect){
		
		try {
			connect.close();
		} catch (SQLException e) {
			System.out.println("ERROR! " + e);
		}
	}
	
	/**
	 * Method creates table in current database
	 * @param connection
	 */
	
	public void createTable(Connection connection){
		
		String result = "";
		String name;
		System.out.print("Input name of table which you want to create: ");
		name = getStr();
		result = result + create1.replace("?", name) + create2;
		System.out.print("Input number of fields which you want to create: ");
		int num = getNumber();
		for(int i = 0; i < num; i++){
			System.out.print("Input "+(i+1)+" name of field: ");
			name = getStr();
			result = result + create3.replace("?", name);
			System.out.print("Input "+(i+1)+" type of field: ");
			name = getStr();
			result = result + create4.replace("?", name);
		}
		result = result + create5;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(result);
		} catch (SQLException e) {
			System.out.println("ERROR! " + e);
		}
	}
	
	/**
	 * Method gets integer number from keyboard
	 * @return - integer number
	 */
	
	public int getNumber(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		int number = 0;
		try {
			number = Integer.parseInt(buf.readLine());
		} catch (NumberFormatException | IOException e) {
			System.out.print("ERROR! " + e+ "\nTry again: ");
			return getNumber();
		}
		if(number <= 0){
			System.out.println("ERROR! Number must be positive.");
			return getNumber();
		} else {
			return number;
		}
		
	}
	
	/**
	 * Method reads selection request to database
	 * @param connection
	 */
	
	public void select(Connection connection){
		
		System.out.print("Input your command 'SELECT' with SQl syntax: ");
		String command = getStr();
		Statement statement = null;
		System.out.print("Input number of columns in your table: ");
		int length = getNumber();
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(command);	
			while(result.next()) {
				for(int i = 1; i <= length; i++){
					System.out.print(result.getString(i)+"\t");
				}
				System.out.println();
			}
		} catch (SQLException e) {
			System.out.println("ERROR! "+ e);
		}
	}
	
	/**
	 * Method reads from keyboard requests UPDATE, DELETE, INSERT, DROP
	 * @param connection - connection with database
	 * @return - number of modified records
	 */
	
	private int updateExecuteUpdate(Connection connection){
		
		System.out.print("Input your command with SQl syntax: ");
		String command = getStr();
		Statement statement = null;
		int res = 0;
		try {
			statement = connection.createStatement();
			res = statement.executeUpdate(command);
		} catch (SQLException e) {
			System.out.println("ERROR! "+ e);
		}
		return res;
	}
	
	/**
	 * Method makes several requests
	 * @param connection - connection with database
	 * @param howMany - number of requests
	 * @return - number of modified records
	 */
	
	private int updateBatchUpdate(Connection connection, int howMany){
		
		Statement statement = null;
		int res = 0;
		try {
			statement = connection.createStatement();
			for(int i = 0; i < howMany; i++){
				System.out.print("Input your command with SQl syntax: ");
				String command = getStr();
				statement.addBatch(command);
			}
			int[] recordsAffected = statement.executeBatch();
			for(int i = 0; i < recordsAffected.length; i++){
				res += recordsAffected[i];
			}
		} catch (SQLException e1) {
			System.out.println("ERROR! "+ e1);
		}
		return res;
	}
	
	/**
	 * Method gets number of requests and causes methods which send requests to the database 
	 * @param connection - connection with database
	 */
	
	public void commandUpdate(Connection connection){
		
		System.out.print("Input how many updates you want to do: ");
		int howMany = getNumber();
		if(howMany == 1){
			System.out.println("You change " + updateExecuteUpdate(connection) + " value(s).");
		} else {
			System.out.println("You change " + updateBatchUpdate(connection, howMany) + " value(s).");	
		}
	}

	/**
	 * Method creates database
	 * @param connection - connection with database
	 */
	
	public void createBase(Connection connection){
		
		System.out.print("Input name of new database: ");
		String name = getStr();
		String command = createDataBase.replace("?", name);
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(command);
		} catch (SQLException e) {
			System.out.println("ERROR! "+ e);
		}
	}
	
	/**
	 * Method prints commands which you can use in this program
	 */
	
	public void commandInfo(){
		
		System.out.println("1 - create table in database");
		System.out.println("2 - command SELECT FROM");
		System.out.println("3 - commands UPDATE, DELETE, INSERT");
		System.out.println("4 - create new database");
		System.out.println("5 - exit");
	}
}
