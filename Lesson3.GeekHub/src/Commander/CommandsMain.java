package Commander;

import java.io.IOException;

/**
 * Main class for commander
 * 
 * @author KOL
 * 
 */

public class CommandsMain {

	static boolean exit = false;

	public static void main(String[] args) throws IOException {

		ScanCommand scan = new ScanCommand(3, 4);
		CommandsMain doIt = new CommandsMain();
		Commands command = Commands.whatOSC();

		do {
			String com = scan.scanStringCommand();
			doIt.command(com, command);
		} while (exit != true);
	}

	/**
	 * Method for selecting commands for execution
	 * 
	 * @param com - command
	 * @param command - instance of the class Commands
	 * 
	 * @throws IOException
	 */
	
	private void command(String com, Commands command) throws IOException {

		switch (com) {
		case "MKD":
			command.makeDir();
			break;
		case "CDUP":
			command.cdUp();
			break;
		case "CWD":
			command.changeDir();
			break;
		case "LIST":
			command.list();
			break;
		case "PWD":
			command.printCurrentDir();
			break;
		case "QUIT":
			exit = true;
			break;
		case "DELE":
			command.delFile();
			break;
		case "RMD":
			command.delDir();
			break;
		case "MDTM":
			command.lastModify();
			break;
		case "SIZE":
			command.size();
			break;
		case "RNFR":
			command.rename();
			break;
		case "HELP":
			command.help();
			break;
		case "DELA":
			command.delAllDir(command.path());
			break;
		case "SYST":
			command.whatOS();
			break;
		case "NLIS":
			command.nList();
			break;
		default:
			System.out.println("Wrong command. Try again, please.");
			break;
		}
	}

}
