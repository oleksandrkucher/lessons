package Commander;

import java.io.*;

/**
 * This class implements a data from the keyboard
 * 
 * minSize - minimum length of command
 * maxSize - maximum length of command
 * commandStr - command for program
 * 
 * @author KOL
 */

public class ScanCommand {

	private String commandStr;
	private final int minSize;
	private final int maxSize;

	ScanCommand(int minSize, int maxSize) {

		this.minSize = minSize;
		this.maxSize = maxSize;

	}

	/**
	 * Method that gets command for FTP-server of the keyboard and checks it, if
	 * the command is not correct procedure is repeated, else method return it
	 * 
	 * @return - command
	 */

	public String scanStringCommand() {

		String str = "";
		do {
			BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
			System.out.print(str + "Input your command: ");
			try {
				commandStr = read.readLine();
			} catch (IOException e) {
				System.out.println("ERROR! " + e);
			}
			str = "ERROR!!! ";
		} while (commandStr.length() < minSize || commandStr.length() > maxSize);
		commandStr = commandStr.toUpperCase();

		return commandStr;
	}

	/**
	 * Method reads a term that should be the name of the folder or file
	 * 
	 * @param name - string that is displayed on the screen that prompts you to enter information
	 * @return name of file or directory
	 */
	
	public String scanDirName(String name) {

		System.out.print("Input " + name + ": ");
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		String nameDir = null;
		try {
			nameDir = read.readLine();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		return nameDir;

	}

	/**
	 * Method reads a term yes/no or y/n
	 * 
	 * @return answer yes/no 
	 */
	
	public String scanYesOrNo() {

		System.out.print("Are you sure? Y/N: ");
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		String yesOrNo = null;
		try {
			yesOrNo = read.readLine();
		} catch (IOException e) {
			System.out.println("ERROR! " + e);
		}
		yesOrNo = yesOrNo.toUpperCase();
		return yesOrNo;

	}

}