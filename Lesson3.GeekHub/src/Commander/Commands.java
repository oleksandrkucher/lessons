package Commander;

import java.io.*;
import java.util.Date;

/**
 * Class contains methods for copying files
 * 
 * root - root folder
 * separator - separator of files in different OS
 * folderPath - path to current folder or file
 * scan - variable for reading commands from the keyboard

 * 
 */

public class Commands {

	private final String root;
	private String folderPath;
	private final String separator;
	private ScanCommand scan = new ScanCommand(3, 4);

	/**
	 * Constructor initializes variables root, separator and folderPath
	 * 
	 * @param root - root folder
	 * @param separator - separator of files in different OS
	 */
	
	public Commands(String root, String separator) {

		this.root = root;
		folderPath = this.root;
		this.separator = separator;

	}

	/**
	 * Method create a directory
	 * @throws IOException
	 */
	
	public void makeDir() throws IOException {

		String nameDir = scan.scanDirName("name of new directory");
		File newDir = new File(folderPath + separator + nameDir);
		if(newDir.exists()){
			System.out.println("Error! This directory is already exists. Change name and try again.\n");
		} else {
			newDir.mkdir();
			if(newDir.exists()){
				System.out.println("Directory is created.\n");
			} else {
				System.out.println("Error! Directory is not created.\n");
			}
		}
	}

	/**
	 * Method change directory to a higher
	 */
	
	public void cdUp() {

		if(!folderPath.equals(root)){
			File dir = new File(folderPath);
			folderPath = dir.getParent();
			System.out.println(folderPath + "\n");
		} else {
			System.out.println("This directory is root. You can not change it.");
		}
	}

	/**
	 * Method changes the active directory
	 * @throws IOException
	 */
	
	public void changeDir() throws IOException {

		String nameDir = scan.scanDirName("name of directory");
		File change = null;
		change = new File(folderPath + separator + nameDir);
		if (change.exists() && change.isDirectory()) {
			folderPath = folderPath + separator + nameDir;
			System.out.println(folderPath + "\n");
		} else {
			System.out.println("Wrong name of directory.");
		}

	}

	/**
	 * Method prints list with subsidiary information of files in current directory
	 */
	
	public void list() {

		File file = new File(folderPath);
		String[] str = file.list();
		if (str.length == 0) {
			System.out.println("Directory is empty.");
		} else {
			System.out.println(folderPath + "\nLIST: ");
			System.out.println("Name\t\t\t\tType\t\t\t\tSize");
			System.out.println("=================================================="
				+ "============================");
			for (int i = 0; i < str.length; i++) {
				String tmp = null;
				if(str[i].length() >= 16){
					int index = str[i].lastIndexOf(".");
					if(index > 0){
						tmp = str[i].substring(0, index - (str[i].length() - 16)) + "~...~" + str[i].substring(index);
					} else {
						tmp = str[i].substring(0, str[i].length() - (str[i].length() - 16)) + "~...~";
					}
					System.out.print(tmp);
				} else {
					System.out.print(str[i]);
				}
				listPrintType(str[i]);
				listPrintSize(str[i]);
			}
		}
		System.out.println();

	}

	/**
	 * Method prints path to current directory
	 */
	
	public void printCurrentDir() {

		System.out.println(folderPath + "\n");
	}

	/**
	 * Method delete one file
	 * @throws IOException
	 */
	
	public void delFile() throws IOException {

		String name = scan.scanDirName("name of file what you want to delete");
		if (name.length() > 0) {
			File del = new File(folderPath + separator + name);
			if (del.exists()) {
				if (del.isFile()) {
					if (yesOrNo() && del.delete()) {
						System.out.println("File deleted!!!");
					}
				} else {
					System.out.println("This is directory!!!");
				}
			} else {
				System.out.println("File does not exist!!!");
			}
		} else {
			System.out.println("ERROR!!! You must input name of file.");
		}
		System.out.println();

	}

	/**
	 * Method delete one empty directory
	 * @throws IOException
	 */
	
	public void delDir() throws IOException {

		String name = scan.scanDirName("name of directory what you want to delete");
		if (name.length() > 0) {
			File del = new File(folderPath + separator + name);
			if (del.exists()){
				if(del.isDirectory()){
					if (yesOrNo() && del.delete()) {
						System.out.println("Directory deleted!!!");
					} else {
						System.out.println("ERROR!!! Directory is not empty!!! "
								+ "Delete all files and directories from it and delete this directory at last.");
					}
				} else {
					System.out.println("This is file!!!");
				}
			} else {
				System.out.println("Directory does not exist!!!");
			}
		} else {
			System.out.println("ERROR!!! You must input name of directory.");
		}
		System.out.println();

	}

	/**
	 * Method displays time of last file modification
	 * @throws IOException
	 */
	
	public void lastModify() throws IOException {

		String name = scan.scanDirName("name of file or directory");
		if (name.length() > 0) {
			File mod = new File(folderPath + separator + name);
			if (mod.exists()) {
				Date date = new Date(mod.lastModified());
				System.out.println("Last modify = " + date);
			} else {
				System.out.println("File or directory is not exists.");
			}
		} else {
			System.out.println("ERROR!!! You must input name of file or directory.");
		}
		System.out.println();
		
	}

	/**
	 * Method displays size of file
	 * @throws IOException
	 */
	
	public void size() throws IOException {

		String name = scan.scanDirName("name of file");
		if (name.length() > 0) {
			File nameSize = new File(folderPath + separator + name);
			if (nameSize.exists()) {
				if (nameSize.isDirectory()) {
					System.out.println(name + " is directory.");
				} else {
					double size = nameSize.length();
					if (size < 1024) {
						System.out.println(size + " bytes");
					} else if(size < 1024*1024){
						System.out.printf("%.3f Kb\n", size / 1024);
					} else if(size < 1024*1024*1024){
						System.out.printf("%.3f Mb\n", size / (1024*1024));
					} else {
						System.out.printf("%.3f Gb\n", size / (1024*1024*1024));
					}
				}
			} else {
				System.out.println("File is not exists.");
			}
		} else {
			System.out.println("ERROR!!! You must input name of file.");
		}
		
		System.out.println();
	}

	/**
	 * Method renames file or empty directory
	 * @throws IOException
	 */
	
	public void rename() throws IOException {

		String oldName = scan.scanDirName("old name of file or directory");
		String oldPath = folderPath + separator + oldName;
		String newName = scan.scanDirName("new name of file or directory");
		String newPath = folderPath + separator + newName;
		File old = new File(oldPath);
		File newN = new File(newPath);
		if (old.exists()) {
			if (old.renameTo(newN)) {
				System.out.println("File or directory renamed.");
			} else if (old.isDirectory()) {
				System.out.println("ERROR!!! Directory is not empty!!! "
								+ "Delete all files and directories from it and rename this directory at last.");
			} else {
				System.out.println("ERROR!!! File is not renamed!!!");
			}
		} else {
			System.out.println("ERROR!!! File or directory is not exist.");
		}
		System.out.println();
	}

	/**
	 * Method prints help file to console
	 * @throws IOException
	 */
	
	public void help() throws IOException {

		String dir;
		dir = System.getProperty("user.dir");
		File help = new File(dir + separator + "src/Commander/help.txt");
		if(help.exists()){
			String s = "";
			FileReader in = null;
			try {
				in = new FileReader(help);
			} catch (FileNotFoundException e) {
				System.out.print("ERROR! " + e);
			}
			int tmp = 0;
			try {
				while ((tmp = in.read()) != -1) {
					s += (char)tmp;
				}
			} catch (FileNotFoundException e) {
				System.out.println("ERROR! " + e);
			} finally {
				if(in != null){
					in.close();
				}
			}
			System.out.println("HELP:\n" + s);
		} else{
			System.out.println("Help file is lost.\n");
		}
	}

	/**
	 * Method asks you to confirm your choice
	 * @return boolean value true or false
	 * @throws IOException
	 */
	
	private boolean yesOrNo() throws IOException {

		String yesOrNo = scan.scanYesOrNo();
		switch (yesOrNo) {
		case "YES":
		case "Y":
			return true;
		case "NO":
		case "N":
			return false;
		default:
			return false;
		}
	}

	/**
	 * Method deletes directory with all files and directories in it
	 * @param path
	 * @throws IOException
	 */
	
	public void delAllDir(String path) throws IOException {

		File del = new File(path);
		if (del.exists()) {
			if (del.isDirectory()) {
				if (yesOrNo() && !del.delete()) {
					delAll(del, path);
					del.delete();
					System.out.println("Directories deleted!!!");
				}
			} else {
				System.out.println("This is file!!!");
			}
		} else {
			System.out.println("Directory does not exist!!!");
		}
	}

	/**
	 * Method reads name of directory or file and return path to it
	 * @return path to file or directory 
	 * @throws IOException
	 */
	
	public String path() throws IOException {

		String name = scan.scanDirName("name of directory");
		String path = folderPath + separator + name;
		return path;
	}

	/**
	 * Secondary recursive method to remove not empty folders
	 * @param del
	 * @param path
	 * @throws IOException
	 */
	
	private void delAll(File del, String path) throws IOException {

		File file = new File(path);
		String[] str = file.list();
		for (int i = 0; i < str.length; i++) {
			File del1 = new File(path + separator + str[i]);
			if (!del1.delete()) {
				delAll(del1, path + separator + str[i]);
			}
		}
		del.delete();
	}

	/**
	 * Method get info about Operation System and prints it
	 */
	
	public void whatOS() {

		String os = System.getProperty("os.name").toLowerCase();
		String osVersion = System.getProperty("os.version");
		String osArch = System.getProperty("os.arch");
		System.out.println("Your OS: " + os + "\nArchitecture: " + osArch
				+ "V\nersion: " + osVersion + "\n");
	}

	/**
	 * Method get separator and root for this OS
	 * @return
	 */
	
	public static Commands whatOSC() {

		String separator = System.getProperty("file.separator");
		String home = System.getProperty("user.home");
		return new Commands(home, separator);
	}

	/**
	 * Method prints type of files for method list
	 * @param str - name of folder or file
	 */
	
	private void listPrintType(String str) {

		File list = new File(folderPath + separator + str);
		if (list.isDirectory()) {
			if (str.length() <= 7) {
				System.out.print("\t\t\t\t[DIR]");
			} else if (str.length() > 7 && str.length() < 16) {
				System.out.print("\t\t\t[DIR]");
			} else {
				System.out.print("\t\t[DIR]");
			}
		} else {
			int index = str.lastIndexOf(".");
			String tmp = str.substring(index + 1, str.length());
			int len = str.length();
			if (len <= 7) {
				System.out.print("\t\t\t\t" + tmp);
			} else if (len >= 8 && len < 16) {
				System.out.print("\t\t\t" + tmp);
			} else {
				System.out.print("\t\t" + tmp);
			}
			len = tmp.length();
			if (tmp.length() <= 7) {
				System.out.print("\t\t\t\t");
			} else if (len >= 8 && len < 16) {
				System.out.print("\t\t\t");
			} else {
				System.out.print("\t\t");
			}
		}
	}

	/**
	 * Method prints size of files for method list
	 * @param str - name of folder or file
	 */
	
	private void listPrintSize(String str) {

		File nameSize = new File(folderPath + separator + str);
		if (nameSize.isFile()) {
			double size = nameSize.length();
			if (size < 1024) {
				System.out.println(size + " bytes");
			} else if(size < 1024*1024){
				System.out.printf("%.3f Kb\n", size / 1024);
			} else if(size < 1024*1024*1024){
				System.out.printf("%.3f Mb\n", size / (1024*1024));
			} else {
				System.out.printf("%.3f Gb\n", size / (1024*1024*1024));
			}
		} else {
			System.out.println();
		}
	}

	/**
	 * Method prints list of files in current directory without subsidiary information
	 */
	
	public void nList() {

		File file = new File(folderPath);
		String[] str = file.list();
		System.out.println("LIST:\n=================================");
		for (int i = 0; i < str.length; i++) {
			System.out.println(str[i]);
		}
	}
}