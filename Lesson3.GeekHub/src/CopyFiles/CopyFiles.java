package CopyFiles;

import java.io.*;
import java.util.Date;

/**
 * Main class for copying files
 * 
 * @author kol
 *
 */

public class CopyFiles {

	public static void main(String[] args) throws IOException {
		
		CopyMethods copy = new CopyMethods();
		System.out.println("The root folder: " + CopyMethods.root);
		long start = copy.chooseCopyAndGetTime();
		long finish = new Date().getTime();
		
		System.out.println("Copying is completed. time = " + (finish - start)/1000 + " sec");

	}

}
