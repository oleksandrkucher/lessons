package CopyFiles;

import java.io.*;
import java.util.Date;
 
  /**
  * Class contains methods for copying files
  * 
  * root - root folder
  * separator - separator of files in different OS
  * in - input stream
  * out - output stream
  * 
  * @author kol
  */

public class CopyMethods {

	static String root;
	static String separator;
	private InputStream in;
	private OutputStream out;
	
	/**
	 * Constructor initializes variables root and separator
	 */
	
	public CopyMethods(){
		
		root = System.getProperty("user.home");;
		separator = System.getProperty("file.separator");
	}
	
	/**
	 * Method reads string from keyboard
	 *  
	 * @param string - out string
	 * @return string which was read from keyboard 
	 */
	
	private String getStr(String string){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		System.out.print("Input name or path of "+ string +" file: ");
		try {
			str = buf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Method create variable of type File and verifies if it exists
	 * 
	 * @return File
	 */
	
	private File getInFile(){
		
		File fileIn = new File(root + separator + getStr("input"));
		if(fileIn.exists()){
			return fileIn;
		} else {
			return getInFile();
		}
	}
	
	/**
	 * Method create input and output stream
	 * 
	 * @throws FileNotFoundException
	 */
	
	private void getInAndOut() throws FileNotFoundException{

		File fileIn = getInFile();
		in = new FileInputStream(fileIn);
		
		try {
			out = new FileOutputStream(root + separator + getStr("copy"));
		} catch (FileNotFoundException e) {
			System.out.print("ERROR! " + e);
		}
	}
	
	/**
	 * Method create buffered input and output stream
	 */
	
	private void getInAndOutBuffered() throws FileNotFoundException{
		
		File fileIn = getInFile();
		in = new BufferedInputStream(new FileInputStream(fileIn));
		File fileOut = new File(root + separator + getStr("copy"));
		
		try {
			out = new BufferedOutputStream(new FileOutputStream(fileOut));
		} catch (FileNotFoundException e) {
			System.out.println("ERROR! " + e);
		}
	}

	/**
	 * Method copies the file
	 */
	
	private void copy(){
		
		int tmp;
		try {
			while((tmp = in.read()) != -1){	
				out.write(tmp);
			}
		} catch (IOException e) {
			System.out.print("ERROR! " + e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				System.out.print("ERROR! " + e);
			}
			
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				System.out.print("ERROR! " + e);
			}
		}
	}

	/**
	 * Method gets integer number from keyboard
	 * 
	 * @return integer number
	 */
	
	private int getNumber(){
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		try {
			return Integer.parseInt(buf.readLine());
		} catch (NumberFormatException | IOException e) {
			System.out.println("ERROR! " + e);
			return getNumber();
		}
	}
	
	/**
	 * Method enables to select the type of copying
	 * 
	 * @return time in milliseconds
	 * @throws FileNotFoundException
	 */
	
	public long chooseCopyAndGetTime() throws FileNotFoundException{
		
		System.out.print("1 - Buffered Copy\n2 - Copy\nInput number: ");
		int number = getNumber();
		long time = 0;
		
		if(number == 1){
			getInAndOutBuffered();
			time = new Date().getTime();
			copy();
		} else if(number == 2){
			getInAndOut();
			time = new Date().getTime();
			copy();
		} else {
			System.out.println("ERROR! Try again.");
			chooseCopyAndGetTime();
		}
		return time;
	}

}
