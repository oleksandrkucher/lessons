package Fibonacci;

import java.io.*;

/**
 * Class gets one integer number n, and display n Fibonacci numbers
 * 
 * @author kol
 *
 */

public class FibonacciSequence {

	public static void main(String[] args) throws IOException{
		
		FibonacciSequence fibonacci = new FibonacciSequence();
		int n = fibonacci.getN();
		fibonacci.fibonacciCalculateAndPrint(n);
		
	}

	/**
	 * Method gets integer number from keyboard
	 * 
	 * @return one integer number
	 */
	
	public int getN() throws IOException{

		int n = 0;
		String str = null;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.print("Input n (n > 0): ");
			try {
				str = buf.readLine();
				n = Integer.parseInt(str);
			} catch (NumberFormatException e) {
				System.out.print("ERROR!!! You must input integer number: ");
			}
		} while(n <= 0);
		return n;

	}

	/**
	 * Method calculates and print n Fibonacci numbers
	 * @param n - number of Fibonacci numbers
	 */
	
	public void fibonacciCalculateAndPrint(int n) {

		int f0 = 0;
		int f1 = 1;
		int f;
		System.out.print("Fibonacci numbers: " + f0);
		for(int i = 0; i < n; i++){
			System.out.print(", ");
			f = f0 + f1;
			f0 = f1;
			f1 = f;
			System.out.print(f0);
		}
	}
}
