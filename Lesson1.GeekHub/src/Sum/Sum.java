package Sum;

import java.io.*;

/**
 * Class gets two number (in the base 8, 10, 16) 
 * and calculates their sum in the base 10
 * 
 * @author kol
 */

public class Sum {

	private int radix;
	
	public static void main(String[] args) throws IOException {
		
		Sum sum = new Sum();
		sum.getRadix();
		int n = sum.scan();
		int m = sum.scan();
		int result = sum.sum(n, m);
		System.out.println("Sum in the base 10 = " + result);
	}
	
	/**
	 * Method adds two terms and return result
	 * 
	 * @param n - first summand
	 * @param m - second summand
	 * @return sum n+m
	 */
	
	public int sum(int n, int m){
		
		return n+m;	
	}
	
	/**
	 * Method gets the value from the keyboard
	 * 
	 * @return - the received value
	 */
	
	public int scan() throws IOException{
		
		String str = null;
		int number;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input number: ");
		try {
			str = buf.readLine();
			number = Integer.parseInt(str, radix);
		} catch (NumberFormatException e) {
			System.out.println("ERROR!!! You must input integer number!");
			return scan();
		}
		return number;
	}

	/**
	 * Method gets the value from the keyboard as base of the number system
	 * 
	 * @return - the received value
	 */
	
	public void getRadix() throws IOException{
		
		String str = null;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.print("Input radix (8, 10 or 16): ");
			try {
				str = buf.readLine();
				radix = Integer.parseInt(str);
			} catch (NumberFormatException e) {
				System.out.println("ERROR!!! You must input integer number!");
			}
		} while(radix != 8 && radix != 10 && radix != 16);
	}
}
