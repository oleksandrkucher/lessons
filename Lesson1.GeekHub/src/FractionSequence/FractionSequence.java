package FractionSequence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class gets integer number n and displays sequence
 * 
 * @author kol
 *
 */

public class FractionSequence {
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		FractionSequence fractions = new FractionSequence();
		int n = fractions.getN();
		fractions.fractionSequence(n);

	}

	/**
	 * Method gets integer number from keyboard
	 * 
	 * @return one integer number
	 */
	
	public int getN() throws IOException{

		int n = 0;
		String str = null;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.print("Input n (n > 0): ");
			try {
				str = buf.readLine();
				n = Integer.parseInt(str);
			} catch (NumberFormatException e) {
				System.out.print("ERROR!!! You must input integer number: ");
			}
		} while(n <= 0);
		return n;

	}
	
	/**
	 * Method calculates all elements of sequence by the formula 1/n and print it
	 * 
	 * @param n - number of elements in the sequence
	 */
	
	public void fractionSequence(int n){
		
		double result;
		for(int i = 1; i <= n; i++){
			result = 1d/i;
			System.out.printf("%.5f  ", result);
		}

	}
	
}
