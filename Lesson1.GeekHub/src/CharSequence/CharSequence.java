package CharSequence;

import java.io.*;

/**
 * Class gets two characters and displays all characters between
 * 
 * @author kol
 *
 */

public class CharSequence {

	public static void main(String[] args) throws IOException {
		
		CharSequence charSequence = new CharSequence();
		char n = charSequence.getChar('n');
		char m = charSequence.getChar('m');
		charSequence.printChars(n, m);
		
	}
	
	/**
	 * Method gets character and return it
	 * 
	 * @param name - name of character
	 * @return - one character
	 */
	
	public char getChar(char name) throws IOException{
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Input " + name + ": ");
		return (char) buf.read();
	}
	
	/**
	 * Method displays all characters between n and m
	 * 
	 * @param n - first character
	 * @param m - second character
	 */
	
	public void printChars(char n, char m){
		
		if(m < n){
			for(char i = m; i <= n; i++){
				System.out.print(i + " ");
			}
		} else {
			for(char i = n; i <= m; i++){
				System.out.print(i + " ");
			}
		}
			
	}

}
