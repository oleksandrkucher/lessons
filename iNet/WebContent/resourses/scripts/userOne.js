function leaveGroup(id) {
	$.ajax({
		url : '/iNet/group/leaveGroup.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#leave");
			var newHref = '<a id="become" href="javascript:void()" onclick="becomeMember(' + id + ')">Became a member of group</a>';
			element.replaceWith(newHref);
		},
	});
}
function becomeMember(id) {
	$.ajax({
		url : '/iNet/group/becomeMember.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#become");
			var newHref = '<a id="leave" href="javascript:void()" onclick="leaveGroup(' + id + ')">Leave group</a>';
			element.replaceWith(newHref);
		},
	});
}
function addToFollowing(id) {
	$.ajax({
		url : '/iNet/userPersonal/addFollowing.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#addFollowing");
			var newHref = '<a id="deleteFollowing" href="#" onclick="deleteFollowing(${user.id})">Remove from following</a>';
			element.replaceWith(newHref);
		},
	});
}
function deleteFollowing(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteFollowing.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#deleteFollowing");
			var newHref = '<a id="addFollowing" href="#" onclick="addToFollowing(${user.id})">Add to following</a>';
			element.replaceWith(newHref);
		},
	});
}
function deleteFriend(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteFriend.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#deleteFriend");
			var newHref = '<a id="addFriend" href="#" onclick="addFriend(${user.id})">Add to friends</a>';
			element.replaceWith(newHref);
		},
	});
}
function addFriend(id) {
	$.ajax({
		url : '/iNet/userPersonal/addFriend.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#addFriend");
			var newHref = '<a id="deleteFriend" href="#" onclick="deleteFriend(${user.id})">Remove from friends</a>';
			element.replaceWith(newHref);
		},
	});
}
function deleteBookmark(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteBookmark.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#deleteBookmark");
			var newHref = '<a id="addBookmark" href="#" onclick="addBookmark(${user.id})">Add to bookmarks</a>';
			element.replaceWith(newHref);
		},
	});
}
function addBookmark(id) {
	$.ajax({
		url : '/iNet/userPersonal/addBookmark.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#addBookmark");
			var newHref = '<a id="deleteBookmark" href="#" onclick="deleteBookmark(${user.id})">Remove from bookmarks</a>';
			element.replaceWith(newHref);
		},
	});
}
function deleteFromBlacklist(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteFromMyBlacklist.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#deleteFromBlacklist");
			var newHref = '<a id="addToBlacklist" href="#" onclick="addToBlacklist(${user.id})">Add to blacklist</a>';
			element.replaceWith(newHref);
		},
	});
}
function addToBlacklist(id) {
	$.ajax({
		url : '/iNet/userPersonal/addToMyBlacklist.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#addToBlacklist");
			var newHref = '<a id="deleteFromBlacklist" href="#" onclick="deleteFromBlacklist(${user.id})">Delete from blacklist</a>';
			element.replaceWith(newHref);
		},
	});
}
function deletePost(id) {
	$.ajax({
		url : '/iNet/userPersonal/deletePost.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function(response) {
			if (response == "true") {
				var element = $("#" + id);
				element.hide(true);
			}
		},
	});
}
function deleteComment(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteComment.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function(response) {
			if (response == "true") {
				var element = $("#comm" + id);
				element.hide(true);
			}
		},
	});
}
function changeLikePost(id) {
	$.ajax({
		url : '/iNet/userPersonal/changeLikeForPost.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function(response) {
			if (response.indexOf("|") != -1) {
				var element = $("#like" + id);
				var dislike = response.substring(0, response.indexOf("|"));
				var like = response.substring(response.indexOf("|") + 1,
						response.length);
				var newElement = '<button id="like' + id
						+ '" onclick="changeLikePost(' + id + ')">Like: ' + like
						+ '</button>';
				element.replaceWith(newElement);
				element = $("#dislike" + id);
				newElement = '<button id="dislike' + id
						+ '" onclick="changeDisLikePost(' + id + ')">Dislike: '
						+ dislike + '</button>';
				element.replaceWith(newElement);
			} else {
				var element = $("#like" + id);
				var newElement = '<button id="like' + id
						+ '" onclick="changeLikePost(' + id + ')">Like: '
						+ response + '</button>';
				element.replaceWith(newElement);
			}
		},
	});
}
function changeLikeComm(id) {
	$.ajax({
		url : '/iNet/userPersonal/changeLikeForComment.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function(response) {
			if (response.indexOf("|") != -1) {
				var element = $("#likeComm" + id);
				var dislike = response.substring(0, response.indexOf("|"));
				var like = response.substring(response.indexOf("|") + 1,
						response.length);
				var newElement = '<button id="likeComm' + id
				+ '" onclick="changeLikeComm(' + id + ')">Like: ' + like
				+ '</button>';
				element.replaceWith(newElement);
				element = $("#dislikeComm" + id);
				newElement = '<button id="dislikeComm' + id
				+ '" onclick="changeDisLikeComm(' + id + ')">Dislike: '
				+ dislike + '</button>';
				element.replaceWith(newElement);
			} else {
				var element = $("#likeComm" + id);
				var newElement = '<button id="likeComm' + id
				+ '" onclick="changeLikeComm(' + id + ')">Like: '
				+ response + '</button>';
				element.replaceWith(newElement);
			}
		},
	});
}
function changeDisLikePost(id) {
	$.ajax({
		url : '/iNet/userPersonal/changeDisLikeForPost.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function(response) {
			if (response.indexOf("|") != -1) {
				var element = $("#like" + id);
				var dislike = response.substring(0, response.indexOf("|"));
				var like = response.substring(response.indexOf("|") + 1,
						response.length);
				var newElement = '<button id="like' + id
						+ '" onclick="changeLikePost(' + id + ')">Like: ' + like
						+ '</button>';
				element.replaceWith(newElement);
				element = $("#dislike" + id);
				newElement = '<button id="dislike' + id
						+ '" onclick="changeDisLikePost(' + id + ')">Dislike: '
						+ dislike + '</button>';
				element.replaceWith(newElement);
			} else {
				var element = $("#dislike" + id);
				var newElement = '<button id="dislike' + id
						+ '" onclick="changeDisLikePost(' + id + ')">Dislike: '
						+ response + '</button>';
				element.replaceWith(newElement);
			}
		},
	});
}
function changeDisLikeComm(id) {
	$.ajax({
		url : '/iNet/userPersonal/changeDisLikeForComment.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function(response) {
			if (response.indexOf("|") != -1) {
				var element = $("#likeComm" + id);
				var dislike = response.substring(0, response.indexOf("|"));
				var like = response.substring(response.indexOf("|") + 1,
						response.length);
				var newElement = '<button id="likeComm' + id
				+ '" onclick="changeLikeComm(' + id + ')">Like: ' + like
				+ '</button>';
				element.replaceWith(newElement);
				element = $("#dislikeComm" + id);
				newElement = '<button id="dislikeComm' + id
				+ '" onclick="changeDisLikeComm(' + id + ')">Dislike: '
				+ dislike + '</button>';
				element.replaceWith(newElement);
			} else {
				var element = $("#dislikeComm" + id);
				var newElement = '<button id="dislikeComm' + id
				+ '" onclick="changeDisLikeComm(' + id + ')">Dislike: '
				+ response + '</button>';
				element.replaceWith(newElement);
			}
		},
	});
}
function inputComment(id, userId) {
	$("#" + id).append(
			'<form action="/iNet/userPersonal/addNewComment.html" method="post">'
					+ '<textarea rows="2" cols="8" name="comment"></textarea>'
					+ '<input type = "hidden" name="id" value="' + id + '">'
					+ '<input type = "hidden" name="userId" value="' + userId
					+ '">' + '<input type = "submit" value = "Add comment">'
					+ '</form>');
	document.getElementById('butCom').disabled = true;
}
function inputCommentToGroup(id, groupId) {
	$("#" + id).append(
			'<form action="/iNet/group/addNewCommentToGroup.html" method="post">'
			+ '<textarea rows="2" cols="8" name="comment"></textarea>'
			+ '<input type = "hidden" name="id" value="' + id + '">'
			+ '<input type = "hidden" name="groupId" value="' + groupId
			+ '">' + '<input type = "submit" value = "Add comment">'
			+ '</form>');
	document.getElementById('butCom').disabled = true;
}
function whoOnline(id) {
	$.ajax({
		url : '/iNet/whoIsOnline.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : 'text',
	});
}