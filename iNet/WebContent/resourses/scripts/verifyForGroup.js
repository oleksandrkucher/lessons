var titleStatus = true;
var uniqueIdStatus = true;

function ifAllReady() {
	if (titleStatus && uniqueIdStatus) {
		document.getElementById('submit').disabled = false;
	} else {
		document.getElementById('submit').disabled = true;
	}
}
function disableSubmit() {
	document.getElementById('submit').disabled = true;
	titleStatus = false;
	uniqueIdStatus = false;
}
function verifyTitle(title) {
	if(title.length > 0){
		titleStatus = true;
	} else {
		titleStatus = false;
	}
	ifAllReady();
}

function verifyUniqueId(uniqueId) {
	if(uniqueId.length > 0){
	$.ajax({
		url : '/iNet/group/verifyGroupUniqueId.html',
		type : 'POST',
		data : {
			val : uniqueId
		},
		dataType : "text",
		success : function(response) {
			if (response == "true") {
				uniqueIdStatus = true;
				$("#uniqueId")
						.replaceWith(
								'<label data-iconOK="ok"></label>');
			} else {
				uniqueIdStatus = false;
				$("#uniqueId")
						.replaceWith(
								'<label data-iconNO="No, try again"></label>');
			}
			ifAllReady();
		},
	});
	} else {
		uniqueIdStatus = false;
	}
	ifAllReady();
}