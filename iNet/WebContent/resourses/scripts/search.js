function search(str){
	$.ajax({
		url: '/iNet/userPersonal/searchAJAX.html',
		type: 'POST',
		data: {searchValue: str}, 
		dataType: "text",
		success: function(response){
			var element = $("#s");
			var obj = jQuery.parseJSON(response);
			var users = '<div id="s">';
			if(obj.length > 0){
				for(var i = 0; i < obj.length; i++){
					users = users + '<div id="' + obj[i].id + '" class="profile_info">' + 
						'<div class="clear_fix ">' +
							'<div class="label fl_l"><a href="/iNet/userPersonal/' + obj[i].uniqueId + '.html"><img src="/iNet/resourses/usersPhotos/' + obj[i].linkToFoto + '"></a></div>' + 
							'<div class="labeled fl_l" style="width: 30%;">' + 
								'<h3><a href="/iNet/userPersonal/' + obj[i].uniqueId + '.html">' + obj[i].firstName + ' ' + obj[i].lastName + '</a><h3>'; 
								if(obj[i].collegeOrUniversityName != undefined){
									users = users + '<h5>' + obj[i].collegeOrUniversityName;
								} else {
									users = users + '<h5>';
								}
								if(obj[i].graduationYear != undefined){
									users = users + obj[i].graduationYear + '</h5>';
								} else {
									users = users + '</h5>';
								}
							users = users + '</div>' + 
							'<div class="label fl_r">' +
								'<h5><a href="sendMessage.html?id=' + obj[i].id + '">Send mail</a></h5>' +
								'<h5><a href="browseFriends.html?id=' + obj[i].id + '">Browse friends</a></h5>' +
							'</div>' +
						'</div>' + 
					'</div><br>';
				}
				users = users + '<div>';
			} else {
				users = users + "<h3>We can't find anyone user! Try again, please.<h3><div>";
			}
			element.replaceWith(users);
		},
	});

}