var emailStatus = true;
var passStatus = true;
var passConfirm = true;
var firstName = true;
var lastName = true;
var uniqueIdStatus = true;

function ifAllReady() {
	/*alert(emailStatus + "\n" + passStatus + "\n" + passConfirm + "\n"
			+ firstName + "\n" + lastName + "\n" + uniqueIdStatus);*/
	if (emailStatus && passStatus && passConfirm && firstName && lastName
			&& uniqueIdStatus) {
		document.getElementById('submit').disabled = false;
	} else {
		document.getElementById('submit').disabled = true;
	}
}
function disableSubmit() {
	document.getElementById('submit').disabled = true;
	emailStatus = false;
	passStatus = false;
	passConfirm = false;
	firstName = false;
	lastName = false;
	uniqueIdStatus = false;
}

function verifyEmail(email) {
	$.ajax({
		url : '/iNet/verifyEmail.action',
		type : 'POST',
		data : {
			val : email
		},
		dataType : "text",
		success : function(response) {
			if (response == "true") {
				emailStatus = true;
				$("#nameStatus")
						.replaceWith(
								'<label id="nameStatus" data-iconOK="ok"></label>');
			} else {
				emailStatus = false;
				$("#nameStatus")
						.replaceWith(
								'<label id="nameStatus" data-iconNO=\'This not a mail\'></label>');
			}
			ifAllReady();
		},
	});
}
function verifyUniqueId(uniqueId) {
	$.ajax({
		url : '/iNet/verifyUniqueId.action',
		type : 'POST',
		data : {
			val : uniqueId
		},
		dataType : "text",
		success : function(response) {
			if (response == "true") {
				uniqueIdStatus = true;
				$("#uniqueId")
						.replaceWith(
								'<label id="uniqueId" data-iconOK="ok"></label>');
			} else {
				uniqueIdStatus = false;
				$("#uniqueId")
						.replaceWith(
								'<label id="uniqueId" data-iconNO=\'No, try again\'></label>');
			}
			ifAllReady();
		},
	});
}
function verifyPass(pass, confirm) {
	if (pass.length < 6) {
		$("#passStatus").replaceWith(
				'<label id="passStatus" data-iconNO="So short. Min 6 chars"></label>');
		$("#passConfStatus").replaceWith(
				'<label id="passConfStatus" data-iconNO="Wrong"></label>');
		passStatus = false;
		passConfirm = false;
	} else if (pass == confirm) {
		$("#passStatus").replaceWith(
				'<label id="passStatus" data-iconOK="ok"></label>');
		$("#passConfStatus").replaceWith(
				'<label id="passConfStatus" data-iconOK="ok"></label>');
		passStatus = true;
		passConfirm = true;
	} else {
		$("#passStatus").replaceWith(
				'<label id="passStatus" data-iconNO="Wrong"></label>');
		$("#passConfStatus").replaceWith(
				'<label id="passConfStatus" data-iconNO="Wrong"></label>');
		passStatus = false;
		passConfirm = false;
	}
	ifAllReady();
}
function verifyLastName(name) {
	if (name.length < 1) {
		lastName = false;
		$("#lastName").replaceWith(
				'<label id="lastName" data-iconNO="So short"></label>');
	} else {
		lastName = true;
		$("#lastName").replaceWith(
				'<label id="lastName" data-iconOK="ok"></label>');
	}
	ifAllReady();
}
function verifyFirstName(name) {
	if (name.length < 1) {
		firstName = false;
		$("#firstName").replaceWith(
				'<label id="firstName" data-iconNO="Wrong"></label>');
	} else {
		firstName = true;
		$("#firstName").replaceWith(
				'<label id="firstName" data-iconOK="ok"></label>');
	}
	ifAllReady();
}