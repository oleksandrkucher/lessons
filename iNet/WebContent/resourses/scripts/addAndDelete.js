function deleteBookmark(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteBookmark.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#" + id);
			element.hide(true);
		},
	});
}
function deleteMessage(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteMessage.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#" + id);
			element.hide(true);
		},
	});
}
function addToFollowing(id) {
	$.ajax({
		url : '/iNet/userPersonal/addFollowing.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#addFollowing");
			var newHref = '<a id="deleteFollowing" href="#" onclick="deleteFollowing(${user.id})">Remove from following</a>';
			element.replaceWith(newHref);
		},
	});
}
function addFriend(id) {
	$.ajax({
		url : '/iNet/userPersonal/addFriend.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#" + id);
			element.hide(true);
		},
	});
}
function deleteFollow(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteFollowing.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#" + id);
			element.hide(true);
		},
	});
}
function deleteFriend(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteFriend.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#" + id);
			element.hide(true);
		},
	});
}
function deleteFromBlacklist(id) {
	$.ajax({
		url : '/iNet/userPersonal/deleteFromMyBlacklist.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#" + id);
			element.hide(true);
		},
	});
}
function addToBlacklist(id) {
	$.ajax({
		url : '/iNet/userPersonal/addToMyBlacklist.html',
		type : 'POST',
		data : {
			id : id
		},
		dataType : "text",
		success : function() {
			var element = $("#"+id);
			element.hide(true);
		},
	});
}
function howManyNew() {
	$.ajax({
		url : '/iNet/userPersonal/howManyNewMessages.html',
		type : 'POST',
		dataType : "text",
		success : function(response) {
			var element = $("#new");
			var newElem = '<a href="/iNet/userPersonal/receivedMessages.html" id="new">Received messages +' + response + '</a>';
			element.replaceWith(newElem);
		},
	});
}