<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <title>iNet Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resourses/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resourses/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resourses/css/style.css" />
    </head>
    <body>
        <div class="container">
            <header><h1>Login</h1></header>
            <section>               
                <div id="container_demo" >
                    <div id="wrapper">
                        <div id="login" class="animate form"><!--j_spring_security_check-->
                            <form action="/iNet/login.action" autocomplete="on" method="POST"> 
                                <h1>Log in</h1> 
	                        	<div>
	                        		<c:if test="${not empty fn:replace(error, ' ', '')}">
	                        			<h2 style="text-align: center; color: red;"><c:out value="${error}"></c:out></h2>
									</c:if>
								</div>
                                <p> 
                                    <label data-icon="e" >Your email</label>
                                    <input name="j_username" required="required" type="email" placeholder="myMail@mail.com"/>
                                </p>
                                <p> 
                                    <label data-icon="p">Your password</label>
                                    <input name="j_password" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                </p>
                                <p class="keeplogin"> 
                                    <input type="checkbox" name="loginkeeping" value="loginkeeping" /> 
                                    <label>RememberMe</label>
                                </p>
                                <p class="login button"> 
                                    <input type="submit" value="Login" /><br>
                                </p>
                                <p>
                                	<a href="http://geekhub.ck.ua">GeekHub</a>
                                	<a href="http://vk.com/geekhub">GeekHub in vk.com</a>
                                </p>
                                <p class="change_link">Not a member yet?
                                	<a href="/iNet/registration.action" class="to_register">Join us</a>
								</p>
                            </form>
                        </div>
                    </div>
                </div>  
            </section>
        </div>
        <div id="footer"><br>
			<%@include file="footerForLoginAndReg.jsp" %>
		</div>
    </body>
</html>