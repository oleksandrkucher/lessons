<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <title>iNet Registration Form</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resourses/favicon.ico"> 
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resourses/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resourses/css/style2.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/myValidateRegister.js"></script>
    </head>
<body onload="disableSubmit()">
	<div class="container">
		<header>
			<h1>Registration</h1>
		</header>
		<section>
			<div id="container_demo">
				<div id="wrapper">
					<div id="login" class="animate form">
						<form action="/iNet/registration.action" autocomplete="on" method="POST">
							<h1>Sign up</h1>
							<p>
								<label data-icon="e">Your email</label> 
								<input name="loginEmail" onchange="verifyEmail(this.value)"required="required" type="email" placeholder="email@domain.com"/>
								<label id="nameStatus" data-iconNO="wait"></label> 
							</p>
							<p>
								<label data-icon="p">Your password</label> 
								<input name="password" onchange="verifyPass(this.value, passwordsignup_confirm.value)" required="required" type="password" placeholder="eg. X8df!90EO" />
								<label id="passStatus" data-iconNO="wait"></label> 
							</p>
							<p>
								<label data-icon="p">Please confirm your password</label> 
								<input name="passwordsignup_confirm" onchange="verifyPass(password.value, this.value)" required="required" type="password" placeholder="eg. X8df!90EO">
								<label id="passConfStatus" data-iconNO="wait"></label> 
							</p>
							<p>
								<label data-icon="u">Your first name</label> 
								<input name="firstName" onchange="verifyFirstName(this.value)" required="required" type="text" placeholder="firstName" />
								<label id="firstName" data-iconNO="wait"></label> 
							</p>
							<p>
								<label data-icon="u">Your last name</label> 
								<input name="lastName" onchange="verifyLastName(this.value)" required="required" type="text" placeholder="lastName" />
								<label id="lastName" data-iconNO="wait"></label> 
							</p>
							<p>
								<label data-icon="u">Your unique link to page</label> 
								<input name="uniqueId" onchange="verifyUniqueId(this.value)" required="required" type="text" placeholder="id00000" />
								<label id="uniqueId" data-iconNO="wait"></label> 
							</p>
							<p class="signin button"><input id="submit" type="submit" value="Sign up"/></p>
								<a href="http://geekhub.ck.ua">GeekHub</a>
						    	<a href="http://vk.com/geekhub">GeekHub in vk.com</a>
								<%@include file="footerForLoginAndReg.jsp" %>
							<p class="change_link">Already a member?<a href="/iNet/login.action" class="to_register">Go and log in</a>
							</p>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>
</html>