<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/userOne.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/addAndDelete.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userOneStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/another.css" type="text/css">
	<title>iNet</title>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp"%>
	</div>
	<div id="content" class="clear_fix " >
		<div class="fl_l" id="left" style="padding: -15px;">
			<c:if test="${not empty fn:replace(user.linkToFoto, ' ', '')}">
				<img alt="myPhoto" src="${pageContext.request.contextPath}/resourses/usersPhotos/${user.linkToFoto}" width=100% align="middle">
			</c:if>
			<c:choose>
				<c:when test="${user.id == sessionScope.loggedUser.id}">
					<p id="hrefs"><a href="/iNet/loadUser.html">Modify info</a></p>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${bookmark == false}">
							<p id="hrefs"><a id="addBookmark" href="javascript:void()" onclick="addBookmark(${user.id})">Add to bookmarks</a></p>
						</c:when>
						<c:otherwise>
							<p id="hrefs"><a id="deleteBookmark" href="javascript:void()" onclick="deleteBookmark(${user.id})">Remove from bookmarks</a></p>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${blacklist == false}">
							<p id="hrefs"><a id="addToBlacklist" href="javascript:void()" onclick="addToBlacklist(${user.id})">Add to blacklist</a></p>
						</c:when>
						<c:otherwise>
							<p id="hrefs"><a id="deleteFromBlacklist" href="javascript:void()" onclick="deleteFromBlacklist(${user.id})">Delete from blacklist</a></p>
						</c:otherwise>
					</c:choose>
					<p id="hrefs"><a href="/iNet/userPersonal/browseFriends.html?id=${user.id}">Browse friends</a></p>
					<p id="hrefs"><a href="/iNet/userPersonal/sendMessage.html?id=${user.id}">Send message</a></p>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${status == 'another'}">
					<p id="hrefs"><a id="addFollowing" href="javascript:void()" onclick="addToFollowing(${user.id})">Add to following</a></p>
				</c:when>
				<c:when test="${status == 'iFollow'}">
					<p id="hrefs"><a id="deleteFollowing" href="javascript:void()" onclick="deleteFollowing(${user.id})">Remove from following</a></p>
				</c:when>
				<c:when test="${status == 'friend'}">
					<p id="hrefs"><a id="deleteFriend" href="javascript:void()" onclick="deleteFriend(${user.id})">Remove from friends</a></p>
				</c:when>
				<c:when test="${status == 'myFollower'}">
					<p id="hrefs"><a id="addFriend" href="javascript:void()" onclick="addFriend(${user.id})">Add to friends</a></p>
				</c:when>
			</c:choose>
			<c:if test="${fn:length(allFriends) > 0}">
				<div><p id="friends">Friends</p></div>
				<div><p id="under">All friends: ${allFriendsCount}</p></div>
				<div id="miniIco">
					<p id="col1">
						<c:forEach var="f" items="${allFriends}">
							<img alt="myPhoto" src="${pageContext.request.contextPath}/resourses/usersPhotos/${f.linkToFoto}">
							<a href="/iNet/userPersonal/${f.uniqueId}.html">${f.firstName} ${f.lastName}</a> 
						</c:forEach>
					</p>
				</div>
			</c:if>
			<c:if test="${fn:length(allOnline) > 0}">
				<div><p id="friends">Friends onLine</p></div>
				<div><p id="under">All friends onLine: ${allOnlineCount}</p></div>
				<div id="miniIco">
					<p id="col1">
						<c:forEach var="f" items="${allOnline}">
							<img alt="myPhoto" src="${pageContext.request.contextPath}/resourses/usersPhotos/${f.linkToFoto}">
							<a href="/iNet/userPersonal/${f.uniqueId}.html">${f.firstName} ${f.lastName}</a> 
						</c:forEach>
					</p>
				</div>
			</c:if>
			<c:if test="${fn:length(allGroups) > 0}">
				<div><p id="friends">Groups</p></div>
				<div><p id="under">All groups: ${allGroupCount}</p></div>
				<div id="miniIco">
					<p id="col1">
						<c:forEach var="f" items="${allGroups}">
							<img alt="myPhoto" src="${pageContext.request.contextPath}/resourses/groupsPhoto/${f.linkToFoto}">
							<a href="/iNet/viewGroup/${f.uniqueId}.html">${f.title}</a> 
						</c:forEach>
					</p>
				</div>
			</c:if>
		</div>
		<div class="fl_r" id="right">
			<div id="profile_info">
				<h4 class="simple page_top clear_fix ta_r">
					<div class="page_name fl_l ta_l">${user.firstName} ${user.nickName} ${user.lastName}</div>
					<div class="profile_grad fl_r">${user.collegeOrUniversityName} ${user.graduationYear}</div>
					<div class="fl_l ta_l clear">
				</h4>
				<h4 class="simple page_top clear_fix ta_r">
					<c:if test="${online == true}">
						<div class="profile_grad fl_r"><h3>online</h3></div>
					</c:if>
					<c:if test="${online == false}">
						<div class="profile_grad fl_r"><h3>offline</h3></div>
					</c:if>
				</h4>
				<div id="profile_short">
					<div class="profile_info">
						<c:if test="${not empty fn:replace(user.birthday, ' ', '') && user.birthdayStatus == 'SHOW'}">
							<div class="clear_fix ">
								<div class="label fl_l">Birthday:</div>
								<div class="labeled fl_l">${user.birthday.date} of ${month[user.birthday.month]} ${user.birthday.year}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.homeCity, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Hometown:</div>
								<div class="labeled fl_l">${user.homeCity}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.homeCountry, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Home country:</div>
								<div class="labeled fl_l">${user.homeCountry}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.sex.displayName, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Sex:</div>
								<div class="labeled fl_l">${user.sex.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.relationshipStatus.displayName, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Relationship status:</div>
								<div class="labeled fl_l">${user.relationshipStatus.displayName}</div>
							</div>
						</c:if>
					</div>
				</div>
				<div id="profile_full_info">
					<h4><b>Contact information</b></h4>
					<div class="profile_info">
						<c:if test="${not empty fn:replace(user.currentCity, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Current city:</div>
								<div class="labeled fl_l">${user.currentCity}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.telephoneNumber, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Telephone number:</div>
								<div class="labeled fl_l">${user.telephoneNumber}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.personalWebSite, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Personal website:</div>
								<div class="labeled fl_l"><a href="http://${user.personalWebSite}">${user.personalWebSite}</a></div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.skype, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Skype:</div>
								<div class="labeled fl_l">${user.skype}</div>
							</div>
						</c:if>
					</div>
					<h4><b>Education</b></h4>
					<div class="profile_info">
						<c:if test="${not empty fn:replace(user.countryOfStudy.displayName, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Country of study:</div>
								<div class="labeled fl_l">${user.countryOfStudy.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.cityOfStudy, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">City of study:</div>
								<div class="labeled fl_l">${user.cityOfStudy}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.collegeOrUniversityName, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">College or university:</div>
								<div class="labeled fl_l">${user.collegeOrUniversityName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.department, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Department:</div>
								<div class="labeled fl_l">${user.department}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.major, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Major:</div>
								<div class="labeled fl_l">${user.major}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.modeOfStudy.displayName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Mode of study:</div>
								<div class="labeled fl_l">${user.modeOfStudy.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.statusOfStudy.displayName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Status:</div>
								<div class="labeled fl_l">${user.statusOfStudy.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.graduationYear, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Graduation year:</div>
								<div class="labeled fl_l">${user.graduationYear}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.schoolName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">School name:</div>
								<div class="labeled fl_l">${user.schoolName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.schoolTown, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">School town:</div>
								<div class="labeled fl_l">${user.schoolTown}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.schoolYears, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">School years:</div>
								<div class="labeled fl_l">${user.schoolYears}</div>
							</div>
						</c:if>
					</div>
					<h4><b>Views</b></h4>
					<div class="profile_info">
						<c:if test="${not empty fn:replace(user.politicalViews.displayName, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Political views:</div>
								<div class="labeled fl_l">${user.politicalViews.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.worldView, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">World view:</div>
								<div class="labeled fl_l">${user.worldView}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.personalPriority.displayName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Personal priority:</div>
								<div class="labeled fl_l">${user.personalPriority.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.importantInOthers.displayName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Important in others:</div>
								<div class="labeled fl_l">${user.importantInOthers.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.viewsOnSmoking.displayName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Views on smoking:</div>
								<div class="labeled fl_l">${user.viewsOnSmoking.displayName}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.viewsOnAlcohol.displayName, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Views on alcohol:</div>
								<div class="labeled fl_l">${user.viewsOnAlcohol.displayName}</div>
							</div>
						</c:if>
					</div>
					<h4><b>Personal information</b></h4>
					<div class="profile_info">
						<c:if test="${not empty fn:replace(user.interests, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Interests:</div>
								<div class="labeled fl_l">${user.interests}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.favoriteMusic, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite music:</div>
								<div class="labeled fl_l">${user.favoriteMusic}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.favoriteFilms, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite movies:</div>
								<div class="labeled fl_l">${user.favoriteFilms}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.favoriteTVShows, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite TV shows:</div>
								<div class="labeled fl_l">${user.favoriteTVShows}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.favoriteBooks, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite books:</div>
								<div class="labeled fl_l">${user.favoriteBooks}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.favoriteQuotes, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite quotes:</div>
								<div class="labeled fl_l">${user.favoriteQuotes}</div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(user.aboutMe, ' ', '')}">
							<div class="clear_fix miniblock">
								<div class="label fl_l">About me:</div>
								<div class="labeled fl_l">${user.aboutMe}</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
			<div class="fl_l" style="width: 100%;">
				<div id="profile_info">
					<div><p id="friends">Wall</p></div>
					<form action="/iNet/userPersonal/addNewPost.html" method="post">
						<textarea rows="5" cols="20" name="newPost"></textarea>
						<input type = "hidden" name="id" value="${user.id }">
						<input type = "submit" 	value = "Add new post">
					</form>
					<div id="profile_full_info">
						<c:set var="val" value="${user.id}"/>
						<c:set var="comm" value="Comment"/>
						<c:forEach var="post" items="${user.wall}">
							<%@include file="../wall.jsp" %>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
		<div id="tmp"></div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>