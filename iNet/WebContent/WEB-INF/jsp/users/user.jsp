<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userStyle.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
<title>iNet</title>
<script>
	function go(month, year){
		if (month == "1" && parseInt(year) % 4 != 0) {
			days = 28;
		} else if(month == "1" && parseInt(year) % 4 == 0){
			days = 29;
		} else if (month == "0" || month == "2"
					|| month == "4" || month == "6"
					|| month == "7" || month == "9" || month == "11") {
				days = 31;
		} else {
			days = 30;
		}
		var dayTag = '';
		for(var i = 1; i <= days; i++){
			dayTag = dayTag + ('<option value="' + i + '">' + i + '</option>');
		}
		var div = '<div id="day">' +
						'<select name="day">' + 
							dayTag +
						'</select>' +	
					'</div>';
		$("#day").empty();
		$("#day").replaceWith(div);
	}
</script>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix ">
		<div id="header"><h2>${user.firstName} ${user.lastName}</h2></div>
		<div id="right">
			<form action="/iNet/saveUser.html" method="post" enctype="multipart/form-data">
				<div id="profile_info">
					<div id="profile_full_info">
						<h4><b>Important part</b></h4>
						<div class="profile_info">
							<div class="clear_fix ">
								<div class="label fl_l">Email login:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.loginEmail}" name="loginEmail">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">First name:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.firstName}" name="firstName">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Last name:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.lastName}" name="lastName">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Unique ID:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.uniqueId}" name="uniqueId">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">NickName:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.nickName}" name="nickName">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Password:</div>
								<div class="labeled fl_l">
									<input type="text" value="" name="password">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Link to photo:</div>
								<div class="labeled fl_l">
									<input type="file" name="file" accept="image/*,image/jpeg">
								<input type="hidden" name="oldLink" value="${user.linkToFoto}">
								</div>
							</div>
						</div>
						<h4><b>Main info</b></h4>
						<div class="profile_info">
							<div class="clear_fix ">
								<div class="label fl_l">Birthday:</div>
								<div class="labeled fl_l">
									<select name="year" onchange="go(month.value, this.value)">
										<c:forEach var="year" begin="1950" end="2005">
											<c:choose>
												<c:when test="${year == user.birthday.year}">
													<option selected value="${year}">${year}</option>
												</c:when>
												<c:otherwise>
													<option value="${year}">${year}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>	
									<select name="month" onchange="go(this.value, year.value)">
										<c:forEach var="i" begin="0" end="11">
											<c:choose>
												<c:when test="${i == user.birthday.month}">
													<option selected value="${i}">${month[i]}</option>
												</c:when>
												<c:otherwise>
													<option value="${i}">${month[i]}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
									<div id="day">
										<select name="day">
											<c:forEach var="day" begin="1" end="31">
												<c:choose>
													<c:when test="${day == user.birthday.date}">
														<option selected value="${day}">${day}</option>
													</c:when>
													<c:otherwise>
														<option value="${day}">${day}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>	
									</div>
									<div>
										<select name="status">
											<c:forEach var="v" items="${birthdayStatus}">
												<c:choose>
													<c:when test="${v.value == user.birthdayStatus}">
														<option selected value="${v.key}">${v.value.displayName}</option>
													</c:when>
													<c:otherwise>
														<option value="${v.key}">${v.value.displayName}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>	
									</div>
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Hometown:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.homeCity}" name="homeCity">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Home country:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.homeCountry}" name="homeCountry">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Sex:</div>
								<div class="labeled fl_l">
									<select name="sex">
										<c:forEach items="${sex}" var="views">
											<c:choose>
												<c:when test="${views.value == user.sex}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Relationship status:</div>
								<div class="labeled fl_l">
									<select name="relationshipStatus">
										<c:forEach items="${relationshipStatus}" var="views">
											<c:choose>
												<c:when test="${views.value == user.relationshipStatus}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>	
								</div>
							</div>
						</div>
						<h4><b>Contact information</b></h4>
						<div class="profile_info">
							<div class="clear_fix ">
								<div class="label fl_l">Current city:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.currentCity}" name="currentCity">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Telephone number:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.telephoneNumber}" name="telephoneNumber">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Personal website:</div>
								<div class="labeled fl_l">
									http:// <input type="text" value="${user.personalWebSite}" name="personalWebSite" style="width: 85%;">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Skype:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.skype}" name="skype">
								</div>
							</div>
						</div>
						<h4><b>Education</b></h4>
						<div class="profile_info">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Country of study:</div>
								<div class="labeled fl_l">
									<select name="countryOfStudy">
										<c:forEach items="${countryOfStudy}" var="views">
											<c:choose>
												<c:when test="${views.value == user.countryOfStudy}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">City of study:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.cityOfStudy}" name="cityOfStudy">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">College or university:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.collegeOrUniversityName}" name="collegeOrUniversityName">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Department:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.department}" name="department">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Major:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.major}" name="major">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Mode of study:</div>
								<div class="labeled fl_l">
									<select name="modeOfStudy">
										<c:forEach items="${modeOfStudy}" var="views">
											<c:choose>
												<c:when test="${views.value == user.modeOfStudy}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Status:</div>
								<div class="labeled fl_l">
									<select name="statusOfStudy">
										<c:forEach items="${statusOfStudy}" var="views">
											<c:choose>
												<c:when test="${views.value == user.statusOfStudy}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Graduation year:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.graduationYear}" name="graduationYear">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">School name:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.schoolName}" name="schoolName">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">School town:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.schoolTown}" name="schoolTown">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">School years:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.schoolYears}" name="schoolYears">
								</div>
							</div>
						</div>
						<h4><b>Views</b></h4>
						<div class="profile_info">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Political views:</div>
								<div class="labeled fl_l">
									<select name="politicalViews">
										<c:forEach items="${politicalViews}" var="views">
											<c:choose>
												<c:when test="${views.value == user.politicalViews}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">World view:</div>
								<div class="labeled fl_l">
									<input type="text" value="${user.worldView}" name="worldView">
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Personal priority:</div>
								<div class="labeled fl_l">
									<select name="personalPriority">
										<c:forEach items="${personalPriority}" var="views">
											<c:choose>
												<c:when test="${views.value == user.personalPriority}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Important in others:</div>
								<div class="labeled fl_l">
									<select name="importantInOthers">
										<c:forEach items="${importantInOthers}" var="views">
											<c:choose>
												<c:when test="${views.value == user.importantInOthers}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Views on smoking:</div>
								<div class="labeled fl_l">
									<select name="viewsOnSmoking">
										<c:forEach items="${viewsOnSmoking}" var="views">
											<c:choose>
												<c:when test="${views.value == user.viewsOnSmoking}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Views on alcohol:</div>
								<div class="labeled fl_l">
									<select name="viewsOnAlcohol">
										<c:forEach items="${viewsOnAlcohol}" var="views">
											<c:choose>
												<c:when test="${views.value == user.viewsOnAlcohol}">
													<option selected value="${views.key}">${views.value.displayName}</option>
												</c:when>
												<c:otherwise>
													<option value="${views.key}">${views.value.displayName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<h4><b>Personal information</b></h4>
						<div class="profile_info">
							<div class="clear_fix miniblock">
								<div class="label fl_l">Interests:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="interests">${user.interests}</textarea>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite music:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="favoriteMusic">${user.favoriteMusic}</textarea>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite movies:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="favoriteFilms">${user.favoriteFilms}</textarea>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite TV shows:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="favoriteTVShows">${user.favoriteTVShows}</textarea>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite books:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="favoriteBooks">${user.favoriteBooks}</textarea>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">Favorite quotes:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="favoriteQuotes">${user.favoriteQuotes}</textarea>
								</div>
							</div>
							<div class="clear_fix miniblock">
								<div class="label fl_l">About me:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="aboutMe">${user.aboutMe}</textarea>
								</div>
							</div>
						</div>
					</div>
					</div>
			<input type = "submit" 	value = "Save">
			<input type="hidden" name="id" value="${user.id}"><br>
			</form>
		</div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>