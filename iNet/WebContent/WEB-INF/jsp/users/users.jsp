<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iNet</title>
<style>
a:link {
	color: blue
}

a:visited {
	color: blue
}

a:hover {
	color: red
}

a:active {
	color: red;
}
body {
	background-image: url("${pageContext.request.contextPath}/resourses/usersPhotos/bg.jpg");
	background-repeat: no-repeat;
	background-attachment: fixed;
}
</style>
</head>
<body>
	<div align = "center">
		<h1>ListUsers</h1>
		<table border="1" width="700">
			<tr>
				<th>id</th>
				<th>loginEmail</th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>${user.id}</td>
					<td>${user.loginEmail}</td>
					<td><a href="/iNet/loadUser.html?id=${user.id}">Edit</a></td>
					<td><a href="/iNet/userPersonal/${user.uniqueId}.html">View</a></td>
					<td style="color: red;"><a href="/iNet/deleteUser.html?id=${user.id}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<br> <a href="/iNet/loadUser.html">Create New User</a>
		<br>
		<br> <a href="/iNet/index.html">Main Page</a>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>