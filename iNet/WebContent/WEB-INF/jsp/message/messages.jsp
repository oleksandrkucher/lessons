<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userOneStyle.css" type="text/css">
<title>iNet</title>
<style type="text/css">
img {
	width: 80%;
	max-height: 80%;
}
#content{
	min-height: 300px;
}
h1 {
	text-align: center;
}
</style>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix " >
		<div class="fl_l">
			<div id="profile_info">
				<h1><c:out value="Messages of ${user.firstName} ${user.lastName}"/></h1>
				<div id="profile_full_info">
					<c:forEach items="${messages}" var="m">
						<h4><b></b></h4>
						<div class="profile_info">
							<div class="clear_fix ">
								<div class="label fl_l"><a href="/iNet/userPersonal/${f.sender.uniqueId}.html"><img src="${pageContext.request.contextPath}/resourses/usersPhotos/${m.sender.linkToFoto}"></a></div>
								<div class="labeled fl_l">
									<h3><a href="/iNet/userPersonal/${m.sender.uniqueId}.html">${m.sender.firstName} ${m.sender.lastName}</a><h3>
								</div>
								<div class="labeled fl_l">
									${m.body}
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>