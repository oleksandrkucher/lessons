<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userOneStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/messageStyle.css" type="text/css">
	<title>iNet</title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/addAndDelete.js"></script>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix " style="min-height: 300px;">
		<h1>Sent messages</h1>
		<h4><b>You have ${fn:length(messages)} messages</b></h4>
		<div class="fl_l">
			<div id="profile_info">
				<div id="profile_full_info">
					<c:choose>
						<c:when test="${fn:length(messages) != 0}">
							<c:forEach items="${messages}" var="f">
								<c:set var="background" value=''/>
								<c:if test="${f.newMessage == true}">
									<c:set var="background" value='style="background-color: #CAC6FF; width: 115%;"'/>
								</c:if>
								<c:if test="${f.newMessage == false}">
									<c:set var="background" value='style="width: 115%;"'/>
								</c:if>
								<div id="${f.id}" class="profile_info" ${background}>
									<div class="clear_fix ">
										<div class="l fl_l">
											<a href="/iNet/userPersonal/${f.recipient.uniqueId}.html"><img src="${pageContext.request.contextPath}/resourses/usersPhotos/${f.recipient.linkToFoto}"></a>
										</div>
										<div class="la fl_l">
											<a href="/iNet/userPersonal/${f.recipient.uniqueId}.html">${f.recipient.firstName} ${f.recipient.lastName}</a>
											<h5>${f.date}<h5>
										</div>
										<div class="la fl_l">
											<a href="/iNet/userPersonal/readSentAndSend.html?id=${f.id}">${fn:substring(f.body, 0, 50)}...</a>
										</div>
										<div class="l fl_r">
											<a href="javascript:void()" onclick="deleteMessage(${f.id})">Delete</a>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<h3><c:out value="You have any sended messages"></c:out></h3>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>