<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userOneStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/messageStyle.css" type="text/css">
	<title>iNet</title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/addAndDelete.js"></script>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix " >
		<div class="fl_l">
			<div id="profile_info">
				<div id="profile_full_info">
				<form action="/iNet/userPersonal/saveMessage.html" method="post">
				<div class="profile_info">
					<div class="clear_fix ">
						<div class="l fl_l">
							<a href="/iNet/userPersonal/${recepient.uniqueId}.html"><img src="${pageContext.request.contextPath}/resourses/usersPhotos/${recepient.linkToFoto}"></a>
						</div>
						<div class="la fl_l">
							<a href="/iNet/userPersonal/${recepient.uniqueId}.html">${recepient.firstName} ${recepient.lastName}</a>
							<hr>
						</div>
					</div>
				</div>
				<div class="clear_fix miniblock">
					<div>
						<textarea rows="10" cols="90" name="body"></textarea>
					</div>
				</div>
				<div class="fl_r">
					<input type = "submit" 	value = "Send">
				</div>
				<input type="hidden" name="id" value="${recepient.id}"><br>
			</form>
			</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>