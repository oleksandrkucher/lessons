<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/errorStyle.css" type="text/css">
<title>iNet 500</title>
</head>
<body>
	<div id="sidebar">
		<hr>
		<p><a href="/iNet/userPersonal/${sessionScope.loggedUser.uniqueId}.html"><h3>Profile</h3></a></p>
		<hr>
		<p><a href="/iNet/index.html"><h3>Main page</h3></a></p>
		<hr>
	</div>
	<div id="container">
		<h1>AAAAAAAA! Error 500</h1>
		<div id="content" class="clear_fix ">
			<img alt="error 500" src="${pageContext.request.contextPath}/resourses/images/404.jpg">
		</div>
	</div>
</body>
</html>