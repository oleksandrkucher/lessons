<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <title>iNet Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resourses/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resourses/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resourses/css/style.css" />
        <script>
			function howMany(){
				$.ajax({
					url: '/iNet/howMany.action',
					type: 'POST',
					dataType: "text",
					success: function(response){ 
						var element = $("#reg");
						var newElem = '<a href="javascript:void()" id="reg" onmouseover="howMany()">Now registered ' + response + ' users</a>';
						element.replaceWith(newElem);
					},
				});
			}
		</script>
    </head>
    <body>
        <div class="container">
            <section>               
                <div id="container_demo" >
                    <div id="wrapper">
                        <div id="login" class="animate form"><!--j_spring_security_check-->
                        	<h1>Hello this is iNet!!!</h1>
							<c:if test="${users != 0}">
								<div><a href="javascript:void()" id="reg" onmouseover="howMany()"><h1>Now registered ${users} users</h1></a></div>
							</c:if>
							<h2 style="text-align: center; font-size: 20px; font-style: oblique;">Please <a href="/iNet/login.action">Go and login</a> or <a href="/iNet/registration.action">Join us</a></h2>
                        </div>
                    </div>
                </div>  
            </section>
        </div>
        <div id="footer"><br>
			<%@include file="loginAndReg/footerForLoginAndReg.jsp" %>
		</div>
    </body>
</html>