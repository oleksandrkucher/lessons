<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userOneStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/listPagesStyle.css" type="text/css">
	<title>iNet</title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/addAndDelete.js"></script>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix " >
		<h1>All followings</h1>
		<h4><b>You have ${fn:length(allFollowings)} followings</b></h4>
		<div class="fl_l">
			<div id="profile_info">
				<div id="profile_full_info">
					<c:choose>
						<c:when test="${fn:length(allFollowings) != 0}">
							<c:forEach items="${allFollowings}" var="f">
								<div id="${f.id}" class="profile_info">
									<div class="clear_fix ">
										<div class="label fl_l"><a href="/iNet/userPersonal/${f.uniqueId}.html"><img src="${pageContext.request.contextPath}/resourses/usersPhotos/${f.linkToFoto}"></a></div>
										<div class="labeled fl_l">
											<h3><a href="/iNet/userPersonal/${f.uniqueId}.html">${f.firstName} ${f.lastName}</a><h3>
											<h5>${f.collegeOrUniversityName} ${f.graduationYear}</h5>
										</div>
										<div class="labeled fl_l">
											<%@include file="../miniMenuFriends.jsp" %>
											<h5><a href="javascript:void()" onclick="deleteFollow(${f.id})">Remove from followings</a></h5>
										</div>
									</div>
								</div><hr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<h3><c:out value="You have any followings"></c:out></h3>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>