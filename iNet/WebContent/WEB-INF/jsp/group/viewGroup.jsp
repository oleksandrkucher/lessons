<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/userOne.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/addAndDelete.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userOneStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/another.css" type="text/css">
	<title>iNet</title>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp"%>
	</div>
	<div id="content" class="clear_fix " >
		<div class="fl_l" id="left" style="padding: -15px;">
			<c:if test="${not empty fn:replace(group.linkToFoto, ' ', '')}">
				<img alt="myPhoto" src="${pageContext.request.contextPath}/resourses/groupsPhoto/${group.linkToFoto}" width=100% align="middle">
			</c:if>
			<c:if test="${member == true}">
				<div><h3><a id="leave" href="javascript:void()" onclick="leaveGroup(${group.id})">Leave group</a></h3></div>
			</c:if>
			<c:if test="${member == false}">
				<div><h3><a id="become" href="javascript:void()" onclick="becomeMember(${group.id})">Became a member of group</a></h3></div>
			</c:if>
			<c:if test="${chief == true}">
				<div><h3><a href="/iNet/deleteGroup.html?id=${group.id}">Delete group</a></h3></div>
				<div><h3><a href="/iNet/loadGroup.html?id=${group.id}">Change group info</a></h3></div>
			</c:if>
			<c:if test="${fn:length(users6) > 0}">
				<div><p id="friends">Users</p></div>
				<div><p id="under">All users: ${allUsers}</p></div>
				<div id="miniIco">
					<p id="col1">
						<c:forEach var="f" items="${users6}">
							<img alt="myPhoto" src="${pageContext.request.contextPath}/resourses/usersPhotos/${f.linkToFoto}">
							<a href="/iNet/userPersonal/${f.uniqueId}.html">${f.firstName} ${f.lastName}</a> 
						</c:forEach>
					</p>
				</div>
			</c:if>
		</div>
		<div class="fl_r" id="right">
			<div id="profile_info">
				<h4 class="simple page_top clear_fix ta_r">
					<div class="page_name fl_l ta_l">${group.title}</div>
					<div class="fl_l ta_l clear">
				</h4>
				<div id="profile_short">
					<div class="profile_info">
						<c:if test="${not empty fn:replace(group.info, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Chief of group:</div>
								<div class="labeled fl_l"><a href="/iNet/userPersonal/${group.chiefOfGroup.uniqueId}.html">${group.chiefOfGroup.firstName} ${group.chiefOfGroup.lastName}</a></div>
							</div>
						</c:if>
						<c:if test="${not empty fn:replace(group.info, ' ', '')}">
							<div class="clear_fix ">
								<div class="label fl_l">Info:</div>
								<div class="labeled fl_l">${group.info}</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
			<div class="fl_l" style="width: 100%;">
				<div id="profile_info">
					<div><p id="friends">Wall</p></div>
					<form action="/iNet/addNewPostToGroup.html" method="post">
						<textarea rows="5" cols="20" name="newPost"></textarea>
						<input type = "hidden" name="id" value="${group.id }">
						<input type = "submit" 	value = "Add new post">
					</form>
					<div id="profile_full_info">
						<c:set var="val" value="${group.id}"/>
						<c:set var="comm" value="CommentToGroup"/>
						<c:forEach var="post" items="${group.wall}">
							<%@include file="../wall.jsp" %>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
		<div id="tmp"></div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>