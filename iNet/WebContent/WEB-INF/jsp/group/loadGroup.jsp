<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userStyle.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
<title>iNet</title>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix " style="min-height: 300px;">
		<div id="header"><h2>${group.title}</h2></div>
		<div id="right">
			<form action="/iNet/saveGroup.html" method="post" enctype="multipart/form-data">
				<div id="profile_info">
					<div id="profile_full_info">
						<div class="profile_info">
							<div class="clear_fix ">
								<div class="label fl_l">Title:</div>
								<div class="labeled fl_l">
									<input type="text" value="${group.title}" onchange="verifyTitle(this.value)"name="title">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Info:</div>
								<div class="labeled fl_l">
									<textarea rows="10" cols="20" name="info">${group.info}</textarea>
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Unique ID:</div>
								<div class="labeled fl_l">
									<input type="text" onchange="verifyUniqueId(this.value)" value="${group.uniqueId}" name="uniqueId" id="uniqueId">
								</div>
							</div>
							<div class="clear_fix ">
								<div class="label fl_l">Link to photo:</div>
								<div class="labeled fl_l">
									<input type="file" name="file" accept="image/*,image/jpeg">
								<input type="hidden" name="oldLink" value="${group.linkToFoto}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<input type = "submit" id="submit" value = "Save">
				<input type="hidden" name="id" value="${group.id}"><br>
			</form>
		</div>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>