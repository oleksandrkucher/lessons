<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bodyStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/profileStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/userStyle.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/listPagesStyle.css" type="text/css">
	<title>iNet</title>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/search.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resourses/scripts/jquery-latest.js"></script>
</head>
<body onload="howManyNew()">
	<div id="sidebar">
		<%@include file="../menu.jsp" %>
	</div>
	<div id="content" class="clear_fix" >
		<h1>Search</h1>
		<h4><b>Input first name, last name or first and last names together</b></h4><hr>
		<form action="/iNet/userPersonal/search.html" method="post">
			<input type="text" style="width: 80%;" name="searchValue"  onchange="search(this.value)"required="required" placeholder="First or last name with ajax"/>
			<input type = "submit" 	value = "Search"><br><hr><br>
			<div id="s">
				<h3><c:out value="Input first or last name of user for search."></c:out></h3>
			</div>
		</form>
	</div>
	<div id="footer">
		<%@include file="../footer.jsp" %>
	</div>
</body>
</html>