<div id="${post.id}" class="profile_info">
	<div class="clear_fix ">
		<div class="l fl_l">
			<a href="/iNet/userPersonal/${post.owner.uniqueId}.html"><img src="${pageContext.request.contextPath}/resourses/usersPhotos/${post.owner.linkToFoto}"></a>
		</div>
		<div class="la fl_l">
			<a href="/iNet/userPersonal/${post.owner.uniqueId}.html">${post.owner.firstName} ${post.owner.lastName}</a>
			<br>${post.dateAndTime}<hr>
			${post.body}
		</div>
		<div class="l fl_r" style="width: 300px; text-align: right;">
			<button id="butCom" onclick="input${comm}(${post.id}, ${val})">Comment</button>
			<button onclick="deletePost(${post.id})">Delete</button>
			<button id="like${post.id}" onclick="changeLikePost(${post.id})">Like: ${post.like}</button>
			<button id="dislike${post.id}" onclick="changeDisLikePost(${post.id})">Dislike: ${post.dislike}</button>
		</div>
		<div class="la fl_r">
		<br>
			<c:forEach var="comment" items="${post.comments}">
				<div id="comm${comment.id}">
					<div class="l fl_l" style="width: 25%;">
						<a href="/iNet/userPersonal/${comment.owner.uniqueId}.html"><img src="${pageContext.request.contextPath}/resourses/usersPhotos/${comment.owner.linkToFoto}"></a>
					</div>
					<div class="la fl_l" style="width: 75%;">
						<a href="/iNet/userPersonal/${comment.owner.uniqueId}.html">${comment.owner.firstName} ${comment.owner.lastName}</a>
						<br>${comment.date}<hr>
						${comment.body}
					</div>
					<div class="l fl_r" style="width: 300px; text-align: right;">
						<button onclick="deleteComment(${comment.id})">Delete</button>
						<button id="likeComm${comment.id}" onclick="changeLikeComm(${comment.id})">Like: ${comment.like}</button>
						<button id="dislikeComm${comment.id}" onclick="changeDisLikeComm(${comment.id})">Dislike: ${comment.dislike}</button>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>