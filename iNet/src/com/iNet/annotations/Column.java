package com.iNet.annotations;

import java.lang.annotation.*;

import com.iNet.beans.enums.Type;

/**
 * Annotation for using to fields which you want to save in table in database
 * name - name of one column, must be completely identical to the field name in the database; mandatory, no default value
 * type - name of data type, for example: Integer, Double, String, Enum, Entity etc; default value String
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

	String name();
	Type type() default Type.STRING;
	int sizeType() default 0;
}