package com.iNet.annotations;

import java.lang.annotation.*;

import com.iNet.beans.enums.TableType;

/**
 * Annotation for using to class which you want to save as table in database
 * name - name of the table, must be completely identical to the table name in the database; mandatory, no default value
 * haveEntity - boolean value means has or not this class field of type other classes that extends class Entity; default value false
 * tableType - enum value
 * haveDate - boolean value means has or not this class field of type Date
 * 
 * In class with tableType=TableType.JOIN_TABLE_WITH_PARAMETR must be 3 fields:
 * first field must be with annotation JoinColumn
 * second field must be with annotation Column and type=Type.INTEGER or another number type
 * third field must be with annotation Column, type may be different
 * 
 * @author kol
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {

	String name();
	boolean haveEntity() default false;
	TableType tableType() default TableType.TABLE;
	boolean haveDate() default false;
}