package com.iNet.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.iNet.beans.enums.NumberTypeOnly;

/**
 * Annotation for using to fields in join table for 
 * establishing communication between tables
 * 
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JoinColumn {

	String name();
	String joinToTable();
	NumberTypeOnly type() default NumberTypeOnly.INTEGER; //INTEGER, LONG, SHORT, BYTE
	String classNameForTable();
}