package com.iNet.annotations;

import java.lang.annotation.*;

import com.iNet.beans.enums.NumberTypeOnly;

/**
 * Annotation for many to many communication between tables
 *  
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ManyToMany {

	String joinTableName();
	String joinColumnName();
	String thisColumnName();
	NumberTypeOnly thisColumnType() default NumberTypeOnly.INTEGER; //Integer, Long, Short, Byte
	String classNameForTable();
}