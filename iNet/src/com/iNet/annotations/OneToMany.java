package com.iNet.annotations;

import java.lang.annotation.*;

/**
 * Annotation for communication one to many between tables
 * use in first table where save field with list with objects extends Entity
 * 
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OneToMany {

	String tableName();
	String columnName();
	String classNameForTable();
}