package com.iNet.annotations;

import java.lang.annotation.*;

/**
 * Annotation for using to fields which you want use as id of table
 * 
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Id {}