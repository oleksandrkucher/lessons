package com.iNet.annotations;

import java.lang.annotation.*;

/**
 * Annotation for using with annotation @Id
 * 
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface GeneratedValue {}