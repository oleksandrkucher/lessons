package com.iNet.annotations;

import java.lang.annotation.*;

/**
 * Annotation for communication one to many between tables
 * use in second table where save field with type extends Entity
 * 
 * @author kol
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ManyToOne {

	String thisColumnName();
	String joinTableName();
}