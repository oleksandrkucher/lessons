package com.iNet.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.iNet.beans.User;

public class AdditionMethod {

	private static String pathForGroup = "/home/kol/Programs/git/GeekHub/iNet/WebContent/resourses/groupsPhoto/";
	private static String pathForUser = "/home/kol/Programs/git/GeekHub/iNet/WebContent/resourses/usersPhotos/";
	
	/**
	 * Gets list of online friends of user which page you view now
	 * @param request
	 * @param user - this user
	 * @return
	 */
	public static List<User> whoIsOnline(HttpServletRequest request, User user){
		List<User> onlineFriends = new ArrayList<>();
		ServletContext context = request.getServletContext();
		for(User u: user.getMyFriends()){
			if(context.getAttribute(u.getId().toString()) != null){
				onlineFriends.add(u);
			}
		}
		return onlineFriends;
	}
	
	/**
	 * Check email by pattern
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		final String emailPatern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern p = Pattern.compile(emailPatern);
		Matcher m = p.matcher(email.toLowerCase());
		return m.matches();
	}

	/**
	 * Gets 6 users (my friends) with using random and create list with them
	 * @param user
	 * @return
	 */
	public static List<User> prepareFriends(List<User> all){
		List<User> friendsOnPage = new ArrayList<>();
		int count = 0;
		if (all.size() < 6) {
			count = all.size();
		} else {
			count = 6;
		}
		for (int i = 0; i < count; i++) {
			friendsOnPage.add(all.get(i));
		}
		return friendsOnPage;
	}

	/**
	 * @return the pathForGroup
	 */
	public static String getPathForGroup() {
		return pathForGroup;
	}

	/**
	 * @param pathForGroup the pathForGroup to set
	 */
	public static void setPathForGroup(String pathForGroup) {
		AdditionMethod.pathForGroup = pathForGroup;
	}

	/**
	 * @return the pathForUser
	 */
	public static String getPathForUser() {
		return pathForUser;
	}

	/**
	 * @param pathForUser the pathForUser to set
	 */
	public static void setPathForUser(String pathForUser) {
		AdditionMethod.pathForUser = pathForUser;
	}
}