package com.iNet.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iNet.beans.User;
import com.iNet.services.WorkBaseDAO;

@Controller
public class SearchController {

	private @Autowired WorkBaseDAO wbDAO;

	/**
	 * Search all users which in blacklist and which not in blacklist 
	 * @param map
	 * @param search - first and second name
	 * @return
	 */
	@RequestMapping(value="userPersonal/search.html")
	public String searchAllPeople(ModelMap map, @RequestParam(value="searchValue", required=false) String search){
		if(search != null){
			map.put("users", search(search));
		}
		return "search/search";
	}
	
	/**
	 * Search only user which not in blacklist
	 * @param search - first and second name
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/searchAJAX.html")
	@ResponseBody
	public String searchSomePeopleAJAX(@RequestParam(value="searchValue", required=false) String search, 
			HttpServletRequest request){
		User loggedUser = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(loggedUser, User.class, loggedUser.getId());
		return makeAnswer(search(search), loggedUser);
	}
	
	/**
	 * Make json object for out in page with ajax and jQuery
	 * @param users which was found
	 * @param loggedUser
	 * @return
	 */
	private String makeAnswer(Set<User> users, User loggedUser){
		List<User> black = new ArrayList<>();
		black.addAll(loggedUser.getiInTheyBlackList());
		black.addAll(loggedUser.getInMyBlackList());
        List<UserJson> find = new ArrayList<>();
        for(User u: users){
        	if(black.indexOf(u) == -1){
        		find.add(new UserJson(u.getId(), u.getUniqueId(), u.getFirstName(), 
        				u.getLastName(), u.getLinkToFoto(), u.getCollegeOrUniversityName(), u.getGraduationYear()));
        	}
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(find);
	}
	
	/**
	 * Search users by first and second name
	 * @param search - first and second name
	 * @return
	 */
	private Set<User> search(String search){
		Set<User> users = new HashSet<>();
		if(search != null){
			search = search.trim();
			String words[] = search.split(" ", 2);
			for(int i = 0; i < words.length; i++){
				String sqlFirstName = "SELECT * FROM `User` WHERE `firstName` LIKE '%" + words[i] + "%'";
				String sqlSecondName = "SELECT * FROM `User` WHERE `lastName` LIKE '%" + words[i] + "%'";
				users.addAll(wbDAO.makeList(sqlFirstName, User.class));
				users.addAll(wbDAO.makeList(sqlSecondName, User.class));
			}
			if(words.length > 1){
				String sqlFirstAndSecondName = "SELECT * FROM `User` WHERE `firstName` LIKE '%" + words[0] + "%' AND `lastName` LIKE '%" + words[1] + "%'";
				String sqlSecondAndFirstName = "SELECT * FROM `User` WHERE `firstName` LIKE '%" + words[1] + "%' AND `lastName` LIKE '%" + words[0] + "%'";
				users.addAll(wbDAO.makeList(sqlFirstAndSecondName, User.class));
				users.addAll(wbDAO.makeList(sqlSecondAndFirstName, User.class));
			}
		}
		return users;
	}

	/**
	 * Class for using in making json answer
	 * This is simple user
	 * 
	 * @author kol
	 */
	public class UserJson {
		private Integer id;
		private String uniqueId;
		private String firstName;
		private String lastName;
		private String linkToFoto;
		private String collegeOrUniversityName;
		private String graduationYear;
		
		public UserJson(Integer id, String uniqueId, String firstName,
				String lastName, String linkToFoto,
				String collegeOrUniversityName, String graduationYear) {
			this.id = id;
			this.uniqueId = uniqueId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.linkToFoto = linkToFoto;
			this.collegeOrUniversityName = collegeOrUniversityName;
			this.graduationYear = graduationYear;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getUniqueId() {
			return uniqueId;
		}
		public void setUniqueId(String uniqueId) {
			this.uniqueId = uniqueId;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getLinkToFoto() {
			return linkToFoto;
		}
		public void setLinkToFoto(String linkToFoto) {
			this.linkToFoto = linkToFoto;
		}
		public String getCollegeOrUniversityName() {
			return collegeOrUniversityName;
		}
		public void setCollegeOrUniversityName(String collegeOrUniversityName) {
			this.collegeOrUniversityName = collegeOrUniversityName;
		}
		public String getGraduationYear() {
			return graduationYear;
		}
		public void setGraduationYear(String graduationYear) {
			this.graduationYear = graduationYear;
		}
	}
}