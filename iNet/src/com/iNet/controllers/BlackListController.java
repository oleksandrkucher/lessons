package com.iNet.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.User;
import com.iNet.beans.joinTables.BlackList;
import com.iNet.beans.joinTables.BookMarksUsers;
import com.iNet.beans.joinTables.Follow;
import com.iNet.beans.joinTables.Friends;
import com.iNet.services.WorkBaseDAO;
import com.iNet.services.JoinTableDAO;

@Controller
public class BlackListController {

	private @Autowired WorkBaseDAO wbDAO;
	private @Autowired JoinTableDAO jtDAO;

	/**
	 * Add user to my blacklist and remove it from my friends followers and following
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="userPersonal/addToMyBlacklist.html")
	@ResponseBody
	public String addToMyBlacklist (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getInMyBlackList().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) < 0){
				jtDAO.addToJoinTable(BlackList.class, id, user.getId());
				if(user.getMyFriends().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
					jtDAO.deleteFromJoinTable(Friends.class, user.getId(), id);
					jtDAO.deleteFromJoinTable(Friends.class, id, user.getId());
				} else {
					jtDAO.deleteFromJoinTable(Follow.class, user.getId(), id);
					jtDAO.deleteFromJoinTable(Follow.class, id, user.getId());
				}
				if(user.getBookMarksUsers().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
					jtDAO.deleteFromJoinTable(BookMarksUsers.class, user.getId(), id);
					jtDAO.deleteFromJoinTable(BookMarksUsers.class, id, user.getId());
				}
				return "true";
			}
		}
		return "false";
	}
	
	/**
	 * Remove user from my blacklist
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="userPersonal/deleteFromMyBlacklist.html")
	@ResponseBody
	public String deleteFromMyBlacklist (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getInMyBlackList().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
				jtDAO.deleteFromJoinTable(BlackList.class, id, user.getId());
			}
		}
		return "true";
	}
	
	/**
	 * Update all values of logged user, gets all users in blacklist and show page iInBlacklist.html
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/iInBlacklist.html")
	public String iInBlackList(ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allUsers", user.getiInTheyBlackList());
		return "blacklist/iInBlacklist";
	}

	/**
	 * Update all values of logged user, gets all users which add me in blacklist and show page inMyBlacklist.html
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/inMyBlacklist.html")
	public String inMyBlackList(ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allUsers", user.getInMyBlackList());
		return "blacklist/inMyBlacklist";
	}
}