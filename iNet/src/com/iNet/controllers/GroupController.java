package com.iNet.controllers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.iNet.beans.Comment;
import com.iNet.beans.Group;
import com.iNet.beans.Post;
import com.iNet.beans.User;
import com.iNet.beans.joinTables.UsersAndGroups;
import com.iNet.interfaces.DynamicValues;
import com.iNet.services.DAO;
import com.iNet.services.JoinTableDAO;
import com.iNet.services.WorkBaseDAO;

@Controller
public class GroupController implements DynamicValues {

	private @Autowired DAO dao;
	private @Autowired WorkBaseDAO wbDAO;
	private @Autowired JoinTableDAO jtDAO;

	@RequestMapping(value="loadGroup.html")
	public String load (ModelMap map, HttpServletRequest request,
			@RequestParam(value="id", required=false) Integer id){
		if (id != null) {
			Group group = wbDAO.getReflect(Group.class, "id",
					Integer.toString(id));
			if (group != null) {
				map.put("group", group);
			}
		}
		return "group/loadGroup";
	}
	
	@RequestMapping(value="group/leaveGroup.html")
	@ResponseBody
	public String leaveGroup(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		jtDAO.deleteFromJoinTable(UsersAndGroups.class, user.getId(), id);
		return "ok";
	}
	
	@RequestMapping(value="group/becomeMember.html")
	@ResponseBody
	public String becomeMemberOfGroup(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		jtDAO.addToJoinTable(UsersAndGroups.class, user.getId(), id);
		return "ok";
	}
	
	@RequestMapping(value="group/verifyGroupUniqueId.html")
	@ResponseBody
	public String verifyGroupUniqueId(HttpServletRequest request,
			@RequestParam(value="val", required=true) String val){
		if(val != null && !"".equals(val)){
			if(wbDAO.getReflect(Group.class, "uniqueId", "\"" + val + "\"") != null){
				return "false";
			} else {
				return "true";
			}
		}
		return "true";
	}
	
	@RequestMapping(value="saveGroup.html")
	public String saveGroup(Group group, HttpServletRequest request,
			@RequestParam(value="file", required=false) MultipartFile file,
			@RequestParam(value="oldLink", required=false) String oldLink) throws IOException{
		User user = (User) request.getSession().getAttribute("loggedUser");
		group.setChiefOfGroup(user);
		if (file != null && file.getSize() <= IMAGE_MAX_SIZE && !"".equals(file.getOriginalFilename())) {
			InputStream in = file.getInputStream();
			String fileName = file.getOriginalFilename().
					substring(file.getOriginalFilename().lastIndexOf('.'));
			FileOutputStream out = new FileOutputStream(AdditionMethod.getPathForGroup()
					+ group.getUniqueId() + fileName);
			int tmp;
			try {
				while ((tmp = in.read()) != -1) {
					out.write(tmp);
				}
				in.close();
				out.close();
			} catch (IOException e) {}
			group.setLinkToFoto(group.getUniqueId() + fileName);
		} else {
			if(oldLink == null || "".equals(oldLink)){
				oldLink = "defaultFoto.jpg";
			}
			group.setLinkToFoto(oldLink);
		}
		group.setInfo(group.getInfo().replace("\n", "<br>"));
		wbDAO.saveReflect(group);
		return "redirect:viewGroup/" + group.getUniqueId() + ".html";
	}
	
	@RequestMapping(value="deleteGroup.html")
	public String deleteGroup(@RequestParam(value="id", required=true) Integer id, HttpServletRequest request){
		User loggedUser = (User)request.getSession().getAttribute("loggedUser");
		Group group = wbDAO.getReflect(Group.class, "id", Integer.toString(id));
		if(group != null && loggedUser.getId() == group.getChiefOfGroup().getId()){
			for(Post f: group.getWall()){
				for(Comment c: f.getComments()){
					wbDAO.update("DELETE FROM `Comments` WHERE `postId`=?", c.getId());
				}
			}
			wbDAO.update("DELETE FROM `Post` WHERE `groupId`=?", id);
			wbDAO.update("DELETE FROM `UsersAndGroups` WHERE `groupId`=?", id);
			wbDAO.delete(Group.class, id);
		} else {
			return "redirect:error/youCant";
		}
		return "redirect:userPersonal/" + loggedUser.getUniqueId() + ".html";
	}
	
	@RequestMapping(value="viewGroup/{uniqueId}.html")
	public String getGroup(@PathVariable String uniqueId, ModelMap map, HttpServletRequest request){
		User loggedUser = (User)request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(loggedUser, User.class, loggedUser.getId());
		wbDAO.setAllFieldsReflectForOneToMany(loggedUser, User.class, loggedUser.getId());
		Group group = wbDAO.getReflect(Group.class, "uniqueId", "\"" + uniqueId + "\"");
		wbDAO.setAllFieldsReflectForManyToMany(group, Group.class, group.getId());
		wbDAO.setAllFieldsReflectForOneToMany(group, Group.class, group.getId());
		for(Post p: group.getWall()){
			wbDAO.setAllFieldsReflectForOneToMany(p, Post.class, p.getId());
		}
		boolean chief = false;
		boolean member = false;
		if(group.getChiefOfGroup().getId() == loggedUser.getId()){
			chief = true;
		}
		if(loggedUser.getGroups().indexOf(group) != -1){
			member = true;
		}
		map.put("chief", chief);
		map.put("member", member);
		map.put("users6", AdditionMethod.prepareFriends(group.getUsers()));
		map.put("allUsers", group.getUsers().size());
		map.put("group", group);
		return "group/viewGroup";
	}
}