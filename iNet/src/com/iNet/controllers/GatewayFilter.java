package com.iNet.controllers;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GatewayFilter implements Filter {
	
	private static String lastLink = null;
	
	@Override
	public void destroy() {}

	@Override
	public void init(FilterConfig config) throws ServletException {}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) req;
		HttpServletResponse httpResponse = (HttpServletResponse) resp;
		if (allowedUrl(httpRequest) || isLoggedIn(httpRequest)) {
			chain.doFilter(req, resp);
			setLastLink(null);
		} else if(allowedUrlRegistration(httpRequest)){
			httpResponse.sendRedirect("/iNet/registration.action");
			setLastLink(httpRequest.getRequestURI().substring(6));
		} else {			
			setLastLink(httpRequest.getRequestURI().substring(6));
			httpResponse.sendRedirect("/iNet/login.action");
		}
	}

	private boolean isLoggedIn(HttpServletRequest request) {
		return null != request.getSession().getAttribute("loggedUser");
	}

	private boolean allowedUrl(HttpServletRequest request) {
		String uri = request.getRequestURI();
		return uri.endsWith("/iNet/login.action");
	}

	private boolean allowedUrlRegistration(HttpServletRequest request) {
		String uri = request.getRequestURI();
		return uri.endsWith("/iNet/registration.action");
	}

	public static String getLastLink() {
		return lastLink;
	}

	public static void setLastLink(String lastLink) {
		GatewayFilter.lastLink = lastLink;
	}
}