package com.iNet.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.Comment;
import com.iNet.beans.Group;
import com.iNet.beans.Post;
import com.iNet.beans.User;
import com.iNet.beans.enums.LikeDislikeType;
import com.iNet.beans.joinTables.DisLikes;
import com.iNet.beans.joinTables.Likes;
import com.iNet.services.JoinTableDAO;
import com.iNet.services.WorkBaseDAO;

@Controller
public class PostAndCommentController {

	private @Autowired WorkBaseDAO wbDAO;
	private @Autowired JoinTableDAO jtDAO;
	
	/**
	 * Save new post into database
	 * @param request
	 * @param id - id of user on the wall which will be located post
	 * @param newPost - text of new post
	 * @return
	 */
	@RequestMapping(value="userPersonal/addNewPost.html")
	public String addNewPost(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id,
			@RequestParam(value="newPost", required=true) String newPost){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post post = new Post();
		newPost = newPost.replace("\n", "<br>");
		post.setBody(newPost);
		post.setComments(new ArrayList<Comment>());
		post.setDateAndTime(new Date());
		post.setDislike(0);
		post.setLike(0);
		post.setOwner(user);
		post.setUser(wbDAO.getReflect(User.class, "id", Integer.toString(id)));
		wbDAO.saveReflect(post);
		//SELECT max(id) FROM table
		//int lastId = dao.queryForInt("SELECT LAST_INSERT_ID()");
		return "redirect:" + wbDAO.getReflect(User.class, "id", Integer.toString(id)).getUniqueId() + ".html";
	}
	
	/**
	 * Save new post for group into database
	 * @param request
	 * @param id - id of group on the wall which will be located post
	 * @param newPost - text of new post
	 * @return
	 */
	@RequestMapping(value="addNewPostToGroup.html")
	public String addNewPostToGroup(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id,
			@RequestParam(value="newPost", required=true) String newPost){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post post = new Post();
		newPost = newPost.replace("\n", "<br>");
		post.setBody(newPost);
		post.setComments(new ArrayList<Comment>());
		post.setDateAndTime(new Date());
		post.setDislike(0);
		post.setLike(0);
		post.setOwner(user);
		post.setGroup(wbDAO.getReflect(Group.class, "id", Integer.toString(id)));
		wbDAO.saveReflect(post);
		return "redirect:viewGroup/" + wbDAO.getReflect(Group.class, "id", Integer.toString(id)).getUniqueId() + ".html";
	}
	
	/**
	 * Delete post from wall
	 * @param request
	 * @param id - id of deleted post
	 * @return
	 */
	@RequestMapping(value="userPersonal/deletePost.html")
	@ResponseBody
	public String deletePost(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post pfo = wbDAO.getReflect(Post.class, "id", Integer.toString(id));
		if ((pfo.getUser() == null && pfo.getOwner().getId() == user.getId()) || (pfo.getUser().getId() == user.getId() || pfo.getOwner().getId() == user.getId())) {
			wbDAO.update("DELETE FROM `DisLikes` WHERE `objectId`=? AND `type`=?", id, whatIndexOfEnum(LikeDislikeType.POST));
			wbDAO.update("DELETE FROM `Likes` WHERE `objectId`=? AND `type`=?", id, whatIndexOfEnum(LikeDislikeType.POST));
			wbDAO.update("DELETE FROM `Comments` WHERE `postId`=?", id);
			wbDAO.delete(Post.class, id);
			return "true";
		} else {
			return "false";
		}
	}
	
	/**
	 * Add or delete like to post 
	 * @param request
	 * @param id - id of post
	 * @return
	 */
	@RequestMapping(value="userPersonal/changeLikeForPost.html")
	@ResponseBody
	public String changeLikeForPost(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post post = wbDAO.getReflect(Post.class, "id", Integer.toString(id));
		if(jtDAO.ifExistsInJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST))){
			jtDAO.deleteFromJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
			post.setLike(post.getLike()-1);
			wbDAO.saveReflect(post);
			return post.getLike().toString();
		} else {
			if(jtDAO.ifExistsInJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST))){
				post.setDislike(post.getDislike()-1);
				jtDAO.deleteFromJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
				jtDAO.addToJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
				post.setLike(post.getLike()+1);
				wbDAO.saveReflect(post);
				return post.getDislike() + "|" + post.getLike();
			} else {
				jtDAO.addToJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
				post.setLike(post.getLike()+1);
				wbDAO.saveReflect(post);
				return post.getLike().toString();
			}
		}
	}
	
	/**
	 * Add or delete dislike to post 
	 * @param request
	 * @param id - id of post
	 * @return
	 */
	@RequestMapping(value="userPersonal/changeDisLikeForPost.html")
	@ResponseBody
	public String changeDisLikeForPost(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post post = wbDAO.getReflect(Post.class, "id", Integer.toString(id));
		if(jtDAO.ifExistsInJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST))){
			jtDAO.deleteFromJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
			post.setDislike(post.getDislike()-1);
			wbDAO.saveReflect(post);
			return post.getDislike().toString();
		} else {
			if(jtDAO.ifExistsInJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST))){
				post.setLike(post.getLike()-1);
				jtDAO.deleteFromJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
				jtDAO.addToJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
				post.setDislike(post.getDislike()+1);
				wbDAO.saveReflect(post);
				return post.getDislike() + "|" + post.getLike();
			} else {
				jtDAO.addToJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.POST));
				post.setDislike(post.getDislike()+1);
				wbDAO.saveReflect(post);
				return post.getDislike().toString();
			}
		}
	}
	
	private int whatIndexOfEnum(LikeDislikeType value){
		LikeDislikeType[] myEnumArray = LikeDislikeType.values();
		for(int i = 0; i < myEnumArray.length; i++){
			if(myEnumArray[i].equals(value)){
				return i;
			}
		}
		return 0;
	}
	
	/**
	 * Save comment to post into database
	 * @param request
	 * @param id - id of post
	 * @param body - body of comment
	 * @param userId - id of user which have post 
	 * @return
	 */
	@RequestMapping(value="userPersonal/addNewComment.html")
	public String addNewComment(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id,
			@RequestParam(value="comment", required=true) String body,
			@RequestParam(value="userId", required=true) Integer userId){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post post = wbDAO.getReflect(Post.class, "id", Integer.toString(id));
		List<Comment> comments = post.getComments();
		Comment comment = new Comment();
		comment.setBody(body);
		comment.setPost(post);
		comment.setOwner(user);
		comment.setLike(0);
		comment.setDislike(0);
		comment.setDate(new Date());
		comments.add(comment);
		post.setComments(comments);
		wbDAO.saveReflect(comment);
		wbDAO.saveReflect(post);
		return "redirect:"+wbDAO.getReflect(User.class, "id", Integer.toString(userId)).getUniqueId() + ".html";
	}
	
	/**
	 * Save comment to post for group wall into database
	 * @param request
	 * @param id - id of post
	 * @param body - body of comment
	 * @param groupId - id of group which have post 
	 * @return
	 */
	@RequestMapping(value="group/addNewCommentToGroup.html")
	public String addNewCommentToGroup(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id,
			@RequestParam(value="comment", required=true) String body,
			@RequestParam(value="groupId", required=true) Integer groupId){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Post post = wbDAO.getReflect(Post.class, "id", Integer.toString(id));
		List<Comment> comments = post.getComments();
		Comment comment = new Comment();
		comment.setBody(body);
		comment.setPost(post);
		comment.setOwner(user);
		comment.setLike(0);
		comment.setDislike(0);
		comment.setDate(new Date());
		comments.add(comment);
		post.setComments(comments);
		wbDAO.saveReflect(comment);
		wbDAO.saveReflect(post);
		return "redirect:/viewGroup/"+wbDAO.getReflect(Group.class, "id", Integer.toString(groupId)).getUniqueId() + ".html";
	}

	/**
	 * Delete comment from wall
	 * @param request
	 * @param id - id of deleted comment
	 * @return
	 */
	@RequestMapping(value="userPersonal/deleteComment.html")
	@ResponseBody
	public String deleteComment(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Comment comment = wbDAO.getReflect(Comment.class, "id", Integer.toString(id));
		if (comment.getOwner().getId() == user.getId() || comment.getPost().getUser().getId() == user.getId()) {
			wbDAO.update("DELETE FROM `DisLikes` WHERE `objectId`=? AND `type`=?", id, whatIndexOfEnum(LikeDislikeType.COMMENT));
			wbDAO.update("DELETE FROM `Likes` WHERE `objectId`=? AND `type`=?", id, whatIndexOfEnum(LikeDislikeType.COMMENT));
			wbDAO.delete(Comment.class, id);
			return "true";
		} else {
			return "false";
		}
	}
	
	/**
	 * Add or delete like to comment 
	 * @param request
	 * @param id - id of comment
	 * @return
	 */
	@RequestMapping(value="userPersonal/changeLikeForComment.html")
	@ResponseBody
	public String changeLikeForComment(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Comment comment = wbDAO.getReflect(Comment.class, "id", Integer.toString(id));
		if(jtDAO.ifExistsInJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT))){
			jtDAO.deleteFromJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
			comment.setLike(comment.getLike()-1);
			wbDAO.saveReflect(comment);
			return comment.getLike().toString();
		} else {
			if(jtDAO.ifExistsInJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT))){
				comment.setDislike(comment.getDislike()-1);
				jtDAO.deleteFromJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
				jtDAO.addToJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
				comment.setLike(comment.getLike()+1);
				wbDAO.saveReflect(comment);
				return comment.getDislike() + "|" + comment.getLike();
			} else {
				jtDAO.addToJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
				comment.setLike(comment.getLike()+1);
				wbDAO.saveReflect(comment);
				return comment.getLike().toString();
			}
		}
	}
	
	/**
	 * Add or delete dislike to comment 
	 * @param map
	 * @param request
	 * @param id - id of post
	 * @return
	 */
	@RequestMapping(value="userPersonal/changeDisLikeForComment.html")
	@ResponseBody
	public String changeDisLikeForComment(HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		Comment comment = wbDAO.getReflect(Comment.class, "id", Integer.toString(id));
		if(jtDAO.ifExistsInJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT))){
			jtDAO.deleteFromJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
			comment.setDislike(comment.getDislike()-1);
			wbDAO.saveReflect(comment);
			return comment.getDislike().toString();
		} else {
			if(jtDAO.ifExistsInJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT))){
				comment.setLike(comment.getLike()-1);
				jtDAO.deleteFromJoinTable(Likes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
				jtDAO.addToJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
				comment.setDislike(comment.getDislike()+1);
				wbDAO.saveReflect(comment);
				return comment.getDislike() + "|" + comment.getLike();
			} else {
				jtDAO.addToJoinTable(DisLikes.class, user.getId(), id, whatIndexOfEnum(LikeDislikeType.COMMENT));
				comment.setDislike(comment.getDislike()+1);
				wbDAO.saveReflect(comment);
				return comment.getDislike().toString();
			}
		}
	}
}