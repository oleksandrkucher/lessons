package com.iNet.controllers;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.Message;
import com.iNet.beans.User;
import com.iNet.services.WorkBaseDAO;

@Controller
public class MessageController {

	private @Autowired WorkBaseDAO wbDAO;
	
	/**
	 * Show messages which send logged user
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/sentMessages.html")
	public String sentMessages(ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
		List<Message> messages = user.getSentMessages();
		Iterator<Message> iterator = messages.iterator();
		while(iterator.hasNext()){
			if(iterator.next().isDelByOwner()){
				iterator.remove();
			}
		}
		map.put("messages", messages);
		return "message/sentMessages";
	}
	
	/**
	 * Show messages which get logged user
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/receivedMessages.html")
	public String receivedMessages(ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
		List<Message> messages = user.getReceivedMessages();
		Iterator<Message> iterator = messages.iterator();
		while(iterator.hasNext()){
			if(iterator.next().isDelByGetter()){
				iterator.remove();
			}
		}
		map.put("messages", messages);
		return "message/receivedMessages";
	}
	
	/**
	 * Show page for read get message and field for answer
	 * @param map
	 * @param request
	 * @param id - id of message
	 * @return
	 */
	@RequestMapping(value="userPersonal/readGetedAndSend.html")
	public String readGetedAndSendMessage(ModelMap map, HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
		Message mess = wbDAO.getReflect(Message.class, "id", Integer.toString(id));
		if(mess.getRecipient().getId() == user.getId()){
			mess.setNewMessage(false);
			wbDAO.saveReflect(mess);
		}
		map.put("message", mess);
		return "message/readGetedAndSend";
	}
	
	/**
	 * Show page for read send message and field for one more message
	 * @param map
	 * @param request
	 * @param id - id of message
	 * @return
	 */
	@RequestMapping(value="userPersonal/readSentAndSend.html")
	public String readSentAndSendMessage(ModelMap map, HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
		map.put("message", wbDAO.getReflect(Message.class, "id", Integer.toString(id)));
		return "message/readSentAndSend";
	}

	/**
	 * Show page with field for new message
	 * @param map
	 * @param request
	 * @param id - id of user which must get new message
	 * @return
	 */
	@RequestMapping(value="userPersonal/sendMessage.html")
	public String sendMessage(ModelMap map, HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		User user = (User) request.getSession().getAttribute("loggedUser");
		map.put("recepient", wbDAO.getReflect(User.class, "id", Integer.toString(id)));
		map.put("sender", user);
		return "message/sendMessage";
	}

	/**
	 * Save new message to database
	 * @param map
	 * @param request
	 * @param id - id of user
	 * @param body - body of message
	 * @return
	 */
	@RequestMapping(value="userPersonal/saveMessage.html")
	public String saveMessage(ModelMap map, HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id,
			@RequestParam(value="body", required=true) String body){
		User user = (User) request.getSession().getAttribute("loggedUser");
		if (user.getiInTheyBlackList().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) == -1) {
			Message message = new Message();
			message.setDelByGetter(false);
			message.setDelByOwner(false);
			message.setNewMessage(true);
			message.setRecipient(wbDAO.getReflect(User.class, "id", Integer.toString(id)));
			message.setSender(user);
			body = body.replace("\n", "<br>");
			message.setBody(body);
			message.setDate(new Date());
			wbDAO.saveReflect(message);
			wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
			return "redirect:sentMessages.html";
		} else {
			return "error/youCant";
		}
	}

	/**
	 * Delete message from database or indicates as deleted if it delete only one user
	 * @param id - id of message
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/deleteMessage.html")
	public String deleteMessage (@RequestParam(value="id", required=true) Integer id,
			HttpServletRequest request){
		Message mess = wbDAO.getReflect(Message.class, "id", Integer.toString(id));
		User user = (User) request.getSession().getAttribute("loggedUser");
		if(mess.getRecipient().getId() == user.getId() && !mess.isDelByOwner()){
			mess.setDelByGetter(true);
			wbDAO.saveReflect(mess);
		} else if(mess.getSender().getId() == user.getId() && !mess.isDelByGetter()){
			mess.setDelByOwner(true);
			wbDAO.saveReflect(mess);
		} else {
			wbDAO.delete(Message.class, id);
		}
		return "redirect:receivedMessages.html";
	}
	
	/**
	 * Count all new messages of logged user
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping(value="userPersonal/howManyNewMessages.html")
	@ResponseBody
	public String howManyNewMessages(HttpServletRequest request, ModelMap map){
		User loggedUser = (User) request.getSession().getAttribute("loggedUser");
		int num = wbDAO.queryForInt("SELECT COUNT(*) FROM `Messages` WHERE `recipientId`=" + 
				loggedUser.getId() + " AND `newMessage`=1");
		return "" + num;
	}
}