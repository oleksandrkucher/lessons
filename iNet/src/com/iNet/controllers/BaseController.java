package com.iNet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.Comment;
import com.iNet.beans.Group;
import com.iNet.beans.Message;
import com.iNet.beans.Post;
import com.iNet.beans.User;
import com.iNet.beans.joinTables.DisLikes;
import com.iNet.beans.joinTables.Follow;
import com.iNet.beans.joinTables.Likes;
import com.iNet.services.CreateTableDAO;
import com.iNet.services.DAO;

/**
 * Controller for work with main page - "Index.html"
 * 
 * @author kol
 */
@Controller
public class BaseController {
	
	private @Autowired DAO dao;
	private @Autowired CreateTableDAO ctDAO;
	
	/**
	 * Show index.action with information about tables "User" and "Ticket" (record count)
	 * Process request "/index.html"
	 * @param map
	 * @return - name of jsp page
	 */
	@RequestMapping(value="index.action")
	public String index (ModelMap map) {
		if (true) {
			ctDAO.createTable(Likes.class);
			ctDAO.createTable(DisLikes.class);
			ctDAO.createTable(Comment.class);
			ctDAO.createTable(Post.class);
			ctDAO.createTable(Follow.class);
			ctDAO.createTable(User.class);
			ctDAO.createTable(Group.class);
			ctDAO.createTable(Message.class);
			ctDAO.createJoinTables();
		}
		map.put("users", dao.getSize(User.class));
		return "index";
	}
	
	/**
	 * Show index.html
	 * @param map
	 * @return
	 */
	@RequestMapping(value="index.html")
	public String indexHtml (ModelMap map) {
		return "redirect:index.action";
	}
	
	/**
	 * Return number of all registered users
	 * @return
	 */
	@RequestMapping(value="howMany.action")
	@ResponseBody
	public String howManyRegistered (){
		return "" + dao.getSize(User.class);
	}
}