package com.iNet.controllers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.iNet.beans.Group;
import com.iNet.beans.Post;
import com.iNet.beans.User;
import com.iNet.beans.enums.BirthdayStatus;
import com.iNet.beans.enums.Country;
import com.iNet.beans.enums.ImportantInOthers;
import com.iNet.beans.enums.ModeOfStudy;
import com.iNet.beans.enums.PersonalPriority;
import com.iNet.beans.enums.PoliticalViews;
import com.iNet.beans.enums.RelationshipStatus;
import com.iNet.beans.enums.Sex;
import com.iNet.beans.enums.StatusOfStudy;
import com.iNet.beans.enums.ViewsOnAlcohol;
import com.iNet.beans.enums.ViewsOnSmoking;
import com.iNet.interfaces.DynamicValues;
import com.iNet.services.DAO;
import com.iNet.services.WorkBaseDAO;

/**
 * Controller for work with table "User"
 */
@Controller
public class UserController implements DynamicValues {

	private @Autowired WorkBaseDAO wbDAO;
	private @Autowired DAO dao;
	private final String[] monthName = {"January", "February", "March", "April", "May", "June", "July", 
		"August", "September", "October", "November", "December"};
	
	/**
	 * Gets all values from table "User" and put it to jsp page
	 * Process request "/listUsers.html"
	 * @param map
	 * @return - name of jsp page, for view this list, without suffix
	 */
	@RequestMapping(value="listUsers.html")
 	public String list (ModelMap map){
		map.put("users", wbDAO.listReflect(User.class));
		return "users/users";
	}
	
	/**
	 * Load one record of logged User from table "User" for updating, and put it to jsp page
	 * Process request "/loadUser.html"
	 * @param id - id of selected record
	 * @param map
	 * @return - name of jsp page, for view and correct this record, without suffix
	 */
	@RequestMapping(value="loadUser.html")
	public String load (ModelMap map, HttpServletRequest request){
		User loggedUser = (User)request.getSession().getAttribute("loggedUser");
		if(loggedUser != null){
			map = enumsPrepareModelMap(map);
			map.put("month", monthName);
			map.put("user", loggedUser);
		}
		return "users/user";
	}
	
	/**
	 * Prepare and show main page of users profile
	 * @param uniqueId - unique id us user
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/{uniqueId}.html")
	public String loadPersonalPage (@PathVariable String uniqueId, ModelMap map, HttpServletRequest request){
		User user = wbDAO.getReflect(User.class, "uniqueId", "\""+uniqueId+"\"");
		if(user != null){
			User loggedUser = (User)request.getSession().getAttribute("loggedUser");
			wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
			wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
			wbDAO.setAllFieldsReflectForManyToMany(loggedUser, User.class, loggedUser.getId());
			wbDAO.setAllFieldsReflectForOneToMany(loggedUser, User.class, loggedUser.getId());
			for(Post p: user.getWall()){
				wbDAO.setAllFieldsReflectForOneToMany(p, Post.class, p.getId());
			}
			if(loggedUser.getiInTheyBlackList().indexOf(user) >= 0){
				return "error/youCant";
			} else {
				String statusOfUser = "";
				boolean bookmark = false;
				boolean blacklist = false;
				boolean online = false;
				if(request.getServletContext().getAttribute(user.getId().toString()) != null){
					online = true;
				}
				if (loggedUser.getId() == user.getId()) {
					statusOfUser = "iAm";
				} else if (loggedUser.getMyFriends().indexOf(user) >= 0) {
					statusOfUser = "friend";
				} else if (loggedUser.getiFollow().indexOf(user) >= 0) {
					statusOfUser = "iFollow";
				} else if (loggedUser.getMyFollowers().indexOf(user) >= 0) {
					statusOfUser = "myFollower";
				} else {
					statusOfUser = "another";
				}
				if (loggedUser.getBookMarksUsers().indexOf(user) >= 0) {
					bookmark = true;
				}
				if (loggedUser.getInMyBlackList().indexOf(user) >= 0) {
					blacklist = true;
				}
				map = enumsPrepareModelMap(map);
				map.put("month", monthName);
				map.put("online", online);
				map.put("user", user);
				map.put("allFriends", AdditionMethod.prepareFriends(user.getMyFriends()));
				map.put("allFriendsCount", user.getMyFriends().size());
				map.put("allOnline", AdditionMethod.prepareFriends(AdditionMethod.whoIsOnline(request, user)));
				map.put("allOnlineCount", AdditionMethod.whoIsOnline(request, user).size());
				map.put("allGroups", prepareGroups(user));
				map.put("allGroupsCount", user.getGroups().size());
				map.put("status", statusOfUser);
				map.put("bookmark", bookmark);
				map.put("blacklist", blacklist);
				return "users/userOne";
			}
		} else {
			return "erroe/error404";
		}
	}
	
	/**
	 * Delete record from table "User" by id
	 * Process request "/deleteUser.html"
	 * @param id - id of record which you want to delete
	 * @return - redirect to page for view all values from table
	 */
	@RequestMapping(value="deleteUser.html")
	public String delete (@RequestParam(value="id", required=true) Integer id){
		wbDAO.delete(User.class, id);
		return "redirect:listUsers.html";
	}
	
	/**
	 * Add or update one record in table "User"
	 * Process request "/saveUser.html"
	 * @param user - object for addition or updating values in table
	 * @return - redirect to page for view all values from table
	 * @throws ParseException 
	 * @throws IOException 
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value="saveUser.html")
	public String save (User user, 
			@RequestParam(value="relationshipStatus", required=false) Integer relationshipStatus,
			@RequestParam(value="importantInOthers", required=false) Integer importantInOthers,
			@RequestParam(value="personalPriority", required=false) Integer personalPriority,
			@RequestParam(value="viewsOnAlcohol", required=false) Integer viewsOnAlcohol,
			@RequestParam(value="viewsOnSmoking", required=false) Integer viewsOnSmoking,
			@RequestParam(value="politicalViews", required=false) Integer politicalViews,
			@RequestParam(value="countryOfStudy", required=false) Integer countryOfStudy,
			@RequestParam(value="statusOfStudy", required=false) Integer statusOfStudy,
			@RequestParam(value="modeOfStudy", required=false) Integer modeOfStudy,
			@RequestParam(value="status", required=false) Integer birthdayStatus,
			@RequestParam(value="file", required=false) MultipartFile file,
			@RequestParam(value="day", required=false) Integer dayOfMonth, 
			@RequestParam(value="oldLink", required=false) String oldLink,
			@RequestParam(value="month", required=false) Integer month,
			@RequestParam(value="year", required=false) Integer year,
			@RequestParam(value="sex", required=false) Integer sex,
			HttpServletRequest request) throws ParseException, IOException{
		user.setRelationshipStatus(relationshipStatus);
		user.setImportantInOthers(importantInOthers);
		user.setPersonalPriority(personalPriority);
		user.setViewsOnAlcohol(viewsOnAlcohol);
		user.setViewsOnSmoking(viewsOnSmoking);
		user.setPoliticalViews(politicalViews);
		user.setCountryOfStudy(countryOfStudy);
		user.setBirthdayStatus(birthdayStatus);
		user.setStatusOfStudy(statusOfStudy);
		user.setModeOfStudy(modeOfStudy);
		user.setSex(sex);
		Date date = new Date();
		date.setDate(dayOfMonth);
		date.setYear(year);
		date.setMonth(month);
		user.setBirthday(date);
		if(user.getPassword() != null && !"".equals(user.getPassword())){
			user.setPassword(dao.passToMD5(user.getPassword()));
		} else {
			user.setPassword(((User)request.getSession().getAttribute("loggedUser")).getPassword());
		}
		if (file != null && file.getSize() <= IMAGE_MAX_SIZE && !"".equals(file.getOriginalFilename())) {
			//System.out.println(file.getOriginalFilename());
			InputStream in = file.getInputStream();
			String fileName = file.getOriginalFilename().
					substring(file.getOriginalFilename().lastIndexOf('.'));
			FileOutputStream out = new FileOutputStream(AdditionMethod.getPathForUser()
					+ user.getUniqueId() + fileName);
			int tmp;
			try {
				while ((tmp = in.read()) != -1) {
					out.write(tmp);
				}
				in.close();
				out.close();
			} catch (IOException e) {}
			user.setLinkToFoto(user.getUniqueId() + fileName);
		} else {
			user.setLinkToFoto(oldLink);
		}
		wbDAO.saveReflect(user);
		request.getSession().setAttribute("loggedUser", user);
		return "redirect:userPersonal/" + user.getUniqueId() + ".html";
	}
	
	/**
	 * Add to ModelMap values of all enumerations which use user
	 * @param map
	 * @return
	 */
	private ModelMap enumsPrepareModelMap(ModelMap map){
		Map<Integer, RelationshipStatus> relationshipStatus = new HashMap<>();
		Map<Integer, ImportantInOthers> importantInOthers = new HashMap<>();
		Map<Integer, PersonalPriority> personalPriority = new HashMap<>();
		Map<Integer, ViewsOnAlcohol> viewsOnAlcohol = new HashMap<>();
		Map<Integer, ViewsOnSmoking> viewsOnSmoking = new HashMap<>();
		Map<Integer, PoliticalViews> politicalViews = new HashMap<>();
		Map<Integer, BirthdayStatus> birthdayStatus = new HashMap<>();
		Map<Integer, StatusOfStudy> statusOfStudy = new HashMap<>();
		Map<Integer, ModeOfStudy> modeOfStudy = new HashMap<>();
		Map<Integer, Country> countryOfStudy = new HashMap<>();
		Map<Integer, Sex> sex = new HashMap<>();
		int i = 0;
		for(ViewsOnSmoking v: ViewsOnSmoking.values()){
			viewsOnSmoking.put(i++, v);
		}
		i = 0;
		for(BirthdayStatus v: BirthdayStatus.values()){
			birthdayStatus.put(i++, v);
		}
		i = 0;
		for(ViewsOnAlcohol v: ViewsOnAlcohol.values()){
			viewsOnAlcohol.put(i++, v);
		}
		i = 0;
		for(PoliticalViews v: PoliticalViews.values()){
			politicalViews.put(i++, v);
		}
		i = 0;
		for(PersonalPriority v: PersonalPriority.values()){
			personalPriority.put(i++, v);
		}
		i = 0;
		for(ImportantInOthers v: ImportantInOthers.values()){
			importantInOthers.put(i++, v);
		}
		i = 0;
		for(RelationshipStatus v: RelationshipStatus.values()){
			relationshipStatus.put(i++, v);
		}
		i = 0;
		for(Sex v: Sex.values()){
			sex.put(i++, v);
		}
		i = 0;
		for(StatusOfStudy v: StatusOfStudy.values()){
			statusOfStudy.put(i++, v);
		}
		i = 0;
		for(Country v: Country.values()){
			countryOfStudy.put(i++, v);
		}
		i = 0;
		for(ModeOfStudy v: ModeOfStudy.values()){
			modeOfStudy.put(i++, v);
		}
		map.put("sex", sex);
		map.put("monthName", monthName);
		map.put("modeOfStudy", modeOfStudy);
		map.put("statusOfStudy", statusOfStudy);
		map.put("countryOfStudy", countryOfStudy);
		map.put("politicalViews", politicalViews);
		map.put("viewsOnAlcohol", viewsOnAlcohol);
		map.put("birthdayStatus", birthdayStatus);
		map.put("viewsOnSmoking", viewsOnSmoking);
		map.put("personalPriority", personalPriority);
		map.put("importantInOthers", importantInOthers);
		map.put("relationshipStatus", relationshipStatus);
		return map;
	}
	
	/**
	 * Gets 6 groups with using random and create list with them
	 * @param user
	 * @return
	 */
	private List<Group> prepareGroups(User user){
		List<Group> listGroups = user.getGroups();
		List<Group> list = new ArrayList<>();
		int count = 0;
		if (listGroups.size() < 6) {
			count = listGroups.size();
		} else {
			count = 6;
		}
		for (int i = 0; i < count; i++) {
			list.add(listGroups.get(i));
		}
		return list;
	}
}