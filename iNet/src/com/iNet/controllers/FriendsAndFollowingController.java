package com.iNet.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.User;
import com.iNet.beans.joinTables.Follow;
import com.iNet.beans.joinTables.Friends;
import com.iNet.services.WorkBaseDAO;
import com.iNet.services.JoinTableDAO;

@Controller
public class FriendsAndFollowingController {

	private @Autowired WorkBaseDAO wbDAO;
	private @Autowired JoinTableDAO jtDAO;
	
	/**
	 * Method add selected user to friend if it was into follower or in following else
	 * use ajax
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="userPersonal/addFriend.html")
	@ResponseBody
	public String addFriend (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getId() != id){
				if(user.getMyFollowers().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
					jtDAO.addToJoinTable(Friends.class, user.getId(), id);
					jtDAO.deleteFromJoinTable(Follow.class, id, user.getId());
				} else if(user.getiFollow().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) < 0){
					jtDAO.addToJoinTable(Follow.class, user.getId(), id);
				}
			}
		}
		return "true";
	}
	
	/**
	 * Method works with ajax and delete user from friends of current user
	 * but this user saving in my followers
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="userPersonal/deleteFriend.html")
	@ResponseBody
	public String deleteFriend (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getMyFriends().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
				jtDAO.deleteFromJoinTable(Friends.class, user.getId(), id);
				jtDAO.deleteFromJoinTable(Friends.class, id, user.getId());
				jtDAO.addToJoinTable(Follow.class, id, user.getId());
			}
		}
		return "true";
	}
	
	/**
	 * Add user to my list of users which i follow 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="userPersonal/addFollowing.html")
	@ResponseBody
	public String addFollowing (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getiFollow().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) < 0){
				jtDAO.addToJoinTable(Follow.class, user.getId(), id);
			}
		}
		return "true";
	}
	
	/**
	 * Remove user from my list of following
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="userPersonal/deleteFollowing.html")
	@ResponseBody
	public String deleteFollowing (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getiFollow().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
				jtDAO.deleteFromJoinTable(Follow.class, user.getId(), id);
			}
		}
		return "true";
	}
	
	/**
	 * Update all values of logged user, gets all friends and show page friends.html with all friends
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/friends.html")
	public String friends (ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allFriends", user.getMyFriends());
		map.put("friendStatus", false);
		return "friendsAndFollows/friends";
	}

	/**
	 * Update all values of logged user, gets all online friends and show them
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/friendsOnline.html")
	public String friendsOnline (ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allFriends", AdditionMethod.whoIsOnline(request, user));
		return "friendsAndFollows/friends";
	}
	
	/**
	 * Update all values of logged user, gets all followers and show page followers.html with all followers
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/followers.html")
	public String followMe (ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allFollowers", user.getMyFollowers());
		return "friendsAndFollows/followers";
	}
	
	/**
	 * Update all values of logged user, gets all followings and show page ifollow.html with all followings
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/ifollow.html")
	public String iFollow (ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allFollowings", user.getiFollow());
		return "friendsAndFollows/followings";
	}
	
	/**
	 * Show all friend of user
	 * @param id - id of user
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/browseFriends.html")
	public String browseFriends (@RequestParam(value="id", required=true) Integer id, 
			ModelMap map, HttpServletRequest request){
		User user = wbDAO.getReflect(User.class, "id", Integer.toString(id));
		if(user != null){
			User loggedUser = (User)request.getSession().getAttribute("loggedUser");
			wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
			wbDAO.setAllFieldsReflectForManyToMany(loggedUser, User.class, loggedUser.getId());
			if(loggedUser.getiInTheyBlackList().indexOf(user) >= 0){
				return "error/youCant";
			} else {
				map.put("allFriends", user.getMyFriends());
				boolean friend = false;
				if(user.getMyFriends().indexOf(loggedUser) == -1 && user.getId() != loggedUser.getId()){
					friend = true;
				}
				map.put("friendStatus", friend);
				return "friendsAndFollows/friends";
			}
		} else {
			return "error/error404";
		}
	}
}