package com.iNet.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.User;
import com.iNet.beans.joinTables.BookMarksUsers;
import com.iNet.services.WorkBaseDAO;
import com.iNet.services.JoinTableDAO;

@Controller
public class BookmarksController {

	private @Autowired WorkBaseDAO wbDAO;
	private @Autowired JoinTableDAO jtDAO;
	
	/**
	 * Method works with ajax and add user's bookmark
	 * @param request
	 * @param id - id of current user
	 * @return
	 */
	@RequestMapping(value="userPersonal/addBookmark.html")
	@ResponseBody
	public String addBookmark (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getBookMarksUsers().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) < 0){
				jtDAO.addToJoinTable(BookMarksUsers.class, user.getId(), id);
			}
		}
		return "true";
	}
	
	/**
	 * Method works with ajax and delete user's bookmark
	 * @param request
	 * @param id - id of current user
	 * @return
	 */
	@RequestMapping(value="userPersonal/deleteBookmark.html")
	@ResponseBody
	public String deleteBookmark (HttpServletRequest request,
			@RequestParam(value="id", required=true) Integer id){
		if (id != null && id > 0) {
			User user = (User) request.getSession().getAttribute("loggedUser");
			if(user.getBookMarksUsers().indexOf(wbDAO.getReflect(User.class, "id", Integer.toString(id))) >= 0){
				jtDAO.deleteFromJoinTable(BookMarksUsers.class, user.getId(), id);
				return "true";
			}
		}
		return "false";
	}
	
	/**
	 * Update all values of logged user, gets all users in bookmarks and show page bookmarksUsers.html with all bookmarks
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping(value="userPersonal/bookmarksUsers.html")
	public String bookmarks (ModelMap map, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		map.put("allBookmarks", user.getBookMarksUsers());
		return "bookmark/bookmarksUsers";
	}
}