package com.iNet.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iNet.beans.User;
import com.iNet.services.DAO;
import com.iNet.services.WorkBaseDAO;

@Controller
public class LoginRegController {

	private @Autowired DAO dao;
	private @Autowired WorkBaseDAO wbDAO;

	/**
	 * Gets login and password and authenticates the user
	 * @param request
	 * @param map
	 * @param login
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "login.action")
	public String login(HttpServletRequest request, ModelMap map,
			@RequestParam(value="j_username", required=false) String login,
			@RequestParam(value="j_password", required=false) String password) {
		if (null != login && null != password) {
			User user = wbDAO.getReflect(User.class, "loginEmail", "\""+login+"\"");
			if (null != user && user.getPassword().equals(dao.passToMD5(password))) {
				wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
				wbDAO.setAllFieldsReflectForOneToMany(user, User.class, user.getId());
				request.getSession().setAttribute("loggedUser", user);
				request.getServletContext().setAttribute(user.getId().toString(), "logged");
				if(GatewayFilter.getLastLink() != null && !"null.html".equals(GatewayFilter.getLastLink()) &&
						GatewayFilter.getLastLink().endsWith(".html")){
					return "redirect:"+GatewayFilter.getLastLink();
				} else {
					return "redirect:userPersonal/"+user.getUniqueId()+".html";
				}
			} else {
				map.put("error", "Wrong password or email");
			}
		}
		return "loginAndReg/login";
	}
	
	/**
	 * Gets login, password, confirm of password, uniqueId, first name, last name and save user with this 
	 * parameters in database if password == passwordConfirm
	 * @param map
	 * @param loginEmail
	 * @param password
	 * @param uniqueId
	 * @param firstName
	 * @param lastName
	 * @param passwordConfirm
	 * @return
	 */
	@RequestMapping(value="registration.action")
	public String registration(ModelMap map, 
			@RequestParam(value="loginEmail", required=false) String loginEmail,
			@RequestParam(value="password", required=false) String password,
			@RequestParam(value="uniqueId", required=false) String uniqueId,
			@RequestParam(value="firstName", required=false) String firstName,
			@RequestParam(value="lastName", required=false) String lastName,
			@RequestParam(value="passwordsignup_confirm", required=false) String passwordConfirm){
		if(null != loginEmail && null != password && null != passwordConfirm && null != uniqueId){
			if(password.equals(passwordConfirm) && wbDAO.getReflect(User.class, "uniqueId", "\""+uniqueId+"\"") == null){
				User user = new User();
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setLoginEmail(loginEmail);
				user.setPassword(dao.passToMD5(password));
				user.setLinkToFoto("defaultFoto.jpg");
				user.setUniqueId(uniqueId);
				wbDAO.saveReflect(user);
			}
			return "loginAndReg/login";
		} else {
			return "loginAndReg/registration";
		}
	}
	
	/**
	 * Delete user if from servlet context
	 * @param request
	 * @return
	 */
	@RequestMapping(value="logout.action")
	public String logout(HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("loggedUser");
		if (null != user) {
			request.getSession().invalidate();
			request.getServletContext().removeAttribute(user.getId().toString());
			return "redirect:login.action";
		}
		return "redirect:index.html";
	}
	
	/**
	 * Gets all friends which online now
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value="whoIsOnline.html")
	@ResponseBody
	private String whoIsOnline(HttpServletRequest request, 
			@RequestParam(value="id", required=true) Integer id){
		User user = wbDAO.getReflect(User.class, "id", Integer.toString(id));
		wbDAO.setAllFieldsReflectForManyToMany(user, User.class, user.getId());
		List<User> onlineFriends = new ArrayList<>();
		ServletContext context = request.getServletContext();
		for(User u: user.getMyFriends()){
			if(context.getAttribute(u.getId().toString()) != null){
				onlineFriends.add(u);
			}
		}
		request.getSession().setAttribute("onlineUsers", onlineFriends);
		return "loginAndReg/onlineUsers";
	}
	
	/**
	 * Verify email if it unique or not
	 * @param val
	 * @return
	 */
	@RequestMapping(value="verifyEmail.action")
	@ResponseBody
	public String verifyEmail(@RequestParam(value="val", required=false) String val){
		if(val == null || "".equals(val) ||
				wbDAO.getReflect(User.class, "loginEmail", "\"" + val + "\"") != null){
			return "false";
		} else if(AdditionMethod.checkEmail(val)){
			return "true";
		} else {
			return "false";
		}
	}

	/**
	 * Verify uniqueId if it unique or not
	 * @param val
	 * @return
	 */
	@RequestMapping(value="verifyUniqueId.action")
	@ResponseBody
	public String verifyUniqueId(@RequestParam(value="val", required=false) String val){
		if(val == null || "".equals(val) ||
				wbDAO.getReflect(User.class, "uniqueId", "\"" + val + "\"") != null){
			return "false";
		} else {
			return "true";
		}
	}
}