package com.iNet.interfaces;

public interface DynamicValues {

	public int DEFAULT_SIZE = 10;
	
	public int SIZE_OF_BYTE_COLUMN = 20;
	public int SIZE_OF_LONG_COLUMN = 20;
	public int SIZE_OF_SHORT_COLUMN = 20;
	public int SIZE_OF_BOOLEAN_COLUMN = 1;
	public int SIZE_OF_INTEGER_COLUMN = 20;
	public int SIZE_OF_VARCHAR_COLUMN = 255;
	
	public int SIZE_OF_TEXT_COLUMN = 10000;
	
	public int SIZE_OF_DATE_COLUMN = 5;
	public int SIZE_OF_IDX_COLUMN = 20;
	public int SIZE_OF_ENUM_COLUMN = 20;
	public int SIZE_OF_INDEX_COLUMN = 20;
	public int SIZE_OF_UNIQUE_ID_COLUMN = 20;
	public int SIZE_OF_JOIN_INDEX_COLUMN = 20;
	public int SIZE_OF_LIKE_AND_DISLIKE_COLUMN = 20;
	
	public int IMAGE_MAX_SIZE = 2048000;
}