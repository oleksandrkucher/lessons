package com.iNet.beans;

import java.util.ArrayList;
import java.util.List;

import com.iNet.annotations.Column;
import com.iNet.annotations.GeneratedValue;
import com.iNet.annotations.Id;
import com.iNet.annotations.ManyToMany;
import com.iNet.annotations.ManyToOne;
import com.iNet.annotations.OneToMany;
import com.iNet.annotations.Table;
import com.iNet.beans.enums.Type;
import com.iNet.interfaces.DynamicValues;

/**
 * Class describe table "Group" in our database
 * 
 * @author kol
 */
@Table(name="Group", haveEntity=true)
public class Group extends Entity implements DynamicValues {

	@Id
	@GeneratedValue
	@Column(name="id", type=Type.INTEGER, sizeType=SIZE_OF_INDEX_COLUMN)
	private Integer id;
	
	@Column(name="title", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String title;
	
	@Column(name="uniqueId", sizeType=SIZE_OF_UNIQUE_ID_COLUMN)
	private String uniqueId;
	
	@Column(name="linkToFoto", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String linkToFoto;
	
	@Column(name="info", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String info;
	
	@ManyToOne(thisColumnName="chiefOfGroupId", joinTableName="User")
	private User chiefOfGroup;
	
	@ManyToMany(thisColumnName="users", joinTableName="UsersAndGroups",
			joinColumnName="groupId", classNameForTable="com.iNet.beans.joinTables.UsersAndGroups")
	private List<User> users;
	
	@OneToMany(tableName="Post", columnName="groupId",
			classNameForTable="com.iNet.beans.Post")
	private List<Post> wall;

	public Group(){
		users = new ArrayList<>();
		wall = new ArrayList<>();
	}
	
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public User getChiefOfGroup() {
		return chiefOfGroup;
	}

	public void setChiefOfGroup(User chiefOfGroup) {
		this.chiefOfGroup = chiefOfGroup;
	}

	public List<Post> getWall() {
		return wall;
	}

	public void setWall(List<Post> wall) {
		this.wall = wall;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getLinkToFoto() {
		return linkToFoto;
	}

	public void setLinkToFoto(String linkToFoto) {
		this.linkToFoto = linkToFoto;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}