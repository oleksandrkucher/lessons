package com.iNet.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.iNet.annotations.Column;
import com.iNet.annotations.GeneratedValue;
import com.iNet.annotations.Id;
import com.iNet.annotations.ManyToOne;
import com.iNet.annotations.OneToMany;
import com.iNet.annotations.Table;
import com.iNet.beans.enums.Type;
import com.iNet.interfaces.DynamicValues;

/**
 * Class describe table "Post" in our database
 * 
 * @author kol
 */
@Table(name="Post", haveEntity=true, haveDate=true)
public class Post extends Entity implements DynamicValues {

	@Id
	@GeneratedValue
	@Column(name="id", type=Type.INTEGER, sizeType=SIZE_OF_INDEX_COLUMN)
	private Integer id;
	
	@ManyToOne(thisColumnName="ownerId", joinTableName="User")
	private User owner;
	
	@ManyToOne(thisColumnName="groupId", joinTableName="Group")
	private Group group;
	
	@ManyToOne(thisColumnName="userId", joinTableName="User")
	private User user;
	
	@Column(name="body", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String body;
	
	@Column(name="like", type=Type.INTEGER, sizeType=SIZE_OF_LIKE_AND_DISLIKE_COLUMN)
	private Integer like;

	@Column(name="date", type=Type.DATE_AND_TIME)
	private Date dateAndTime;
	
	@Column(name="dislike", type=Type.INTEGER, sizeType=SIZE_OF_LIKE_AND_DISLIKE_COLUMN)
	private Integer dislike;
	
	@OneToMany(tableName="Comments", columnName="postId",
			classNameForTable="com.iNet.beans.Comment")
    private List<Comment> comments;
	
	public Post(){
		comments = new ArrayList<Comment>();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getLike() {
		return like;
	}

	public void setLike(Integer like) {
		this.like = like;
	}

	public Integer getDislike() {
		return dislike;
	}

	public void setDislike(Integer dislike) {
		this.dislike = dislike;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * @return the dateAndTime
	 */
	public Date getDateAndTime() {
		return dateAndTime;
	}

	/**
	 * @param dateAndTime the dateAndTime to set
	 */
	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	/**
	 * @return the owner
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
}