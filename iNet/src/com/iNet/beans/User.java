package com.iNet.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.iNet.annotations.Column;
import com.iNet.annotations.GeneratedValue;
import com.iNet.annotations.Id;
import com.iNet.annotations.ManyToMany;
import com.iNet.annotations.OneToMany;
import com.iNet.annotations.Table;
import com.iNet.beans.enums.BirthdayStatus;
import com.iNet.beans.enums.Country;
import com.iNet.beans.enums.ImportantInOthers;
import com.iNet.beans.enums.ModeOfStudy;
import com.iNet.beans.enums.PersonalPriority;
import com.iNet.beans.enums.PoliticalViews;
import com.iNet.beans.enums.RelationshipStatus;
import com.iNet.beans.enums.Sex;
import com.iNet.beans.enums.StatusOfStudy;
import com.iNet.beans.enums.Type;
import com.iNet.beans.enums.ViewsOnAlcohol;
import com.iNet.beans.enums.ViewsOnSmoking;
import com.iNet.interfaces.DynamicValues;

/**
 * Class describe table "User" in our database
 * 
 * @author kol
 */
@Table(name="User")
public class User extends Entity implements DynamicValues {

	@Id
	@GeneratedValue
	@Column(name="id", type=Type.INTEGER, sizeType=SIZE_OF_INDEX_COLUMN)
	private Integer id;
    
	@Column(name="loginEmail", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String loginEmail;
	
	@Column(name="password", sizeType=SIZE_OF_VARCHAR_COLUMN)
    private String password;
	
	@Column(name="firstName", sizeType=SIZE_OF_VARCHAR_COLUMN)
    private String firstName;
	
	@Column(name="lastName", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String lastName;

	@Column(name="birthday", type=Type.DATE)
	private Date birthday;
	
	@Column(name="birthdayStatus", type=Type.ENUM)
	private BirthdayStatus birthdayStatus;
	
	@Column(name="uniqueId", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String uniqueId;
	
	@Column(name="nickName", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String nickName;
	
	@Column(name="linkToFoto", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String linkToFoto;
	
	@Column(name="loginPhone", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String loginPhone;
	
	@Column(name="countryOfStudy", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private Country countryOfStudy;
	
	@Column(name="cityOfStudy", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String cityOfStudy;
		 	 
	@Column(name="collegeOrUniversityName", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String collegeOrUniversityName;
		 	 
	@Column(name="department", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String department;
		 	 
	@Column(name="schoolName", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String schoolName;
	
	@Column(name="schoolTown", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String schoolTown;
	
	@Column(name="schoolYears", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String schoolYears;
	
	@Column(name="major", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String major;
		 	 
	@Column(name="modeOfStudy", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private ModeOfStudy modeOfStudy;
		 	 
	@Column(name="statusOfStudy", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private StatusOfStudy statusOfStudy;
		 	 
	@Column(name="graduationYear", sizeType=SIZE_OF_INTEGER_COLUMN)
	private String graduationYear;
	
	@Column(name="homeCity", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String homeCity;
	
	@Column(name="currentCity", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String currentCity;
	
	@Column(name="homeCountry", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String homeCountry;
	
	@Column(name="telephoneNumber", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String telephoneNumber;
	
	@Column(name="skype", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String skype;
	
	@Column(name="personalWebSite", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String personalWebSite;
	
	@Column(name="favoriteMusic", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String favoriteMusic;

	@Column(name="favoriteFilms", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String favoriteFilms;
	
	@Column(name="favoriteTVShows", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String favoriteTVShows;
	
	@Column(name="favoriteBooks", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String favoriteBooks;

	@Column(name="favoriteQuotes", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String favoriteQuotes;
	
	@Column(name="aboutMe", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String aboutMe;
	
	@Column(name="interests", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String interests;
	
	@Column(name="worldView", sizeType=SIZE_OF_VARCHAR_COLUMN)
	private String worldView;
	
	@Column(name="importantInOthers", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private ImportantInOthers importantInOthers;
	
	@Column(name="personalPriority", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private PersonalPriority personalPriority;
	
	@Column(name="politicalViews", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private PoliticalViews politicalViews;
	
	@Column(name="relationshipStatus", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private RelationshipStatus relationshipStatus;
	
	@Column(name="sex", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private Sex sex;
	
	@Column(name="viewsOnAlcohol", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private ViewsOnAlcohol viewsOnAlcohol;
	
	@Column(name="viewsOnSmoking", type=Type.ENUM, sizeType=SIZE_OF_ENUM_COLUMN)
	private ViewsOnSmoking viewsOnSmoking;
	
	@OneToMany(tableName="Posts", columnName="ownerId",
			classNameForTable="com.iNet.beans.Post")
	private List<Post> ownerOfPosts;
	
	@OneToMany(tableName="Post", columnName="userId", 
			classNameForTable="com.iNet.beans.Post")
	private List<Post> wall;
	
	@OneToMany(tableName="Comments", columnName="ownerId",
			classNameForTable="com.iNet.beans.Comment")
    private List<Comment> comments;
	
	@OneToMany(tableName="Group", columnName="chiefOfGroupId",
			classNameForTable="com.iNet.beans.Group")
	private List<Group> groupsWhereIAmAdmin;
	
	@OneToMany(tableName="Messages", columnName="senderId",
			classNameForTable="com.iNet.beans.Message")
	private List<Message> sentMessages;
	
	@OneToMany(tableName="Messages", columnName="recipientId",
			classNameForTable="com.iNet.beans.Message")
	private List<Message> receivedMessages;
	
	@ManyToMany(thisColumnName="iFollow", joinTableName="Follow", 
			joinColumnName="iFollow", classNameForTable="com.iNet.beans.joinTables.Follow")
	private List<User> iFollow;

	@ManyToMany(thisColumnName="myFollowers", joinTableName="Follow", 
			joinColumnName="followMe", classNameForTable="com.iNet.beans.joinTables.Follow")
	private List<User> myFollowers;
	
	@ManyToMany(thisColumnName="myFriends", joinTableName="Friends", 
			joinColumnName="Friends", classNameForTable="com.iNet.beans.joinTables.Friends")
	private List<User> myFriends;
	
	@ManyToMany(thisColumnName="bookMarksUsers", joinTableName="BookMarksUsers",
			joinColumnName="iAmId", classNameForTable="com.iNet.beans.joinTables.BookMarksUsers")
	private List<User> bookMarksUsers;

	@ManyToMany(thisColumnName="inMyBlackList", joinTableName="BlackList", 
			joinColumnName="iDoNotWant", classNameForTable="com.iNet.beans.joinTables.BlackList")
	private List<User> inMyBlackList;

	@ManyToMany(thisColumnName="iInTheyBlackList", joinTableName="BlackList", 
			joinColumnName="doNotWantMe", classNameForTable="com.iNet.beans.joinTables.BlackList")
	private List<User> iInTheyBlackList;

	@ManyToMany(thisColumnName="groups", joinTableName="UsersAndGroups",
			joinColumnName="userId", classNameForTable="com.iNet.beans.joinTables.UsersAndGroups")
	private List<Group> groups;
	
	//later
	@ManyToMany(thisColumnName="bookMarksGroups", joinTableName="BookMarksGroups",
			joinColumnName="iAmId", classNameForTable="com.iNet.beans.joinTables.BookMarksGroups")
	private List<Group> bookMarksGroups;
	
    public User() {
    	wall = new ArrayList<>();
    	groups = new ArrayList<>();
    	comments = new ArrayList<>();
    	iFollow = new ArrayList<>();
    	myFriends = new ArrayList<>();
    	myFollowers = new ArrayList<>();
    	ownerOfPosts = new ArrayList<>();
    	sentMessages = new ArrayList<>();
    	inMyBlackList = new ArrayList<>();
    	bookMarksUsers = new ArrayList<>();
    	bookMarksGroups = new ArrayList<>();
		receivedMessages = new ArrayList<>();
		iInTheyBlackList = new ArrayList<>();
	}

	public List<Post> getOwnerOfPosts() {
		return ownerOfPosts;
	}

	public void setOwnerOfPosts(List<Post> ownerOfPosts) {
		this.ownerOfPosts = ownerOfPosts;
	}

	public List<Post> getWall() {
		return wall;
	}

	public void setWall(List<Post> wall) {
		this.wall = wall;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<User> getInMyBlackList() {
		return inMyBlackList;
	}

    public void setInMyBlackList(List<User> inMyBlackList) {
		this.inMyBlackList = inMyBlackList;
	}
    
    public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Post> getPosts() {
		return ownerOfPosts;
	}

	public void setPosts(List<Post> posts) {
		this.ownerOfPosts = posts;
	}

	public List<Message> getSentMessages() {
		return sentMessages;
	}

	public void setSentMessages(List<Message> sentMessages) {
		this.sentMessages = sentMessages;
	}

	public List<Message> getReceivedMessages() {
		return receivedMessages;
	}

	public void setReceivedMessages(List<Message> receivedMessages) {
		this.receivedMessages = receivedMessages;
	}

	public List<Group> getGroupsWhereIAmAdmin() {
		return groupsWhereIAmAdmin;
	}

	public void setGroupsWhereIAmAdmin(List<Group> groupsWhereIAmAdmin) {
		this.groupsWhereIAmAdmin = groupsWhereIAmAdmin;
	}

	public List<User> getiInTheyBlackList() {
		return iInTheyBlackList;
	}

	public void setiInTheyBlackList(List<User> iInTheyBlackList) {
		this.iInTheyBlackList = iInTheyBlackList;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<User> getBookMarksUsers() {
		return bookMarksUsers;
	}

	public void setBookMarksUsers(List<User> bookMarksUsers) {
		this.bookMarksUsers = bookMarksUsers;
	}

	public List<Group> getBookMarksGroups() {
		return bookMarksGroups;
	}

	public void setBookMarksGroups(List<Group> bookMarksGroups) {
		this.bookMarksGroups = bookMarksGroups;
	}

	public String getLoginEmail() {
		return loginEmail;
	}

	public void setLoginEmail(String usernameEmail) {
		this.loginEmail = usernameEmail;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getLinkToFoto() {
		return linkToFoto;
	}

	public void setLinkToFoto(String linkToFoto) {
		if(linkToFoto != null && !linkToFoto.trim().equals("")){
			this.linkToFoto = linkToFoto;
		} else {
			this.linkToFoto = "defaultFoto.jpg";
		}
		
	}

	public String getLoginPhone() {
		return loginPhone;
	}

	public void setLoginPhone(String loginPhone) {
		this.loginPhone = loginPhone;
	}

	public String getHomeCity() {
		return homeCity;
	}

	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}

	public String getHomeCountry() {
		return homeCountry;
	}

	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getPersonalWebSite() {
		return personalWebSite;
	}

	public void setPersonalWebSite(String personalWebSite) {
		this.personalWebSite = personalWebSite;
	}

	public String getFavoriteMusic() {
		return favoriteMusic;
	}

	public void setFavoriteMusic(String favoriteMusic) {
		this.favoriteMusic = favoriteMusic;
	}

	public String getFavoriteFilms() {
		return favoriteFilms;
	}

	public void setFavoriteFilms(String favoriteFilms) {
		this.favoriteFilms = favoriteFilms;
	}

	public String getFavoriteTVShows() {
		return favoriteTVShows;
	}

	public void setFavoriteTVShows(String favoriteTVShows) {
		this.favoriteTVShows = favoriteTVShows;
	}

	public String getFavoriteBooks() {
		return favoriteBooks;
	}

	public void setFavoriteBooks(String favoriteBooks) {
		this.favoriteBooks = favoriteBooks;
	}

	public String getFavoriteQuotes() {
		return favoriteQuotes;
	}

	public void setFavoriteQuotes(String favoriteQuotes) {
		this.favoriteQuotes = favoriteQuotes;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getWorldView() {
		return worldView;
	}

	public void setWorldView(String worldView) {
		this.worldView = worldView;
	}

	public ImportantInOthers getImportantInOthers() {
		return importantInOthers;
	}

	public void setImportantInOthers(Integer importantInOthers) {
		if(importantInOthers == null){
			importantInOthers = 0;
		}
		this.importantInOthers = ImportantInOthers.values()[importantInOthers];
	}

	public PersonalPriority getPersonalPriority() {
		return personalPriority;
	}

	public void setPersonalPriority(Integer personalPriority) {
		if(personalPriority == null){
			personalPriority = 0;
		}
		this.personalPriority = PersonalPriority.values()[personalPriority];
	}

	public PoliticalViews getPoliticalViews() {
		return politicalViews;
	}

	public void setPoliticalViews(Integer politicalViews) {
		if(politicalViews == null){
			politicalViews = 0;
		}
		this.politicalViews = PoliticalViews.values()[politicalViews];
	}

	public RelationshipStatus getRelationshipStatus() {
		return relationshipStatus;
	}

	public void setRelationshipStatus(Integer relationshipStatus) {
		if(relationshipStatus == null){
			relationshipStatus = 0;
		}
		this.relationshipStatus = RelationshipStatus.values()[relationshipStatus];
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		if(sex == null){
			sex = 0;
		}
		this.sex = Sex.values()[sex];
	}

	public ViewsOnAlcohol getViewsOnAlcohol() {
		return viewsOnAlcohol;
	}

	public void setViewsOnAlcohol(Integer viewsOnAlcohol) {
		if(viewsOnAlcohol == null){
			viewsOnAlcohol = 0;
		}
		this.viewsOnAlcohol = ViewsOnAlcohol.values()[viewsOnAlcohol];
	}

	public ViewsOnSmoking getViewsOnSmoking() {
		return viewsOnSmoking;
	}

	public void setViewsOnSmoking(Integer viewsOnSmoking) {
		if(viewsOnSmoking == null){
			viewsOnSmoking = 0;
		}
		this.viewsOnSmoking = ViewsOnSmoking.values()[viewsOnSmoking];
	}

	public Country getCountryOfStudy() {
		return countryOfStudy;
	}

	public void setCountryOfStudy(Integer countryOfStudy) {
		if(countryOfStudy == null){
			countryOfStudy = 0;
		}
		this.countryOfStudy = Country.values()[countryOfStudy];
	}

	public String getCityOfStudy() {
		return cityOfStudy;
	}

	public void setCityOfStudy(String cityOfStudy) {
		this.cityOfStudy = cityOfStudy;
	}

	public String getCollegeOrUniversityName() {
		return collegeOrUniversityName;
	}

	public void setCollegeOrUniversityName(String collegeOrUniversityName) {
		this.collegeOrUniversityName = collegeOrUniversityName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public ModeOfStudy getModeOfStudy() {
		return modeOfStudy;
	}

	public void setModeOfStudy(Integer modeOfStudy) {
		if(modeOfStudy == null){
			modeOfStudy = 0;
		}
		this.modeOfStudy = ModeOfStudy.values()[modeOfStudy];
	}

	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getCurrentCity() {
		return currentCity;
	}

	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}

	public StatusOfStudy getStatusOfStudy() {
		return statusOfStudy;
	}

	public void setStatusOfStudy(Integer statusOfStudy) {
		if(statusOfStudy == null){
			statusOfStudy = 0;
		}
		this.statusOfStudy = StatusOfStudy.values()[statusOfStudy];
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolTown() {
		return schoolTown;
	}

	public void setSchoolTown(String schoolTown) {
		this.schoolTown = schoolTown;
	}

	public String getSchoolYears() {
		return schoolYears;
	}

	public void setSchoolYears(String schoolYears) {
		this.schoolYears = schoolYears;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public List<User> getMyFriends() {
		return myFriends;
	}

	public void setMyFriends(List<User> myFriends) {
		this.myFriends = myFriends;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (uniqueId == null) {
			if (other.uniqueId != null)
				return false;
		} else if (!uniqueId.equals(other.uniqueId))
			return false;
		return true;
	}

	public List<User> getiFollow() {
		return iFollow;
	}

	public void setiFollow(List<User> iFollow) {
		this.iFollow = iFollow;
	}

	public List<User> getMyFollowers() {
		return myFollowers;
	}

	public void setMyFollowers(List<User> myFollowers) {
		this.myFollowers = myFollowers;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the birthdayStatus
	 */
	public BirthdayStatus getBirthdayStatus() {
		return birthdayStatus;
	}

	/**
	 * @param birthdayStatus the birthdayStatus to set
	 */
	public void setBirthdayStatus(Integer birthdayStatus) {
		if(birthdayStatus == null){
			birthdayStatus = 0;
		}
		this.birthdayStatus = BirthdayStatus.values()[birthdayStatus];
	}
}