package com.iNet.beans.joinTables;

import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;

@Table(name="BookMarksUsers", tableType=TableType.JOIN_TABLE)
public class BookMarksUsers extends Entity {

	@JoinColumn(name="iAmId", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int iAmId;
	
	@JoinColumn(name="userId", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int userId;

	public int getiAmId() {
		return iAmId;
	}

	public void setiAmId(int iAmId) {
		this.iAmId = iAmId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}