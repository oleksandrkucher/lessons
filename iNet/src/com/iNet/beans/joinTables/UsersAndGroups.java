package com.iNet.beans.joinTables;

import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;

@Table(name="UsersAndGroups", tableType=TableType.JOIN_TABLE)
public class UsersAndGroups extends Entity {

	@JoinColumn(name="userId", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private Integer userId;
	
	@JoinColumn(name="groupId", type=NumberTypeOnly.INTEGER, joinToTable="Group",
			classNameForTable="com.iNet.beans.Group")
	private Integer groupid;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}
}