package com.iNet.beans.joinTables;

import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;

@Table(name="BlackList", tableType=TableType.JOIN_TABLE)
public class BlackList extends Entity {

	@JoinColumn(name="doNotWantMe", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int doNotWantMe;
	
	@JoinColumn(name="iDoNotWant", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int iDoNotWant;

	public int getDoNotWantMe() {
		return doNotWantMe;
	}

	public void setDoNotWantMe(int doNotWantMe) {
		this.doNotWantMe = doNotWantMe;
	}

	public int getiDoNotWant() {
		return iDoNotWant;
	}

	public void setiDoNotWant(int iDoNotWant) {
		this.iDoNotWant = iDoNotWant;
	}
}