package com.iNet.beans.joinTables;

import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;

@Table(name="Friends", tableType=TableType.JOIN_TABLE)
public class Friends extends Entity {

	@JoinColumn(name="firstFriend", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int firstFriend;
	
	@JoinColumn(name="secondFriend", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int secondFriend;

	public int getFirstFriend() {
		return firstFriend;
	}

	public void setFirstFriend(int firstFriend) {
		this.firstFriend = firstFriend;
	}

	public int getSecondFriend() {
		return secondFriend;
	}

	public void setSecondFriend(int secondFriend) {
		this.secondFriend = secondFriend;
	}
}