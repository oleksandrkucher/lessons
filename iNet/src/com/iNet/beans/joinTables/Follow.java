package com.iNet.beans.joinTables;

import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;

@Table(name="Follow", tableType=TableType.JOIN_TABLE)
public class Follow extends Entity {

	@JoinColumn(name="iFollow", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int iFollow;
	
	@JoinColumn(name="followMe", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int followMe;

	public int getiFollow() {
		return iFollow;
	}

	public void setiFollow(int iFollow) {
		this.iFollow = iFollow;
	}

	public int getFollowMe() {
		return followMe;
	}

	public void setFollowMe(int followMe) {
		this.followMe = followMe;
	}
}