package com.iNet.beans.joinTables;

import com.iNet.annotations.Column;
import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.LikeDislikeType;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;
import com.iNet.beans.enums.Type;

@Table(name = "Likes", tableType=TableType.JOIN_TABLE_WITH_PARAMETR)
public class Likes extends Entity {
	@JoinColumn(name="userId", type=NumberTypeOnly.INTEGER, joinToTable="User", 
			classNameForTable = "com.iNet.beans.User")
	private int userId;

	@Column(name = "objectId", type=Type.INTEGER)
	private int objectId;

	@Column(name="type", type=Type.ENUM)
	private LikeDislikeType type;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the type
	 */
	public LikeDislikeType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(LikeDislikeType type) {
		this.type = type;
	}

	/**
	 * @return the objectId
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}
}