package com.iNet.beans.joinTables;

import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;

@Table(name="BookMarksGroups", tableType=TableType.JOIN_TABLE)
public class BookMarksGroups extends Entity {

	@JoinColumn(name="iAmId", type=NumberTypeOnly.INTEGER, joinToTable="User",
			classNameForTable="com.iNet.beans.User")
	private int iAmId;
	
	@JoinColumn(name="groupId", type=NumberTypeOnly.INTEGER, joinToTable="Group",
			classNameForTable="com.iNet.beans.Group")
	private int groupId;

	public int getiAmId() {
		return iAmId;
	}

	public void setiAmId(int iAmId) {
		this.iAmId = iAmId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
}