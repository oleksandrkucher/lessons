package com.iNet.beans.enums;

public enum Type {
	STRING, INTEGER, DATE, DATE_AND_TIME, ENUM, TEXT, BOOLEAN, LONG, SHORT, BYTE, ENTITY;
}