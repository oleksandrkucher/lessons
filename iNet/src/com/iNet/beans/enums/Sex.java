package com.iNet.beans.enums;

public enum Sex {

	NOT_SPESIFIED("WTF?"), MALE("Male"), FEMALE("Female");
	
	private final String displayName;

    private Sex(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}