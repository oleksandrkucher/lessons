package com.iNet.beans.enums;

public enum RelationshipStatus {

	NOT_SPESIFIED(""), SINGLE("Single"), IN_A_RELATIOSHIP("In a relationship"), ENGAGED("Engaged"), 
	MARRIED("Married"), IN_LOVE("In love"), ITS_COMPLICATED("Its complicated"), 
	ACTIVELY_SEARCHING("Actively searching"); 
	
	private final String displayName;

    private RelationshipStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}