package com.iNet.beans.enums;

public enum ImportantInOthers {

	NOT_SPECIFIED(""), INTELLECT_AND_CREATIVITY("Intellect and creativity"), 
	KINDNESS_AND_HONESTY("Kindness and honesty"), HEALTH_AND_BEAUTY("Health and beauty"), 
	WEALTH_AND_POWER("Wealth and power"), COURAGE_AND_PERSISTENCE("Courage and persistence"), 
	HUMOR_AND_LOVE_FOR_LIFE("Humor and love for life");
	
	private final String displayName;

    private ImportantInOthers(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}