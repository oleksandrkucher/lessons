package com.iNet.beans.enums;

public enum Country {

	NOT_SPECIFIED(""), UKRAINE("Ukraine"), RUSSIA("Russia"), POLAND("Poland"), USA("USA");
	
	private final String displayName;

    private Country(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}