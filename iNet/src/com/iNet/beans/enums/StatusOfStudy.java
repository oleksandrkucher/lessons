package com.iNet.beans.enums;

public enum StatusOfStudy {

	NOT_SPECIFIED(""), APPLICANT("Applicant"), STUDENT_SPECIALIST("Student (Specialist)"),
	STUDENT_BACHELORS("Student (Bachelor's)"), STUDENT_MASTERS("Student (Master's)"),
	ALUMNUS_SPECIALIST("Alumnus (Specialist)"), ALUMNUS_BACHELORS("Alumnus (Bachelor's)"),
	ALUMNUS_MASTERS("Alumnus (Master's)"), POSTGRADUATE_STUDENT("Postgraduate Student"),
	CANDIDATE_OF_SCIENCES("Candidate of Sciences");
	
	private final String displayName;

    private StatusOfStudy(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}