package com.iNet.beans.enums;

public enum PersonalPriority {

	NOT_SPESIFIED(""), FAMILY_AND_CHILDREN("Family and children"), CAREER_AND_MONEY("Career and money"), 
	ENTERTAINMENT_AND_LEISURE("Entertainment and leisure"), SCIENCE_AND_RESEARCH("Science and research"), 
	IMPROVING_THE_WORLD("Improving the world"), PERSONAL_DEVELOPMENT("Personal development"), 
	BEAUTY_AND_ART("Beauty and art"), FAME_AND_INFLUENCE("Fame and influence");
	
	private final String displayName;

    private PersonalPriority(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}