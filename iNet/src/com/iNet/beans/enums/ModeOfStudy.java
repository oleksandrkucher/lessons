package com.iNet.beans.enums;

public enum ModeOfStudy {

	NOT_SPECIFIED(""), FULL_TIME("Full-time"), PART_TIME("Part-time"), DISTANCE_LEARNING("Distance Learning");
	
	private final String displayName;

    private ModeOfStudy(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}