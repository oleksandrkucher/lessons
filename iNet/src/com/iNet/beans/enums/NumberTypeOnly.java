package com.iNet.beans.enums;

public enum NumberTypeOnly {
	INTEGER, LONG, BYTE, SHORT;
}