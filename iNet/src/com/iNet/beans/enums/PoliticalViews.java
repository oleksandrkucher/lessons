package com.iNet.beans.enums;

public enum PoliticalViews {

	NOT_SPESIFIED(""), APATHETIC("Apathetic"), COMMUNIST("Communist"), SOCIALIST("Socialist"), 
	CENTRIST("Centrist"), LIBERAL("Liberal"), CONSERVATIVE("Conservative"), 
	MONARCHIST("Monarchist"), ULTRACONSERVATIVE("Ultraconservative");
	
	private final String displayName;

    private PoliticalViews(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}