package com.iNet.beans.enums;

public enum ViewsOnAlcohol {

	NOT_SPECIFIED(""), VERY_NEGATIVE("Very negative"), NEGATIVE("Negative"), 
	COMPROMISABLE("Compromisable"), NEUTRAL("Neutral"), POSITIVE("Positive");
	
	private final String displayName;

    private ViewsOnAlcohol(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}