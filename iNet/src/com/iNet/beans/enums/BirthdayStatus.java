package com.iNet.beans.enums;

public enum BirthdayStatus {

	SHOW("Show date on my page"), DONT_SHOW("Do not show date on my page");
	
	private final String displayName;

	private BirthdayStatus(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}