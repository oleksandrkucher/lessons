package com.iNet.beans.enums;

public enum ViewsOnSmoking {

	NOT_SPECIFIED(""), VERY_NEGATIVE("Very negative"), NEGATIVE("Negative"), 
	COMPROMISABLE("Compromisable"), NEUTRAL("Neutral"), POSITIVE("Positive");
	
	private final String displayName;

    private ViewsOnSmoking(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}