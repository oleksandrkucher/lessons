package com.iNet.beans;

import java.util.Date;

import com.iNet.annotations.Column;
import com.iNet.annotations.GeneratedValue;
import com.iNet.annotations.Id;
import com.iNet.annotations.ManyToOne;
import com.iNet.annotations.Table;
import com.iNet.beans.enums.Type;
import com.iNet.interfaces.DynamicValues;

/**
 * Class describe table "Comment" in our database
 * 
 * @author kol
 */
@Table(name="Comments", haveEntity=true, haveDate=true)
public class Comment extends Entity implements DynamicValues {

	@Id
	@GeneratedValue
	@Column(name="id", type=Type.INTEGER, sizeType=SIZE_OF_INDEX_COLUMN)
	private Integer id;
	
	@Column(name="body", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String body;
	
	@ManyToOne(thisColumnName="ownerId", joinTableName="User")
	private User owner;
	
	@ManyToOne(thisColumnName="postId", joinTableName="Post")
	private Post post;
	
	@Column(name="date", type=Type.DATE_AND_TIME)
	private Date date;
	
	@Column(name="like", type=Type.INTEGER, sizeType=SIZE_OF_LIKE_AND_DISLIKE_COLUMN)
	private Integer like;

	@Column(name="dislike", type=Type.INTEGER, sizeType=SIZE_OF_LIKE_AND_DISLIKE_COLUMN)
	private Integer dislike;
	
 	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Integer getLike() {
		return like;
	}

	public void setLike(Integer like) {
		this.like = like;
	}

	public Integer getDislike() {
		return dislike;
	}

	public void setDislike(Integer dislike) {
		this.dislike = dislike;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the post
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * @param post the post to set
	 */
	public void setPost(Post post) {
		this.post = post;
	}
}