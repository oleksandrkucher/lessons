package com.iNet.beans;

import java.util.Date;

import com.iNet.annotations.Column;
import com.iNet.annotations.GeneratedValue;
import com.iNet.annotations.Id;
import com.iNet.annotations.ManyToOne;
import com.iNet.annotations.Table;
import com.iNet.beans.enums.Type;
import com.iNet.interfaces.DynamicValues;

/**
 * Class describe table "Message" in our database
 * 
 * @author kol
 */
@Table(name="Messages", haveEntity=true, haveDate=true)
public class Message extends Entity implements DynamicValues{

	@Id
	@GeneratedValue
	@Column(name="id", type=Type.INTEGER, sizeType=SIZE_OF_INDEX_COLUMN)
	private Integer id;
	
	@ManyToOne(thisColumnName="senderId", joinTableName="User")
	private User sender;
	
	@ManyToOne(thisColumnName="recipientId", joinTableName="User")
	private User recipient;
	
	@Column(name="body", type=Type.TEXT, sizeType=SIZE_OF_TEXT_COLUMN)
	private String body;
	
	@Column(name="newMessage", type=Type.BOOLEAN, sizeType=SIZE_OF_BOOLEAN_COLUMN)
	private boolean newMessage;
	
	@Column(name="delByOwner", type=Type.BOOLEAN, sizeType=SIZE_OF_BOOLEAN_COLUMN)
	private boolean delByOwner;
	
	@Column(name="delByGetter", type=Type.BOOLEAN, sizeType=SIZE_OF_BOOLEAN_COLUMN)
	private boolean delByGetter;
	
	@Column(name="date", type=Type.DATE_AND_TIME)
	private Date date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public boolean isNewMessage() {
		return newMessage;
	}

	public void setNewMessage(boolean newMessage) {
		this.newMessage = newMessage;
	}

	public boolean isDelByOwner() {
		return delByOwner;
	}

	public void setDelByOwner(boolean delByOwner) {
		this.delByOwner = delByOwner;
	}

	public boolean isDelByGetter() {
		return delByGetter;
	}

	public void setDelByGetter(boolean delByGetter) {
		this.delByGetter = delByGetter;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getRecipient() {
		return recipient;
	}

	public void setRecipient(User recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
}