package com.iNet.services;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;

import com.iNet.annotations.Column;
import com.iNet.annotations.GeneratedValue;
import com.iNet.annotations.Id;
import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.ManyToMany;
import com.iNet.annotations.ManyToOne;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.NumberTypeOnly;
import com.iNet.beans.enums.TableType;
import com.iNet.beans.enums.Type;
import com.iNet.interfaces.DynamicValues;

public class CreateTableDAO extends JdbcTemplate implements DynamicValues {

	private static List<String> createdTables = new ArrayList<>();
	@SuppressWarnings("rawtypes")
	private static Set<Class> joinTables = new HashSet<>();
	
	/**
	 * Create table in database with using reflect and annotations.
	 * My annotations look like Hibernate annotations
	 * If class use annotations ManyToMany or ManyToOne method call himself again 
	 * and create table for field with this annotation
	 * 
	 * Method created this table and all tables with which it has ties, direct and no
	 * 
	 * Method works only with class which have annotation Table and classes which extends Entity   
	 * @param clazz - class of object. You want to create table for this class
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> void createTable(Class<T> clazz) {
		if (createdTables.indexOf(clazz.getAnnotation(Table.class).name()) == -1) {
			createdTables.add(clazz.getAnnotation(Table.class).name());
			StringBuilder sql = new StringBuilder();
			StringBuilder key = new StringBuilder();
			boolean haveManyToMany = false;
			if (clazz.getAnnotation(Table.class) != null && clazz.getAnnotation(Table.class).tableType().equals(TableType.TABLE)) {
				sql = sql.append("CREATE TABLE IF NOT EXISTS `" + clazz.getAnnotation(Table.class).name() + "` (");
				final Field[] fields = clazz.getDeclaredFields();
				Field.setAccessible(fields, true);
				for (final Field f : fields) {
					if (f.getAnnotation(Id.class) != null && f.getAnnotation(GeneratedValue.class) != null
							&& f.getAnnotation(Column.class) != null) {
						sql = sql.append("`" + f.getAnnotation(Column.class).name()
								+ "` " + whatType(f.getAnnotation(Column.class).type(), f)
								+ " NOT NULL AUTO_INCREMENT");
						key = key.append("PRIMARY KEY (`"
								+ f.getAnnotation(Column.class).name() + "`)");
					} else if (f.getAnnotation(Column.class) != null) {
						sql = sql.append(", `" + f.getAnnotation(Column.class).name()
								+ "` " + whatType(f.getAnnotation(Column.class).type(), f)
								+ " NULL DEFAULT NULL");
					} else if (f.getAnnotation(ManyToOne.class) != null) {
						sql = sql.append(", `" + f.getAnnotation(ManyToOne.class).thisColumnName() + "` " + "INT("
								+ SIZE_OF_JOIN_INDEX_COLUMN + ")" + " NULL DEFAULT NULL");
						Class<T> classForManyToOne = (Class<T>)f.getType();
						key = key.append(", FOREIGN KEY (`"
								+ f.getAnnotation(ManyToOne.class).thisColumnName() + "`) REFERENCES `"
								+ f.getAnnotation(ManyToOne.class).joinTableName() + "` (`id`)");
						createTable(classForManyToOne);
					} else if (f.getAnnotation(ManyToMany.class) != null) {
						if (!haveManyToMany) {
							haveManyToMany = true;
						}
						try {
							joinTables.add(Class.forName(f.getAnnotation(ManyToMany.class).classNameForTable()));
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
				sql = sql.append(", " + key + ")");
				execute(sql.toString());
				//System.out.println(sql);
			} else if (clazz.getAnnotation(Table.class) != null && clazz.getAnnotation(Table.class)
					.tableType().equals(TableType.JOIN_TABLE)) {
				sql = generateSQLForCreatingJoinTable(clazz);
				//System.out.println(sql);
				execute(sql.toString());
			} else if(clazz.getAnnotation(Table.class) != null && clazz.getAnnotation(Table.class)
					.tableType().equals(TableType.JOIN_TABLE_WITH_PARAMETR)){
				sql = generateSQLForCreatingJoinTableWithParameter(clazz);
				//System.out.println(sql);
				execute(sql.toString());
			}
		}
	}
	
	/**
	 * Generate sql query for creating join table
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T extends Entity> StringBuilder generateSQLForCreatingJoinTable(Class<T> clazz){
		StringBuilder foreignKey = new StringBuilder();
		StringBuilder primaryKey = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		sql = sql.append("CREATE TABLE IF NOT EXISTS `" + clazz.getAnnotation(Table.class).name() + "` (");
		final Field[] fields = clazz.getDeclaredFields();
		Field.setAccessible(fields, true);
		for (Field f : fields) {
			if (f.getAnnotation(JoinColumn.class) != null) {
				sql = sql.append("`" + f.getAnnotation(JoinColumn.class).name()
					+ "` " + whatTypeForJoinAndIndexColumn(f.getAnnotation(JoinColumn.class).type()) 
					+ " NOT NULL, ");
				foreignKey = foreignKey.append(", FOREIGN KEY (`" + f.getAnnotation(JoinColumn.class).name()
					+ "`) REFERENCES `" + f.getAnnotation(JoinColumn.class).joinToTable() + "` (`id`)");
				if(createdTables.indexOf(f.getAnnotation(JoinColumn.class).joinToTable()) == -1){
					try {
						createTable((Class<T>)Class.forName(f.getAnnotation(JoinColumn.class).classNameForTable()));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		primaryKey = primaryKey.append("PRIMARY KEY (`"
				+ fields[0].getAnnotation(JoinColumn.class).name() + "`, `"
				+ fields[1].getAnnotation(JoinColumn.class).name() + "`)");
		sql = sql.append(" " + primaryKey + foreignKey + ")");
		return sql;
	}
	
	/**
	 * Generate part of query for creating join table with additional parameter
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T extends Entity> StringBuilder generateSQLForCreatingJoinTableWithParameter(Class<T> clazz){
		StringBuilder foreignKey = new StringBuilder();
		StringBuilder primaryKey = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		sql = sql.append("CREATE TABLE IF NOT EXISTS `" + clazz.getAnnotation(Table.class).name() + "` (");
		final Field[] fields = clazz.getDeclaredFields();
		Field.setAccessible(fields, true);
		for (Field f : fields) {
			if (f.getAnnotation(JoinColumn.class) != null) {
				sql = sql.append("`" + f.getAnnotation(JoinColumn.class).name()
						+ "` " + whatTypeForJoinAndIndexColumn(f.getAnnotation(JoinColumn.class).type()) 
						+ " NOT NULL, ");
				foreignKey = foreignKey.append(", FOREIGN KEY (`" + f.getAnnotation(JoinColumn.class).name()
						+ "`) REFERENCES `" + f.getAnnotation(JoinColumn.class).joinToTable() + "` (`id`)");
				if(createdTables.indexOf(f.getAnnotation(JoinColumn.class).joinToTable()) == -1){
					try {
						createTable((Class<T>)Class.forName(f.getAnnotation(JoinColumn.class).classNameForTable()));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			} else if(f.getAnnotation(Column.class) != null){
				sql = sql.append("`" + f.getAnnotation(Column.class).name()
						+ "` " + whatType(f.getAnnotation(Column.class).type(), f) 
						+ " NOT NULL, ");
			}
		}
		primaryKey = primaryKey.append("PRIMARY KEY (`"
				+ fields[0].getAnnotation(JoinColumn.class).name() + "`, `"
				+ fields[1].getAnnotation(Column.class).name() + "`)");
		sql = sql.append(" " + primaryKey + foreignKey + ")");
		return sql;
	}
	
	/**
	 * Method read annotation parameters and create correct sql query
	 * @param typeName - name of type which you write into annotation
	 * @param f - current field of class
	 * @return part of new sql query
	 */
	private StringBuilder whatType(Type typeName, Field f){
		StringBuilder type = null;
		switch(typeName){
			case INTEGER:
				type = new StringBuilder("INT(");
				break;
			case LONG:
				type = new StringBuilder("BIGINT(");
				break;
			case STRING:
				type = new StringBuilder("VARCHAR(");
				break;
			case BOOLEAN:
				type = new StringBuilder("TINYINT(");
				break;
			case SHORT:
				type = new StringBuilder("MEDIUMINT(");
				break;
			case BYTE:
				type = new StringBuilder("SMALLINT(");
				break;
			case TEXT:
				type = new StringBuilder("TEXT");
				break;
			case DATE:
				type = new StringBuilder("DATE");
				break;
			case DATE_AND_TIME:
				type = new StringBuilder("DATETIME");
				break;
			case ENUM:
				type = new StringBuilder("INT(");
				break;
			default :
				type = new StringBuilder("VARCHAR(");
				break;
		}
		if(f.getAnnotation(Column.class).sizeType() > 0 && !Type.DATE.equals(typeName) && 
				!Type.TEXT.equals(typeName) && !Type.DATE_AND_TIME.equals(typeName)){
			type = type.append(f.getAnnotation(Column.class).sizeType() + ")");
		} else if(!Type.DATE.equals(typeName) && !Type.DATE_AND_TIME.equals(typeName) && 
				!Type.TEXT.equals(typeName)){
			type = type.append(DEFAULT_SIZE + ")");
		} 
		if(typeName.equals(Type.BOOLEAN)){
			type = type.append(" NOT NULL DEFAULT '0'");
		}
		return type;
	}

	/**
	 * Method read annotation parameters and create correct sql query.
	 * This method specially for table with annotation Table and parameter joinTable=true
	 * @param typeName - name of type which you write into annotation
	 * @param f - current field of class
	 * @return part of new sql query
	 */
	private StringBuilder whatTypeForJoinAndIndexColumn(NumberTypeOnly typeName){
		StringBuilder type = null;
		switch(typeName){
			case INTEGER:
				type = new StringBuilder("INT(");
				break;
			case LONG:
				type = new StringBuilder("BIGINT(");
				break;
			case SHORT:
				type = new StringBuilder("MEDIUMINT(");
				break;
			case BYTE:
				type = new StringBuilder("SMALLINT(");
				break;
			default :
				type = new StringBuilder("INT(");
				break;
		}
		type = type.append(SIZE_OF_INDEX_COLUMN + ")");
		return type;
	}

	/**
	 * Method call another method for creating join tables
	 */
	public <T extends Entity> void createJoinTables(){
		for(Class<T> c: joinTables){
			createTable(c);
		}
	}
}