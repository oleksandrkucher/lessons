package com.iNet.services;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.jdbc.core.JdbcTemplate;

import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.interfaces.DynamicValues;

public class DAO extends JdbcTemplate implements DynamicValues {

	/**
	 * Gets the number of rows in the table
	 * @param clazz - class of object which stored in the table what you need
	 * @return integer number
	 */
	public <T extends Entity> int getSize(Class<T> clazz){
		return queryForInt("SELECT COUNT(*) FROM `" + clazz.getAnnotation(Table.class).name() + "`");
	}

	/**
	 * Convert inputed password to md5 for verify it
	 * @param password
	 * @return
	 */
	public String passToMD5(String password){
		byte[] bytesOfMessage;
		byte[] theDigest = null;
		try {
			bytesOfMessage = password.getBytes("Unicode");
			MessageDigest md = MessageDigest.getInstance("MD5");
			theDigest = md.digest(bytesOfMessage);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String str = "";
		for(int i = 0; i < theDigest.length; i++){
			str += theDigest[i];
		}
		//System.out.println(str);
		return str;
	}
}