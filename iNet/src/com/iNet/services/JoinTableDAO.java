package com.iNet.services;

import java.lang.reflect.Field;

import org.springframework.jdbc.core.JdbcTemplate;

import com.iNet.annotations.Column;
import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.TableType;
import com.iNet.interfaces.DynamicValues;

public class JoinTableDAO extends JdbcTemplate implements DynamicValues {

	/**
	 * Add link before tables to join table
	 * @param clazzOfJoinTable
	 * @param currentId
	 * @param anotherId
	 * @return
	 */
	public <T extends Entity> boolean addToJoinTable(Class<T> clazzOfJoinTable, int currentId, int anotherId){
		if (clazzOfJoinTable.getAnnotation(Table.class) != null
				&& clazzOfJoinTable.getAnnotation(Table.class).tableType().equals(TableType.JOIN_TABLE)) {
			Field[] fields = clazzOfJoinTable.getDeclaredFields();
			Field.setAccessible(fields, true);
			StringBuilder sql = new StringBuilder("INSERT INTO `"
					+ clazzOfJoinTable.getAnnotation(Table.class).name() + "` (");
			for (Field f : fields) {
				sql = sql.append("`" + f.getAnnotation(JoinColumn.class).name() + "`,");
			}
			sql = sql.deleteCharAt(sql.lastIndexOf(","));
			sql = sql.append(") VALUES (" + currentId + "," + anotherId + ")");
			return update(sql.toString()) > 0;
		} else {
			return false;
		}
	}

	/**
	 * Add link before tables to join table with additional int parameter
	 * @param clazzOfJoinTable
	 * @param currentId
	 * @param anotherId
	 * @param parameter
	 * @return
	 */
	public <T extends Entity> boolean addToJoinTable(Class<T> clazzOfJoinTable, 
			int currentId, int anotherId, int parameter){
		if (clazzOfJoinTable.getAnnotation(Table.class) != null
				&& clazzOfJoinTable.getAnnotation(Table.class).tableType().equals(TableType.JOIN_TABLE_WITH_PARAMETR)) {
			Field[] fields = clazzOfJoinTable.getDeclaredFields();
			Field.setAccessible(fields, true);
			StringBuilder sql = new StringBuilder("INSERT INTO `"
					+ clazzOfJoinTable.getAnnotation(Table.class).name() + "` (");
			sql = sql.append("`" + fields[0].getAnnotation(JoinColumn.class).name() + "`,");
			sql = sql.append("`" + fields[1].getAnnotation(Column.class).name() + "`,");
			sql = sql.append("`" + fields[2].getAnnotation(Column.class).name() + "`");
			sql = sql.append(") VALUES (" + currentId + "," + anotherId + "," + parameter + ")");
			return update(sql.toString()) > 0;
		} else {
			return false;
		}
	}
	
	/**
	 * Verifies exists or not this link before tables in this join table 
	 * @param clazz - class of join table
	 * @param firstId
	 * @param secondId
	 * @return
	 */
	public <T extends Entity> boolean ifExistsInJoinTable(Class<T> clazz, int firstId, int secondId){
		if (clazz.getAnnotation(Table.class) != null
				&& clazz.getAnnotation(Table.class).tableType().equals(TableType.JOIN_TABLE)) {
			Field[] fields = clazz.getDeclaredFields();
			Field.setAccessible(fields, true);
			StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM `"
					+ clazz.getAnnotation(Table.class).name() + "` WHERE ");
			sql = sql.append("`" + fields[0].getAnnotation(JoinColumn.class).name() + "`=" + firstId);
			sql = sql.append(" AND `" + fields[1].getAnnotation(JoinColumn.class).name() + "`=" + secondId);
			return queryForInt(sql.toString()) > 0;
		} else {
			return false;
		}
	}
	
	/**
	 * Verifies exists or not this link before tables in this join table with additional parameter
	 * @param clazz
	 * @param firstId
	 * @param secondId
	 * @param type
	 * @return
	 */
	public <T extends Entity> boolean ifExistsInJoinTable(Class<T> clazz, int firstId, 
			int secondId, int parameter){
		if (clazz.getAnnotation(Table.class) != null
				&& clazz.getAnnotation(Table.class).tableType().equals(TableType.JOIN_TABLE_WITH_PARAMETR)) {
			Field[] fields = clazz.getDeclaredFields();
			Field.setAccessible(fields, true);
			StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM `"
					+ clazz.getAnnotation(Table.class).name() + "` WHERE ");
			sql = sql.append("`" + fields[0].getAnnotation(JoinColumn.class).name() + "`=" + firstId);
			sql = sql.append(" AND `" + fields[1].getAnnotation(Column.class).name() + "`=" + secondId);
			sql = sql.append(" AND `" + fields[2].getAnnotation(Column.class).name() + "`=" + parameter);
			return queryForInt(sql.toString()) > 0;
		} else {
			return false;
		}
	}
	
	/**
	 * Delete record from join table which using in annotation ManyToMany
	 * @param obj - object which use for operations
	 * @param clazz - class of obj
	 * @param columnName - name of column in table where saved object obj
	 * @param id - id of selected object
	 * @param anotherId - id of object which has connection with this object
	 * @return
	 */
	public <T extends Entity> boolean deleteFromJoinTable(Class<T> clazzOfJoinTable, int currentId, int anotherId){
		if (clazzOfJoinTable.getAnnotation(Table.class) != null
				&& clazzOfJoinTable.getAnnotation(Table.class).tableType().equals(TableType.JOIN_TABLE)) {
			Field[] fieldsOfJoinTable = clazzOfJoinTable.getDeclaredFields();
			Field.setAccessible(fieldsOfJoinTable, true);
			StringBuilder sql = new StringBuilder("DELETE FROM `"
					+ clazzOfJoinTable.getAnnotation(Table.class).name() + "` WHERE `"
					+ fieldsOfJoinTable[0].getAnnotation(JoinColumn.class).name() + "`=" 
					+ currentId + " AND `" + fieldsOfJoinTable[1].getAnnotation(JoinColumn.class).name()  
					+ "`=" + anotherId);
			return update(sql.toString()) > 0;
		} else {
			return false;
		}
	}

	/**
	 * Delete record from join table with additional parameter
	 * @param clazzOfJoinTable
	 * @param currentId
	 * @param anotherId
	 * @param parameter
	 * @return
	 */
	public <T extends Entity> boolean deleteFromJoinTable(Class<T> clazzOfJoinTable, 
			int currentId, int anotherId, int parameter){
		if (clazzOfJoinTable.getAnnotation(Table.class) != null
				&& clazzOfJoinTable.getAnnotation(Table.class).tableType().equals(TableType.JOIN_TABLE_WITH_PARAMETR)) {
			Field[] fieldsOfJoinTable = clazzOfJoinTable.getDeclaredFields();
			Field.setAccessible(fieldsOfJoinTable, true);
			StringBuilder sql = new StringBuilder("DELETE FROM `"
					+ clazzOfJoinTable.getAnnotation(Table.class).name() + "` WHERE `"
					+ fieldsOfJoinTable[0].getAnnotation(JoinColumn.class).name() + "`=" + currentId + 
					" AND `" + fieldsOfJoinTable[1].getAnnotation(Column.class).name() + "`=" + anotherId +  
					" AND `" + fieldsOfJoinTable[2].getAnnotation(Column.class).name() + "`=" + parameter);
			return update(sql.toString()) > 0;
		} else {
			return false;
		}
	}
}