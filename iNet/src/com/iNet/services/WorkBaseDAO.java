package com.iNet.services;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import com.iNet.annotations.Column;
import com.iNet.annotations.JoinColumn;
import com.iNet.annotations.ManyToMany;
import com.iNet.annotations.ManyToOne;
import com.iNet.annotations.OneToMany;
import com.iNet.annotations.Table;
import com.iNet.beans.Entity;
import com.iNet.beans.enums.Type;
import com.iNet.interfaces.DynamicValues;

/**
 * Class includes methods for work with database
 * 
 * @author kol
 */
public class WorkBaseDAO extends JdbcTemplate implements DynamicValues {

	/**
	 * Gets one record from table in database and sets as values of new object (extends Entity)
	 * Universal method for all objects (which extends Entity); with using reflection
	 * Use this method when one or more fields of your object which extends Entity, extends Entity too
	 * @param clazz - class of object which you want to get from database
	 * @param id - id of selected record
	 * @return new object extends Entity
	 */
	@SuppressWarnings("rawtypes")
	public <T extends Entity> T getReflect(final Class<T> clazz, String field, String value){
		if(clazz.getAnnotation(Table.class) != null){
			final Field[] fields = clazz.getDeclaredFields();
			Field.setAccessible(fields, true);
			String sql = "SELECT * FROM `" + clazz.getAnnotation(Table.class).name() + "` WHERE `" + field + "`="+value;
			String test = "SELECT COUNT(*) FROM `" + clazz.getAnnotation(Table.class).name() + "` WHERE `" + field + "`="+value;
			//System.out.println(sql);
			if(queryForInt(test) == 1){
				final T object = queryForObject(sql, new RowMapper<T>() {
					@Override
					@SuppressWarnings("unchecked")
					public T mapRow(ResultSet rs, int i) throws SQLException {
						T obj = null;
						try {
							obj = clazz.getConstructor().newInstance();
						} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
								| InvocationTargetException | NoSuchMethodException	| SecurityException e1) {
							e1.printStackTrace();
						}
						for(Field f: fields){
							String str = null;
							int intTmp = 0;
							boolean bool = false;
							Entity entity = null;
							if(f.getAnnotation(Column.class) != null){
								try {
									switch(f.getAnnotation(Column.class).type()){
									case INTEGER:
										intTmp = rs.getInt(f.getAnnotation(Column.class).name());
										f.set(obj, intTmp);
										break;
									case STRING:
										str = rs.getString(f.getAnnotation(Column.class).name());
										f.set(obj, str);
										break;
									case ENUM:
										intTmp = rs.getInt(f.getAnnotation(Column.class).name());
										Enum[] e = (Enum[]) f.getType().getEnumConstants();
										if(e != null){
											f.set(obj, e[intTmp]);
										} else {
											f.set(obj, 0);
										}
										break;
									case BOOLEAN:
										bool = rs.getBoolean(f.getAnnotation(Column.class).name());
										f.set(obj, bool);
										break;
									case DATE:
									case DATE_AND_TIME:
										str = rs.getString(f.getAnnotation(Column.class).name());
										if (str != null) {
											SimpleDateFormat dt;
											if(f.getAnnotation(Column.class).type().equals(Type.DATE)){
												dt = new SimpleDateFormat("yyyy-MM-dd");
											} else {
												dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
											}
											Date date = null;
											try {
												date = dt.parse(str);
											} catch (ParseException e1) {
												e1.printStackTrace();
											}
											f.set(obj, date);
										} else {
											f.set(obj, null);
										}
										break;
									case ENTITY:
										intTmp = rs.getInt(f.getAnnotation(Column.class).name());
										if(intTmp > 0){
											entity = getReflect((Class<T>)f.getType(), "id", Integer.toString(intTmp));
											f.set(obj, entity);
										} else {
											f.set(obj, null);
										}
										break;
									default:
										str = rs.getString(f.getAnnotation(Column.class).name());
										f.set(obj, str);
										break;
									}
								} catch (IllegalArgumentException | IllegalAccessException e) {
									e.printStackTrace();
								}
							} else if (f.getAnnotation(ManyToOne.class) != null) {
								intTmp = rs.getInt(f.getAnnotation(ManyToOne.class).thisColumnName());
								try {
									if(intTmp > 0){
										entity = getReflect((Class<T>)f.getType(), "id", Integer.toString(intTmp));
										f.set(obj, entity);
									} else {
										f.set(obj, null);
									}
								} catch (IllegalArgumentException | IllegalAccessException e) {
									e.printStackTrace();
								}
							}
						}
						return obj;
					}
				});
				return object;
			} else {
				return null;
			}
		} else {
			return null;
		}
    }
	
	/**
	 * Gets all objects as list and add to current object
	 * @param obj - current object
	 * @param clazz - class of current object
	 * @param id - id of current object
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> void setAllFieldsReflectForManyToMany(T obj, Class<T> clazz, int id){
		if(clazz.getAnnotation(Table.class) != null){
			final Field[] fields = clazz.getDeclaredFields();
			Field.setAccessible(fields, true);
			for(Field f: fields){
				if(f.getAnnotation(ManyToMany.class) != null){
					try {
						try {
							f.set(obj, listReflectForManyToMany(
									(Class<T>)Class.forName(f.getAnnotation(ManyToMany.class).classNameForTable()), 
									id, f.getAnnotation(ManyToMany.class).joinColumnName()));
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * Create list of objects for field with annotation ManyToMany
	 * @param needField - field which has annotation ManyToMany 
	 * @param id - id of current object
	 * @param clazz - class of join table
	 * @return list of objects or null if table has not field with annotation ManyToMany 
	 */
	@SuppressWarnings("unchecked")
	private <T extends Entity> List<T> listReflectForManyToMany(Class<T> classOfJoinTable, 
			int firstId, String joinColumnName){
		String sql = null;
		StringBuilder select = new StringBuilder("SELECT ");
		StringBuilder whereStart = new StringBuilder();
		StringBuilder whereEnd = new StringBuilder();
		StringBuilder from = new StringBuilder(" FROM ");
		Class<T> classOfAnotherTable = null;
		final Field[] fieldsOfJoinTable = classOfJoinTable.getDeclaredFields();
		Field.setAccessible(fieldsOfJoinTable, true);
		if(classOfJoinTable.getAnnotation(Table.class) != null){
			from = from.append("`" + classOfJoinTable.getAnnotation(Table.class).name() + "`, ");
			if(joinColumnName.equals(classOfJoinTable.getAnnotation(Table.class).name())){
				try {
					classOfAnotherTable = (Class<T>) Class.forName(fieldsOfJoinTable[0].getAnnotation(JoinColumn.class).classNameForTable());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				whereStart = whereStart.append(" WHERE `" + classOfJoinTable.getAnnotation(Table.class).name()
						+ "`.`" + fieldsOfJoinTable[0].getAnnotation(JoinColumn.class).name() + "`="
						+ firstId + " AND `" + classOfAnotherTable.getAnnotation(Table.class).name()
						+ "`.`id`=`" + classOfJoinTable.getAnnotation(Table.class).name() + "`.`"
						+ fieldsOfJoinTable[1].getAnnotation(JoinColumn.class).name() + "`");
				whereEnd = whereEnd.append(" WHERE `" + classOfJoinTable.getAnnotation(Table.class).name()
						+ "`.`" + fieldsOfJoinTable[1].getAnnotation(JoinColumn.class).name() + "`="
						+ firstId + " AND `" + classOfAnotherTable.getAnnotation(Table.class).name()
						+ "`.`id`=`" + classOfJoinTable.getAnnotation(Table.class).name() + "`.`"
						+ fieldsOfJoinTable[0].getAnnotation(JoinColumn.class).name() + "`");
			} else {
				for (Field f : fieldsOfJoinTable) {
					if (f.getAnnotation(JoinColumn.class) != null && f.getAnnotation(JoinColumn.class).name().equals(joinColumnName)) {
						whereStart = whereStart.append(" WHERE `" + classOfJoinTable.getAnnotation(Table.class).name()
								+ "`.`" + joinColumnName + "`=" + firstId);
					} else if (f.getAnnotation(JoinColumn.class) != null) {
						try {
							classOfAnotherTable = (Class<T>) Class.forName(f.getAnnotation(JoinColumn.class).classNameForTable());
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
						whereEnd = whereEnd.append(" AND `"
								+ classOfAnotherTable.getAnnotation(Table.class).name()
								+ "`.`id`=`" + classOfJoinTable.getAnnotation(Table.class).name() + "`.`"
								+ f.getAnnotation(JoinColumn.class).name() + "`");
					}
				}
			}
			if (classOfAnotherTable != null && classOfAnotherTable.getAnnotation(Table.class) != null) {
				from = from.append("`" + classOfAnotherTable.getAnnotation(Table.class).name() + "`");
				final Field[] fieldsOfAnotherTable = classOfAnotherTable.getDeclaredFields();
				Field.setAccessible(fieldsOfAnotherTable, true);
				for (Field f : fieldsOfAnotherTable) {
					if (f.getAnnotation(Column.class) != null) {
						select = select.append("`" + f.getAnnotation(Column.class).name() + "`,");
					}
				}
				int index = select.lastIndexOf(",");
				if (index >= 0) {
					select = select.deleteCharAt(select.lastIndexOf(","));
				}
			}
		}
		if(joinColumnName.equals(classOfJoinTable.getAnnotation(Table.class).name())){
			List<T> list = new ArrayList<>();
			list.addAll(makeList(select.toString() + from.toString() + whereStart.toString(), classOfAnotherTable));
			list.addAll(makeList(select.toString() + from.toString() + whereEnd.toString(), classOfAnotherTable));
			return list;
		} else {
			sql = new String(select.toString() + from.toString() + whereStart.toString() + whereEnd.toString());
			//System.out.println(sql);
			return makeList(sql, classOfAnotherTable);
		}
	}
	
	/**
	 * Gets all objects and add to list in current object
	 * @param obj - current object
	 * @param clazz - class of current object
	 * @param id - id of current object
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity, E extends Entity> void setAllFieldsReflectForOneToMany(T obj, Class<T> clazz, int id) {
		if (clazz.getAnnotation(Table.class) != null) {
			final Field[] fields = clazz.getDeclaredFields();
			Field.setAccessible(fields, true);
			for (Field f : fields) {
				if (f.getAnnotation(OneToMany.class) != null) {
					try {
						f.set(obj, listReflectForOneToMany((Class<E>) Class.forName(f.getAnnotation(OneToMany.class).classNameForTable()),
								f.getAnnotation(OneToMany.class).columnName(), id));
					} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException  e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * Create list of objects for field with annotation OneToMany
	 * @param classOfAnotherTable - class of object which saved in another table
	 * @param columnName name of column with annotation OneToMany
	 * @param id - id of object
	 * @return list of objects or null if table has not field with annotation ManyToOne 
	 */
	private <T extends Entity> List<T> listReflectForOneToMany(Class<T> classOfAnotherTable, String columnName, int id){
		if(classOfAnotherTable.getAnnotation(Table.class) != null){
			StringBuilder select = new StringBuilder("SELECT * ");
			StringBuilder where = new StringBuilder("WHERE ");
			final Field[] fieldsOfAnotherTable = classOfAnotherTable.getDeclaredFields();
			Field.setAccessible(fieldsOfAnotherTable, true);
			for(Field f: fieldsOfAnotherTable){
				if(f.getAnnotation(ManyToOne.class) != null){
					if(f.getAnnotation(ManyToOne.class).thisColumnName().equals(columnName)){
						where = where.append("`" + classOfAnotherTable.getAnnotation(Table.class).name() + "`.`" + 
								f.getAnnotation(ManyToOne.class).thisColumnName() + "`=" + id);
						select = select.append("FROM `" + classOfAnotherTable.getAnnotation(Table.class).name() + "` ");
						if(classOfAnotherTable.getAnnotation(Table.class).haveDate()){
							where = where.append(" ORDER BY `date` DESC");
						}
						return makeList(select.toString() + where.toString(), classOfAnotherTable);
					}
				}
			}
		}
		return null;
	}
	
	 /**
	 * Gets all records from table in database and sets as values of new objects (extends Entity)
	 * Universal method for all objects (which extends Entity); with using reflection
	 * Use this method when one or more fields of your object which extends Entity, extends Entity too
	 * @param clazz - class of object which you want to get from database
	 * @return list of new objects extends Entity
	 */
	public <T extends Entity> List<T> listReflect(final Class<T> clazz){
		if(clazz.getAnnotation(Table.class) != null){
			final Field[] fields = clazz.getDeclaredFields();
			Field.setAccessible(fields, true);
			return makeList("SELECT * FROM " + clazz.getAnnotation(Table.class).name(), clazz);
		} else {
			return null;
		}
    }
	
	/**
	 * Make request to the database and gets list of objects
	 * Then one by one gets all object from list with all parameter
	 * because simple query can't get all objects correct
	 * @param sql - sql query
	 * @param clazz - class of object? you gets list of this objects
	 * @return
	 */
	public <T extends Entity> List<T> makeList(String sql, Class<T> clazz){
		List<T> objectList = (List<T>) query(sql, new BeanPropertyRowMapper<>(clazz));
		List<T> list = new ArrayList<>();
		Iterator<T> iterator = objectList.iterator();
		while(iterator.hasNext()){
			list.add(getReflect(clazz, "id", Integer.toString(iterator.next().getId())));
			iterator.remove();
		}
		return list;
	}
	
	/**
	 * Delete record from table in database by id
	 * @param clazz - class of object which you want to delete from database
	 * @param id - id of which you want to delete 
	 * @return true if delete is successfully or false otherwise
	 */
	public <T extends Entity> boolean delete (Class<T> clazz, Integer id){
		return update ("DELETE FROM `" + clazz.getAnnotation(Table.class).name() + "` WHERE `id` = ?", id) > 0;
	}
	
	/**
	 * Add or update one record in table in database, using for this operation values from 
	 * object (extends Entity) 
	 * Universal method for all objects (which extends Entity); with using reflection
	 * Use this method when one or more fields of your object which extends Entity, extends Entity too
	 * @param <T>
	 * @param obj - object for save
	 */
	public <T extends Entity> boolean saveReflect (final T obj) {
		final Class<? extends Entity> clazz = obj.getClass();
		final Field[] fields = clazz.getDeclaredFields();
		Field.setAccessible(fields, true);
		PreparedStatementCreator psc = null;
		final int size = fields.length;
		if (obj.isNew()) {
			psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					int tmp = 0;
					StringBuffer sql = new StringBuffer("INSERT INTO `" + 
							clazz.getAnnotation(Table.class).name() + "` (");
					for (Field f : fields) {
						if(f.getAnnotation(Column.class) != null){
							sql = sql.append("`" + f.getAnnotation(Column.class).name() + "`, ");
						} else if(f.getAnnotation(ManyToOne.class) != null){
							sql = sql.append("`" + f.getAnnotation(ManyToOne.class).thisColumnName() + "`, ");
						} else if(f.getAnnotation(JoinColumn.class) != null){
							sql = sql.append("`" + f.getAnnotation(JoinColumn.class).name() + "`, ");
						} else {
							tmp++;
						}
					}
					sql = sql.append(") VALUES (");
					sql = sql.deleteCharAt(sql.lastIndexOf(","));
					for (int j = 1; j < size-tmp; j++) {
						sql = sql.append("?, ");
					}
					sql = sql.append("?)");
					//System.out.println(sql);
					return preparePreparedStatement(new String(sql), fields, obj, con);
				}
			};
		} else {
			psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					StringBuffer sql = new StringBuffer("UPDATE `" + 
						clazz.getAnnotation(Table.class).name() + "` SET ");
					int tmp = 0;
					for (Field f : fields) {
						if(f.getAnnotation(Column.class) != null){
							sql = sql.append("`" + f.getAnnotation(Column.class).name() + "` = ?, ");
						} else if(f.getAnnotation(ManyToOne.class) != null){
							sql = sql.append("`" + f.getAnnotation(ManyToOne.class).thisColumnName() + "` = ?, ");
						} else {
							tmp++;
						}
					}
					sql = sql.append(" WHERE `id` = ?");
					sql = sql.deleteCharAt(sql.lastIndexOf(","));
					PreparedStatement ps = preparePreparedStatement(new String(sql), fields, obj, con);
					ps.setInt(size+1-tmp, obj.getId());
					return ps;
				}
			};
		}
		return update(psc) > 0;
	}
	
	/**
	 * Method which finish preparing database query and create PreparedStatement
	 * @param sql - query without values which must be save in database
	 * @param fields - fields from object class
	 * @param obj - object for saving in database
	 * @param con - connection to database
	 * @return - ready database query (PreparedStatement)
	 * @throws SQLException
	 */
	@SuppressWarnings("rawtypes")
	private <T> PreparedStatement preparePreparedStatement(String sql, Field[] fields, T obj, Connection con) 
			throws SQLException{
		//System.out.println(sql + "\n");
		PreparedStatement ps = con.prepareStatement(sql);
		int i = 1, index = 0;
		for (Field f : fields) {
			try {
				if (f.getAnnotation(Column.class) != null || f.getAnnotation(ManyToOne.class) != null) {
					if(f.getAnnotation(Column.class) != null){
						if (f.getAnnotation(Column.class).type().equals(Type.ENTITY)) {
							Entity entity = (Entity) f.get(obj);
							if(entity != null){
								ps.setInt(i++, entity.getId());
							} else {
								ps.setInt(i++, 0);
							}
						} else if (f.getAnnotation(Column.class).type().equals(Type.ENUM)) {
							if(f.get(obj) != null){
								Enum[] enums = (Enum[]) f.getType().getEnumConstants();
								if(enums != null){
									int j = 0;
									for(Enum e: enums){
										if(e.equals(f.get(obj))){
											index = j;
										}
										j++;
									}
									ps.setInt(i++, index);
								} else {
									ps.setInt(i++, 0);
								}
							} else {
								ps.setInt(i++, 0);
							}
						} else if(f.getAnnotation(Column.class).type().equals(Type.DATE)){
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd"); 
							if(f.get(obj) != null){
								ps.setString(i++, dt.format(f.get(obj)));
							} else {
								ps.setString(i++, null);
							}
						} else if(f.getAnnotation(Column.class).type().equals(Type.DATE_AND_TIME)){
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
							if(f.get(obj) != null){
								ps.setString(i++, dt.format(f.get(obj)));
							} else {
								ps.setString(i++, null);
							}
						} else if(f.getAnnotation(Column.class).type().equals(Type.TEXT)){
							ps.setString(i++, (String) f.get(obj));
						} else {
							//if(f.get(obj) != null && f.get(obj).toString().length() >= f.getAnnotation(Column.class).sizeType()){
								//f.set(obj, f.get(obj).toString().substring(0, f.getAnnotation(Column.class).sizeType()-2));
							//}
							ps.setObject(i++, f.get(obj));
						}
					} else if(f.getAnnotation(ManyToOne.class) != null){
						if (f.get(obj) != null) {
							Entity entity = (Entity) f.get(obj);
							ps.setInt(i++, entity.getId());
						} else {
							ps.setObject(i++, null);
						}
					}
				} else if(f.getAnnotation(JoinColumn.class) != null){
					if (f.get(obj) != null){
						ps.setInt(i++, f.getInt(obj));
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return ps;
	}
}