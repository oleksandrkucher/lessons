package BeanRepresenter;

public class Human {

	private String name;
	private int age;
	private Car car;
	private Cat cat;
	
	public Human(String name, int age, Car car, Cat cat) {
		this.name = name;
		this.age = age;
		this.car = car;
		this.cat = cat;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public Car getCar() {
		return car;
	}
	
	public void setCar(Car car) {
		this.car = car;
	}
	
	public Cat getCat() {
		return cat;
	}
	
	public void setCat(Cat cat) {
		this.cat = cat;
	}

}
